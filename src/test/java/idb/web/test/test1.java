package idb.web.test;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import idb.core.fileutils.TestDataProviders;
import idb.core.logs.LogManager;
import idb.core.testmanagement.JiraUtils;
import idb.web.businessfunctions.BusinessFunctions;
import idb.web.engine.CNWebActionEngine;
import idb.web.pages.HomePage;




public class test1 extends BusinessFunctions{
	public CNWebActionEngine actEngine = null;
	String className=test1.class.getName();
	@BeforeClass
	public void beforeClass() {
		try {
			actEngine = super.cnActionEngine;
			launchBrowser(actEngine);
			loadApplication(actEngine);
			//test


		} catch (Exception e) {
			reportStatus(Status.FAIL, "Exception at Dashboard Test Before Class");
			LogManager.logException(e, className, "Exception at Dashboard Tests Before Class...");
		}
	}

	@DataProvider(name="Test")
	public static Object[][] Dashboard() throws FileNotFoundException, IOException{

		String inpFileName = "UI"+ File.separator + "Test1.xlsx";
		String inpSheetName = "Sheet1";

		Object[][] data = TestDataProviders.loadExcelTable(inpFileName, inpSheetName);		
		return data;		
	}
	@Test(dataProvider = "Test")
	public void verifyDashBoardMarketingBannerDisplay(HashMap<String, String> inpDetails) throws InterruptedException {
		String textForSearching = inpDetails.get("SearchText").trim();
		JiraUtils.testCaseName = inpDetails.get("TestCaseId").trim();

		boolean res = false;

		try {
			System.out.println(textForSearching);
			HomePage hp = new HomePage(cnActionEngine);
			res =hp.enterText(textForSearching);

			reportStatus(res,"Successfully launched the application","Failed to launch the application");
			Assert.assertEquals(true, true);
		} catch (Exception e) {
			reportStatus(Status.FAIL, "Exception to Run verify Dashboard marketing banner display test");
			LogManager.logException(e, className,
					"Exception to Run verify Dashboard marketing banner display test case");
		}

	}

}
