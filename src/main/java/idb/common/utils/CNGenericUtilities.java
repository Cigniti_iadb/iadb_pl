package idb.common.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import idb.core.logs.LogManager;

public class CNGenericUtilities {
	
	static String className=CNGenericUtilities.class.getName();

	/**
	* Purpose: To generate random string between two values
	* Input: String Min and Max values
	* Output: String ; random string based on the input format
	* Author-Date: 14-Aug-2018
	* Reviewed-Date:
	*/
	public static String getRandomInRange(String max, String min)
	{
		int Min=0;
		int Max=0;
    	int randomNum=0;
    	String random=null;
    	int length=String.valueOf(max).length();
		try {
			Min=Integer.parseInt(min);
			Max=Integer.parseInt(max);
			
			 if( length>=4) {
					Max= Min+300;
			}
			
		    Random randomNumber = new Random();
		    randomNum = randomNumber.nextInt((Max - Min) + 1) + Min;
		    random=Integer.toString(randomNum);
			
		} catch (Exception e) {
			LogManager.logException(e,className, "Exception to generate random number");
		}	
		
		return random;
	}
	
	/**
	* Purpose: To generate random string based on the given criteria 
	* Input: String Integer,AlphaNumeric
	* Output: String ; random string based on the input format
	* Author-Date: 14-Aug-2018
	* Reviewed-Date:
	*/
    public static  String getRandom(String format,int number) {
    	String output ="";
    	try {
    		char[] charsString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
            char[] charsAlphaNum = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
            char[] charsNum = "1234567890".toCharArray();
            char[] chars=null;
            if(format.equalsIgnoreCase("ALPHANUMERIC"))
            chars=charsAlphaNum;
            else if(format.equalsIgnoreCase("INT"))
                   chars=charsNum;
            else
                   chars=charsString;
            StringBuilder sb = new StringBuilder();
            Random random = new Random();
            for (int i = 0; i < number; i++) {
                   char c = chars[random.nextInt(chars.length)];
                   sb.append(c);
            }
           output = sb.toString();
            LogManager.logInfo(className, "Random number is : " +output);
			
		} catch (Exception e) {
			LogManager.logException(e,className, "Exception to Random value");
		}
        
        return output;
  }

    /**
	 *@Purpose: To get selected format of date
	 *@author: Bhaskar Soorisetti
	 *@Input:String dateFormat, String dateValue 
	 *@Output: String date
	 *@Author-Date: 26-04-2019
	 *@Reviewed-Date:
	 */
    public static String getDateFormat(String dateFormat,String dateValue) {
    	String date="";
    	try {
    		DateFormat dateFormater=new SimpleDateFormat(dateFormat);
    	    DateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    	    Date dtObj=df.parse(dateValue);
			date=dateFormater.format(dtObj);
		} catch (Exception e) {
			LogManager.logException(e,className, "Exception to Date value");
		}
    	return date;
    }
	 
    /**
	 * @return the current timestamp in format like 2018-11-09T22:55:40.110Z
	 */
	public static String nextTimestamp() {
		
		return (new Timestamp(System.currentTimeMillis()).toInstant()).toString();			
	}
	
	/**
	 *@Purpose: To format a float value with 2 decimals
	 *@author: Jagadeesh Boddeda
	 *@Input:String amount 
	 *@Output: String amount with 2 decimals
	 *@Author-Date: 17-07-2019
	 *@Reviewed-Date:
	 */
	public static String formatFloatValue(String amount) {
		String formattedAmt="";
		try {
			DecimalFormat df = new DecimalFormat("0.00");
			df.setMaximumFractionDigits(2);
			formattedAmt = df.format(Float.parseFloat(amount));
		} catch (Exception e) {
			LogManager.logException(e,className, "Exception to format value to float");
		}
		return formattedAmt;
	}
	
	/**
	 *@Purpose: To return sum of 2 float values with 2 decimals
	 *@author: Jagadeesh Boddeda
	 *@Input:String dateFormat, String dateValue 
	 *@Output: String date
	 *@Author-Date: 17-07-2019
	 *@Reviewed-Date:
	 */
	public static String sumFloatValues(String val1, String val2) {
		String sum = "";
		try {
			sum = BigDecimal.valueOf(Float.sum(Float.parseFloat(val1), Float.parseFloat(val2))).setScale(2,BigDecimal.ROUND_HALF_UP).toString();
		} catch (Exception e) {
			LogManager.logException(e,className, "Exception to sum value to float numbers");
		}
		return sum;
	}
	
	/**
	 *@Purpose: To get expiry month and year in mm/yyyy format
	 *@author: Jagadeesh Boddeda
	 *@Input:String expiry month in MMM format, String expiry year in yyyy format
	 *@Output: String date in mm/yyyy format
	 *@Author-Date: 26-04-2019
	 *@Reviewed-Date:
	 */
	public static String getExpiryMonthYear(String expiryMonth, String expiryYear) {
		String expdate = "";
		try {
			Date date = new SimpleDateFormat("MMM").parse(expiryMonth);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int month=cal.get(Calendar.MONTH)+1;
			if(month > 9) {
				expdate=month+"/"+expiryYear;
			}else {
				expdate="0"+month+"/"+expiryYear;
			}
		} catch (Exception e) {
			LogManager.logException(e,className, "Exception to get expiry month and year in mm/yyyy format");
		}
		return expdate;
	}
	
	/**
	 *@Purpose: To get public IP address
	 *@author: Jagadeesh Boddeda
	 *@Output: String external/public ip address
	 *@Author-Date: 17-07-2019
	 *@Reviewed-Date:
	 */
	public static String getPublicIpAddress() {

		String str=null;
		try{
			URL connection = new URL("http://checkip.amazonaws.com/");
			URLConnection con = connection.openConnection();
			BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
			str = reader.readLine();
		}catch(MalformedURLException e){
			LogManager.logException(e,className, "URL Exception to get external or public ip address");
		}catch(IOException e) {
			LogManager.logException(e,className, "IO Exception to get external or public ip address");
		}
		return str;
		}
	
	/**
	 * @return the current timestamp in Gregorian xml format like 2018-11-09T22:55:40.110Z
	 */
	public static XMLGregorianCalendar currentGreogorianTimeStamp() {
		XMLGregorianCalendar timeStamp = null;
		try {
			timeStamp =  DatatypeFactory.newInstance().newXMLGregorianCalendar(new DateTime().toGregorianCalendar());
			LogManager.logInfo(className, "current timestamp in Gregorian xml format "+timeStamp);
			
		} catch (DatatypeConfigurationException e) {
			LogManager.logException(e,className, "Failed to get current timestamp in Gregorian xml format");
		}
		return timeStamp;			
	}
	
	/**
	 * @return To get Date & Time in Gregorian String format 
	 */
	public static String convertToGregorianFormat(String date, String format) {
		String timeStamp = null;
		try {
			DateTimeFormatter formatter = DateTimeFormat.forPattern(format).withOffsetParsed();
			DateTime dt = formatter.parseDateTime(date);
			timeStamp=DatatypeFactory.newInstance().newXMLGregorianCalendar(dt.toGregorianCalendar()).toString();
			
			LogManager.logInfo(className, "timestamp in Gregorian format "+timeStamp);
			
		} catch (Exception e) {
			LogManager.logException(e,className, "Failed to get timestamp in Gregorian format");
		}
		return timeStamp;			
	}
	
	public static String getDateInRequiredFormat(String cdate, String currentFormat, String expectedFormat) throws ParseException {
		String expdate="";
		try {
			DateFormat formatter = new SimpleDateFormat(currentFormat); 
			Date date = (Date)formatter.parse(cdate);
			SimpleDateFormat newFormat = new SimpleDateFormat(expectedFormat);
			expdate = newFormat.format(date);
			LogManager.logInfo(className, "date in required format is : "+expdate);
		}catch (Exception e) {
			LogManager.logException(e,className, "Failed to get date in required format");
		}
		return expdate;
	}
	/**
	 * @Purpose: To add unique values to array list
	 * @param : List<String> list,String newEntry
	 * @return: List<String>
	 * @Author-Date: 04/09/2019
	 * @Reviewed-Date:
	 */
	public static List<String> addUniqueValuesToList(List<String> list,String newEntry){
	    if(!list.contains(newEntry)) {
	    	list.add(newEntry);
	    }
	    return list;
	    	
	}
	/**
	 * @Purpose: To update values in ascending order in array list
	 * @param : List<String> list
	 * @return: List<String>
	 * @Author-Date: 04/09/2019
	 * @Reviewed-Date:
	 */
	public static List<String> updateAscendingOrderListItems(List<String> list){
	    	@SuppressWarnings("unused")
			Set<String> set = new LinkedHashSet<String>(list);
		    Collections.sort(list);
	    return list;
	    	
	}
	
	/**
	 *@Purpose: To return float value with 2 decimals
	 *@author: Jagadeesh Boddeda
	 *@Input: float value with more than 2 decimals
	 *@Output: float value with 2 decimals
	 *@Author-Date: 21-10-2019
	 *@Reviewed-Date:
	 */
	public static float formatFloatValue(float val) {
		float fval = 0.00f;
		try {
			DecimalFormat df = new DecimalFormat();
			df.setMaximumFractionDigits(2);
			fval = Float.parseFloat(df.format(val));
		} catch (Exception e) {
			LogManager.logException(e,className, "Exception to sum value to float numbers");
		}
		return fval;
	}
	
}
