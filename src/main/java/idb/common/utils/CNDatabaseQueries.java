package idb.common.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import idb.config.idbConstants;
import idb.core.accelerators.AbstractTestEngine;
import idb.core.database.DatabaseVerification;
import idb.core.logs.LogManager;

public class CNDatabaseQueries {

	String className=CNDatabaseQueries.class.getName();

	/**
	 * @purpose: To get Count Of Inmate Search Results 
	 * @param: String dbName,String tableName,String facilityName,String inmateName
	 * @return: int number of inmates
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public int getCountOfInmateSearchResults(String dbName,String tableName,String facilityName,String inmateName) {
		int value=0;
		try {			
			//db.establishDatabaseConnection(dbName);
			String sqlQuery="SELECT * FROM "+tableName+" WHERE FACILITY_ID=(SELECT OFFENDERCONNECTSITEID FROM FACILITYSERVICES WHERE FACILITYNAME LIKE '%"+facilityName+"%') AND (FIRST_NAME LIKE '%"+inmateName+"%' OR LAST_NAME LIKE '%"+inmateName+"%')";
			ResultSet resList = CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			//ResultSet resList =db.dbConnection.getQueryResponse(sqlQuery);
			while(resList.next()) {
				value++;
			}
			resList.close();
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get response");
		}
		return value;
	}
	
	/**
	 * @purpose: To get count of search results
	 * @author: bhaskar
	 * @param: String dbname and String sqlquery
	 * @return: int count of the result rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 * 
	 */
	public int getCountOfSearchResults(String dbName,String query) {
		int value=0;
		try {
			//db.establishDatabaseConnection(dbName);
			DatabaseVerification db = CNDatabases.DB.valueOf(dbName).getConnection();			
			ResultSet resList =db.getQueryResponse(query);
			while(resList.next()) {
				value++;
			}
			resList.close();
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get response");
		}
		return value;
	}
	/**
	 * @purpose: To get COMO member details
	 * @param: String name,String facilityId
	 * @return: List<JSONObject> como details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public  List<JSONObject> getCoMoMemberDetails(String name,String facilityId ) {
		List<JSONObject> resList=null;
		try {
			//db.establishDatabaseConnection(CNDatabases.DB.OC);
			String sqlQuery="SELECT * FROM CORRECTIONS_MEMBERS WHERE FIRST_NAME LIKE '%"+name+"%' OR LAST_NAME LIKE '%"+name+"%' AND FACILITY_ID='"+facilityId+"'";
			resList =CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to Result list");
		}
		return resList;
	}
	/**
	 * @purpose: To get active COMO members count
	 * @param: String accountGuid
	 * @return: int count of Active COMO Members for an account
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */ 
	public int getActiveComoMembersCountForAccount(String accountGuid) {
		int count=0;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT COUNT(*) FROM OFFENDERCONNECT.CORRECTIONS_ACCOUNT_MEMBER WHERE ACCOUNT_ID = '"+accountGuid+"' and ACTIVE ='1'";
			List<JSONObject> resList =CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			count=resList.get(0).getInt("count(*)");
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get getStatusCoMoAccountMember result list");
		}
		return count;
	}

	/**
	 * @purpose: To set ComoMember to Active
	 * @param String accountGuid,String MemberId
	 * @return boolean true if set to active
	 * @author E005111(Vamshi Krishna Voddeti)
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public boolean setComoMemberToActive(String accountGuid, String memberId) {
		boolean blRes =false;
		String activeStatus =null;
		;   	 String sqlQuery=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			sqlQuery="UPDATE OFFENDERCONNECT.CORRECTIONS_ACCOUNT_MEMBER SET  ACTIVE ='1' WHERE ACCOUNT_ID ='"+accountGuid+"' AND MEMBER_ID = '"+memberId+"'";
			CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			sqlQuery="SELECT ACTIVE FROM OFFENDERCONNECT.CORRECTIONS_ACCOUNT_MEMBER WHERE ACCOUNT_ID ='"+accountGuid+"' AND MEMBER_ID = '"+memberId+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			activeStatus=resList.get(0).get("active").toString();
			blRes=(activeStatus.equalsIgnoreCase("1"));
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to set  member ID to active");
		}
		return blRes;
	}

	/**
	 * @purpose: To get FacilityId Of Member
	 * @param : String memberId
	 * @return : String FacilityId
	 * @author E005111(Vamshi Krishna Voddeti)
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getFacilityIdOfMember(String memberId) {
		String facId=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT FACILITY_ID FROM OFFENDERCONNECT.CORRECTIONS_MEMBERS WHERE MEMBER_ID='"+memberId+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			facId=resList.get(0).get("facilityId").toString();
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get Facility Id for Member");
		}
		return facId;
	}

	/**
	 * @purpose: To get PrincipalId of Active Member
	 * @param : String memberId
	 * @return : String PrincipalID
	 * @author E005111(Vamshi Krishna Voddeti)
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getPrincipalIdOfActiveMember(String memberId) {
		String principalID=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT PRINCIPAL_ID FROM OFFENDERCONNECT.CORRECTIONS_MEMBERS WHERE MEMBER_ID='"+memberId+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			principalID=resList.get(0).get("principalId").toString();
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get Principal Id for  Member");
		}
		return principalID;
	}

	/**
	 * @purpose: To get Status of member for Account
	 * @param : String acountId,String memberId
	 * @return : int activestatus
	 * @author : E005111(Vamshi)
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public int getStatusOfMemberForAccount(String acountId, String memberId) {
		int activestatus = 0;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT ACTIVE FROM OFFENDERCONNECT.CORRECTIONS_ACCOUNT_MEMBER WHERE ACCOUNT_ID='"+acountId+"' AND MEMBER_ID='"+memberId+"'" ;
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			activestatus=resList.get(0).getInt("active");
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get the status of Member for Active account");
		}
		return activestatus;
	}

	/**
	 * @purpose: To activate or Deactivate User Account
	 * @param: String emailId,int activeStatusCode
	 * @return: boolean activate/deactivate user
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public boolean activateOrDeactivateUserAccount(String emailId,int activeStatusCode) {
		boolean res=false;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="UPDATE OFFENDERCONNECT.ACCOUNT SET ACTIVE ='"+activeStatusCode+"' WHERE EMAIL_ADDR ='"+emailId+"'";	
			int value=CNDatabases.offenderConnect().executeUpdateQuery(sqlQuery);
			if(value > 0) {
				res=true;
			}
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to Activate Or Deactivate User Account");
		}
		return res;
	}
	/**
	 * @purpose: To verify CoMo migrated for facility
	 * @param: String facility
	 * @return: int CoMo migrated for facility status 
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public int verifyCoMoMigratedForFacility(String facility) {
		int value=0;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.FACILITYSERVICES WHERE FACILITYNAME LIKE '%"+facility+"%'";
			List<JSONObject> resList =CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			value=Integer.parseInt(resList.get(0).getString("comoMigrated").toString());			
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get response");
		}
		return value;
	}
	/**
	 * @purpose: To verify status CoMo Account Member
	 * @param: String accountGuid
	 * @return: int CoMo migrated for facility status 
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public int getStatusCoMoAccountMember(String accountGuid) {
		int value=0;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.CORRECTIONS_ACCOUNT_MEMBER WHERE ACCOUNT_ID = '"+accountGuid+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			value=Integer.parseInt(resList.get(0).getString("active").toString());			
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get response");
		}
		return value;
	}
	/**
	 * @purpose: To verify CoMo payment method for facility
	 * @param: String serviceType
	 * @return: int tender from service fee
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String verifyCoMoPaymentMethodForFacility(String serviceType) {
		String tender=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.CORRECTIONS_SERVICE_FEE WHERE FACILITY_SERVICE_ID =(SELECT facility_service_id from CORRECTIONS_FACILITY_SERVICE where DISPLAY_NAME='"+serviceType+"') AND METHOD = 'WEB'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			tender=resList.get(0).getString("tender");			
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get response");
		}
		return tender;
	}
	/**
	 * @purpose: To verify CoMo Min And Max value for facility
	 * @param: String serviceType
	 * @return: List<JSONObject> service fee details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public  List<JSONObject>  verifyCoMoMinAndMaxValueForFacility(String serviceType) {
		List<JSONObject> resList=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.CORRECTIONS_SERVICE_FEE where FACILITY_SERVICE_ID=(select facility_service_id from CORRECTIONS_FACILITY_SERVICE where DISPLAY_NAME='"+serviceType+"') AND METHOD = 'WEB'";
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get response");
		}
		return resList;
	}
	/**
	 * @purpose: To get recent credit payment
	 * @param: String accountGuid
	 * @return: List<JSONObject> credit payment details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getRecentCreditPayment(String accountGuid) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeCreditPaymentQuery(accountGuid);
		return resList;
	}
	/**
	 * @purpose: To get account number from account table
	 * @param: String accountGuid
	 * @return: String account table details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getAccountNumberFromAccountTable(String accountGuid) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeAccountNumberFromAccountTableQuery(accountGuid);
		return (String) resList.get(0).get("accountNumber");
	}
	/**
	 * @purpose: To get password from account table
	 * @param: String userId
	 * @return: String get password from account table details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getPasswordFromAccountTable(String userId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList =executeAccountDataFromAccountTableQuery(userId);
		return (String) resList.get(0).get("password");
	}
	/**
	 * @purpose: To update password from account table
	 * @param: String userId, String password
	 * @return: int updated rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public int updateAccountPassword(String userId, String password) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeUpdateAccountPasswordQuery(userId, password);
		int updCount = resList.get(0).getInt("UpdatedRows");
		return updCount;
	}
	/**
	 * @purpose: To get number of active credit cards
	 * @param: String accountNumber
	 * @return: int number of active rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public int getNumOfActiveCreditCards(String accountNumber) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeNumOfActiveCreditCardsQuery(accountNumber);
		return Integer.parseInt((String) resList.get(0).get("count(*)"));
	}
	/**
	 * @purpose: To get account GUID from account Table
	 * @param: String username
	 * @return: String account GUID
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getAccountGuidFromAccountTable(String username) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeAccountDataFromAccountTableQuery(username);
		return (String) resList.get(0).get("accountGuid");
	}
	/**
	 * @purpose: To get transaction id
	 * @param: String accountGuid
	 * @return: String transaction id
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getTransactionId(String accountGuid) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeCorrectionsTransaction("account_guid", accountGuid);
		return (String) resList.get(0).get("transactionId");
	}
	/**
	 * @purpose: To get billing id
	 * @param: String accountNumber
	 * @return: String billing id
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getBillingId(String accountNumber) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeGetBillingId(accountNumber);
		return (String) resList.get(0).get("gtlBillingId");
	}
	/**
	 * @purpose: To get Transaction Token
	 * @param: String merchantorderId
	 * @return: String Transaction Token
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getTransactionToken(String merchantorderId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeGetTransactionToken(merchantorderId);
		return (String) resList.get(0).get("transactionToken");
	}
	/**
	 * @purpose: To get account details from account Table
	 * @param: String username, String columnName
	 * @return: String Transaction Token
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getAccountDetailsFromAccountTable(String username, String columnName) {
		//mdb.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeAccountDataFromAccountTableQuery(username);
		return (String) resList.get(0).get(columnName);
	}
	/**
	 * @purpose: To get account details from account Table
	 * @param: String username
	 * @return: List<JSONObject> get account details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getAccountDetails(String username) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeAccountDataFromAccountTableQuery(username);
		return  resList;
	}
	/**
	 * @purpose: To get facilities from account Table
	 * @param: String accountGuid
	 * @return: List<JSONObject> get account details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getFacilitiesOfAccount(String accountGuid){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeGetFacilitiesOfAccountQuery(accountGuid);
		return resList;
	}
	/**
	 * @purpose: To get portal id services
	 * @param: String accountNumber,String portalId
	 * @return: List<JSONObject> get portal id services details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getPortalIdServices(String accountNumber,String portalId){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executePortalIdReturnServices(accountNumber,portalId);
		return resList;
	}

	/**
	 * @purpose: To get facilities of state
	 * @param: String state
	 * @return: List<JSONObject> get facility details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getFacilitiesOfState(String state){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeGetFacilitiesOfStateQuery(state);
		return resList;
	}

	/**
	 * @purpose: To get account members
	 * @param: String accountId
	 * @return: List<JSONObject> get account members
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getAccountMembersQuery(String accountId){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeGetAccountMembersQuery(accountId);
		return resList;
	}

	/**
	 * @purpose: To get facility name
	 * @param: String facilityId
	 * @return: List<JSONObject> get facility details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getFacilityName(String facilityId){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeGetFacilityName(facilityId);
		return resList;
	}

	/**
	 * @purpose: To get autoPay Id
	 * @param: String inmateId
	 * @return: String get auto pay id
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getAutoPayId(String inmateId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeAutoPayIdQuery(inmateId);
		return (String) resList.get(0).get("autopayId");
	}
	
	/**
	 * @purpose: To get autoPay details
	 * @param: String inmateId
	 * @return: String get auto pay id
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getAutoPayDetails(String inmateId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeAutoPayIdQuery(inmateId);
		return resList;
	}
	
	/**
	 * @purpose: To get account auto pay info
	 * @param: String autopayID
	 * @return: List<JSONObject> get account auto pay details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getAccountAutoPayInfo(String autopayID) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeAccountAutoPayQuery(autopayID);
		return resList;
	}
	/**
	 * @purpose: To update auto pay trust
	 * @param: String autopayID
	 * @return: List<JSONObject> update account auto pay details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> updateAutoPayTrust(String autopayId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeUpdateAutoPayTrustQuery(autopayId);
		return resList;
	}
	/**
	 * @purpose: To get messaging transaction description
	 * @param: String accountGuid
	 * @return: List<JSONObject> get messaging transaction purchases pay details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getMessagingTransactionDescription(String accountGuid){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> restList = executeMessagingTransactionPurchaseQuery(accountGuid);
		return restList;
	}
	/**
	 * @purpose: To get messaging email details
	 * @param: String accountGuid
	 * @return: List<JSONObject> get messaging email details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getMessagingEmail(String accountGuid){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> restList = executeMessagingSendEmailQuery(accountGuid);
		return restList;
	}
	/**
	 * @purpose: To get minimum value of advance pay facility
	 * @param: String ocSiteId
	 * @return: String minimum value of advance pay
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getAdvPayFacilityMinValue(String ocSiteId){
		String min=null;
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeAdvPayFacilityMinValue(ocSiteId);
		min =resList.get(0).getString("depositMin");
		return min;
	}
	/**
	 * @purpose: To get active stored card GUID
	 * @param: String accountNumber
	 * @return: String active stored card GUID
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getActiveStoredCardGuid(String accountNumber){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeGetActiveStoredCardGuid(accountNumber);
		return resList.get(0).getString("creditCardGuid");
	}

	/**
	 * @purpose: To get transaction details from ProviderServiceTransaction DB table 
	 * @param: String userId,String inmateId
	 * @return: List<JSONObject> provider service transaction details
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getProviderServiceTransactionDetails(String userId,String inmateId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeProviderServiceTransactionQuery(userId,inmateId);
		return resList;
	}

	/**
	 * @Purpose: To get Master transaction details 
	 * @author : Bhaskar Surisetti
	 * @param : String accountGuid
	 * @return: List<String> round up selection details
	 * @Author-Date: 03-20-2019
	 * @Reviewed-Date:
	 */
	public List<JSONObject> verifyRoundupSelection(String masterTransGuid) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM(SELECT * FROM OFFENDERCONNECT.ROUNDUP_SELECTION WHERE MASTER_TRANSACTION_GUID='%s') WHERE ROWNUM=1", masterTransGuid);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;

	}
	/**
	 * @Purpose: To get values from system parameters table 
	 * @param : String colName, String parameterId,String ocSiteId, String deviceId
	 * @return: String system parameters
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public String getSystemParameters(String colName, String parameterId,String ocSiteId, String deviceId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executeSystemParameters(parameterId, ocSiteId, deviceId);
		return resList.get(0).getString(colName);
	}

	/**
	 * @Purpose: To get values from system parameters table 
	 * @param : String colName, String parameterId,String parameterCategoryName
	 * @return: String system parameters
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public String getSystemParameters(String colName, String parameterId,String parameterCategoryName) {
		List<JSONObject> resList =null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String query = String.format("SELECT * from OFFENDERCONNECT.SYSTEMPARAMETERS where PARAMETERID ='%s' and PARAMETERCATEGORYNAME='%s'",parameterId,parameterCategoryName);
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);			
		} catch (Exception e) {
			LogManager.logException(e,className,"Exception get System parameters");
		}
		return resList.get(0).getString(colName);

	}


	// All the queries. These queries can be reused and that why not make part of the csv

	/* returns deposit data for all deposit types
	 * CHARGE_AMOUNT is used to validate F&F AdvancePay Phone deposits
	 * CREDIT_AMOUNT is used to validate Inmate deposits: pin debit, debit link, trust
	 */
	/**
	 * @Purpose: To get values from credit card payment
	 * @param : String accountGuid
	 * @return: List<JSONObject> credit card payment details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeCreditPaymentQuery(String accountGuid) {
		// We cannot run the below Query to return everything as there is a blob that breaks the query
		String query = String.format("SELECT * FROM (SELECT CREDIT_AMOUNT, CHARGE_AMOUNT, TRANSACTION_SERVICE_TYPE FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE ACCOUNT_GUID = '%s' AND PAYMENT_STATUS = 'ACCEPTED' ORDER BY PAYMENT_DATE_TIME DESC) WHERE ROWNUM < 2", accountGuid);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get account number from account table
	 * @param : String accountGuid
	 * @return: List<JSONObject> account details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> executeAccountNumberFromAccountTableQuery(String accountGuid) {
		String query = String.format("SELECT * FROM OFFENDERCONNECT.ACCOUNT WHERE ACCOUNT_GUID='%s'", accountGuid);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get no of active credit cards
	 * @param : String accountNumber
	 * @return: List<JSONObject> account details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeNumOfActiveCreditCardsQuery(String accountNumber) {
		String query = String.format("SELECT COUNT(*) FROM OFFENDERCONNECT.STORED_CREDIT_CARD WHERE ACCOUNT_NUMBER='%s' AND ACTIVE=1", accountNumber);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get account data from account table
	 * @param : String username
	 * @return: List<JSONObject> account details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeAccountDataFromAccountTableQuery(String username) {
		String query = String.format("SELECT * FROM OFFENDERCONNECT.ACCOUNT WHERE USER_ID='%s'", username);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get correction transaction details
	 * @param : String colName, String colValue
	 * @return: List<JSONObject> correction transaction
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> executeCorrectionsTransaction(String colName, String colValue) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * from(select * from CORRECTIONS_TRANSACTION where "+colName+"='%s' order by CREATED_DTS desc) where rownum=1", colValue);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get correction facility service details
	 * @param : String colName, String colValue
	 * @return: List<JSONObject> correction facility service
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> executeGetCorrectionsFacilityService(String serviceType) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * from Corrections_facility_service where DISPLAY_NAME='%s'",serviceType);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get correction transaction item
	 * @param : String transactionId
	 * @return: List<JSONObject> correction transaction item
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> executeCorrectionsTransactionItem(String transactionId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * from CORRECTIONS_TRANSACTION_ITEM where TRANSACTION_ID='%s'", transactionId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get billing details
	 * @param : String accountNumber
	 * @return: List<JSONObject> billing details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeGetBillingId(String accountNumber) {
		String query = String.format("SELECT * from(SELECT * FROM STORED_CREDIT_CARD where ACCOUNT_NUMBER='%s' and active='1')where rownum=1",accountNumber);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get transaction token
	 * @param : String merchantorderId
	 * @return: List<JSONObject> transaction token
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeGetTransactionToken(String merchantorderId) {
		String query = String.format("SELECT * FROM OFFENDERCONNECT.CORRECTIONS_TRANSACTION_TOKENS where MERCHANT_ORDER_ID='%s'", merchantorderId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get pin debit details
	 * @param : String accountGuid
	 * @return: List<JSONObject> pin debit details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getPinDebitPayment(String accountGuid) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		List<JSONObject> resList = executePinDebitPaymentQuery(accountGuid);
		return resList;
	}

	/**
	 * @Purpose: To get pin debit payment details
	 * @param : String accountGuid
	 * @return: List<JSONObject> pin debit payment details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executePinDebitPaymentQuery(String accountGuid) {
		String query = String.format("SELECT * FROM (SELECT CHARGE_AMOUNT, TRANSACTION_SERVICE_TYPE FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE ACCOUNT_GUID = '%s' AND PAYMENT_STATUS = 'ACCEPTED' ORDER BY PAYMENT_DATE_TIME DESC) WHERE ROWNUM < 2", accountGuid);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get facilities of account details
	 * @param : String accountGuid
	 * @return: List<JSONObject> facilities of account details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeGetFacilitiesOfAccountQuery(String accountGuid) {
		String query = String.format("SELECT afl.OFFENDERCONNECTSITEID, fs.FACILITYNAME FROM OFFENDERCONNECT.ACCOUNT_FACILITY_LINK  afl\n" +
				"INNER JOIN OFFENDERCONNECT.FACILITYSERVICES fs ON fs.OFFENDERCONNECTSITEID = afl.OFFENDERCONNECTSITEID\n" +
				"INNER JOIN OFFENDERCONNECT.ACCOUNT a ON a.ACCOUNT_GUID = afl.ACCOUNT_GUID\n" +
				"WHERE afl.ACCOUNT_GUID = '%s' ", accountGuid);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get portal id return details
	 * @param : String accountNumber,String portalId
	 * @return: List<JSONObject> portal id return services
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executePortalIdReturnServices(String accountNumber,String portalId) {

		String query = String.format("SELECT distinct cfs.service_id, cfs.display_name, cstp.service_name from OFFENDERCONNECT.corrections_members cm\r\n" + 
				"inner join OFFENDERCONNECT.corrections_account_member cam on cam.member_id = cm.member_id inner join OFFENDERCONNECT.account act on act.account_guid = cam.account_id\r\n" + 
				"inner join OFFENDERCONNECT.corrections_roster_members rm on cm.member_id = rm.member_id inner join OFFENDERCONNECT.corrections_roster cr on rm.roster_id = cr.roster_id\r\n" + 
				"inner join OFFENDERCONNECT.corrections_facility_service cfs on cr.roster_id = cfs.roster_id inner join OFFENDERCONNECT.corrections_fac_service_portal cfsp on cfsp.facility_service_id = cfs.facility_service_id\r\n" + 
				"inner join OFFENDERCONNECT.corrections_service_type cstp on cfs.service_id = cstp.id left join OFFENDERCONNECT.corrections_portal cp on cfsp.portal_id = cp.portal_id\r\n" + 
				"where act.account_number = '%s' and cp.portal_id = '%s' and cfsp.fsp_active = '1' and cfs.active = '1' and cp.portal_active = '1' and cam.active = 1", accountNumber,portalId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get state's facility 
	 * @param : String state
	 * @return: List<JSONObject> facility of state
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeGetFacilitiesOfStateQuery(String state) {
		String query = String.format("SELECT * FROM offenderconnect.facilityservices facility WHERE facility.state = '%s' AND (facility.pinDebit = 'Y' OR facility.inmatePhones ='Y' OR facility.inmateBanker = 'Y' OR facility.email = 'Y' OR facility.debitlink_yn='Y') ORDER BY facility.facilityname", state);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get facility state
	 * @param : String state
	 * @return: String state
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public String getFacilityState(String facilityName) {
		String state="";
		try {
			String query = String.format("SELECT * FROM OFFENDERCONNECT.FACILITYSERVICES WHERE FACILITYNAME LIKE '%s'", "%"+facilityName+"%");

			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
			System.out.println(resList.get(0).toString());
			state=resList.get(0).get("state").toString();
		} catch (Exception e) {
			LogManager.logException(e,className, "Unable get State for Given Facility");
		}

		return state;
	}

	/**
	 * @Purpose: To get account member
	 * @param : String accountId
	 * @return: List<JSONObject> account members
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeGetAccountMembersQuery(String accountId) {
		String query = String.format("SELECT * FROM OFFENDERCONNECT.CORRECTIONS_ACCOUNT_MEMBER WHERE ACCOUNT_ID = '%s' AND ACTIVE='1'", accountId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get details from CORRECTIONS_ACCOUNT_MEMBER
	 * @param : String accountId, String principalId
	 * @return: List<JSONObject> account members
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getCorrectionsAccountMembers(String accountId, String principalId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM OFFENDERCONNECT.CORRECTIONS_ACCOUNT_MEMBER WHERE ACCOUNT_ID = '%s' and MEMBER_ID in (SELECT MEMBER_ID FROM OFFENDERCONNECT.CORRECTIONS_MEMBERS where principal_id = '%s') ", accountId, principalId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get velocity rules for corrections facility
	 * @param : String tender,String method, String facilityServiceId
	 * @return: List<JSONObject> correction velocity rules
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getCorrectionsVelocityRules(String tender,String method, String facilityServiceId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * from CORRECTIONS_VELOCITY_RULES where TENDER='%s' and METHOD='%s' and FACILITY_SERVICE_ID = '%s' and ACTIVE ='1'", tender, method, facilityServiceId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get sum of Daily, Weekly and Monthly transactions for given como principal Id
	 * @param : String tender,String method, String principalId
	 * @return: List<JSONObject> correction total transaction sum
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getCorrectionsTotalTransactionsSum(String tender,String method, String principalId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT coalesce(sum(cti.item_total_amount), 0) as total, 'monthly' as period from corrections_transaction_item cti " + 
				"inner join corrections_transaction ct on cti.transaction_id = ct.transaction_id where cti.recipient_principal_id = '"+principalId+"' and cti.facility_service_id = 24 " + 
				"and ct.status = '1' and ct.tender = '"+tender+"' and ct.method = '"+method+"' and ct.reporting_dts >= trunc(add_months(sysdate,- 1)) and ct.reporting_dts <= trunc(sysdate + 1) - 1/(24*60*60) union " + 
				"select coalesce(sum(cti.item_total_amount), 0) as total, 'weekly' as period from corrections_transaction_item cti " + 
				"inner join corrections_transaction ct on cti.transaction_id = ct.transaction_id where cti.recipient_principal_id = '"+principalId+"' and cti.facility_service_id = 24 " + 
				"and ct.status = '1' and ct.tender = '"+tender+"' and ct.method = '"+method+"' and ct.reporting_dts >= trunc(sysdate - 7) and ct.reporting_dts <= trunc(sysdate + 1) - 1/(24*60*60) union " + 
				"select coalesce(sum(cti.item_total_amount), 0) as total, 'daily' as period from corrections_transaction_item cti " + 
				"inner join corrections_transaction ct on cti.transaction_id = ct.transaction_id where cti.recipient_principal_id = '"+principalId+"' and cti.facility_service_id = 24 " + 
				"and ct.status = '1' and ct.tender = '"+tender+"' and ct.method = '"+method+"' and ct.reporting_dts >= trunc(sysdate) and ct.reporting_dts <= trunc(sysdate + 1) - 1/(24*60*60)");
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get correction AML info
	 * @param : String offenderConnectSiteId
	 * @return: List<JSONObject> correction AML information
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getCorrectionsAmlInformation(String offenderConnectSiteId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT i.STATE, i.MIN_AMOUNT,i.MAX_AMOUNT,r.REQUIRED_DATA from OFFENDERCONNECT.CORRECTIONS_AML_INFO i, OFFENDERCONNECT.CORRECTIONS_AML_DATA_REQUIRED r\r\n" + 
				"where i.STATE in (SELECT STATE from OFFENDERCONNECT.FACILITYSERVICES where OFFENDERCONNECTSITEID = '%s') and r.THRESHOLD_LEVEL = i.THRESHOLD_LEVEL", offenderConnectSiteId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To update COMO AML Info Thresholds
	 * @param : HashMap<String,Float> inpDetails,String state
	 * @return: int updated COMO AML Threshold
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public int updateComoAMLInfoThresholds(HashMap<String,Float> inpDetails,String state) {
		int updateCount=0;

		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String query = String.format("UPDATE OFFENDERCONNECT.CORRECTIONS_AML_INFO SET MIN_AMOUNT = CASE THRESHOLD_LEVEL WHEN 'T1' THEN '%s' WHEN 'T2' THEN '%s' WHEN 'T3' THEN '%s' END,MAX_AMOUNT = CASE THRESHOLD_LEVEL WHEN 'T1' THEN ('%s') WHEN 'T2' THEN ('%s') WHEN 'T3' THEN ('%s') END WHERE THRESHOLD_LEVEL in ('T1','T2','T3') AND STATE = '%s'",inpDetails.get("minAmtT1"),inpDetails.get("minAmtT2"),inpDetails.get("minAmtT3"),inpDetails.get("maxAmtT1"),inpDetails.get("maxAmtT2"),inpDetails.get("maxAmtT3"),state);
			updateCount= CNDatabases.offenderConnect().executeUpdateQuery(query);
		} catch (Exception e) {

		}
		return updateCount;
	}

	/**
	 * @Purpose: To get facility name
	 * @param : String facilityId
	 * @return: List<JSONObject> get facilities details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeGetFacilityName(String facilityId) {
		String query = String.format("SELECT * from FACILITYSERVICES where OFFENDERCONNECTSITEID ='%s'",facilityId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get members
	 * @param : String memberId
	 * @return: List<Map<String,String>>  get member details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public  List<Map<String,String>> executeGetMembersQuery(String memberId) {
		List<Map<String,String>> resList =null;
		try {
			String query = String.format("SELECT * FROM OFFENDERCONNECT.CORRECTIONS_MEMBERS where MEMBER_ID = '%s'",memberId);
			resList = getResult(idbConstants.dbName_OffenderConnect, query);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return resList;
	}

	/**
	 * @Purpose: To get auto pay id
	 * @param : String inmateId
	 * @return: List<JSONObject> get auto pay details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeAutoPayIdQuery(String inmateId){
		String query = String.format("SELECT * FROM OFFENDERCONNECT.ACCOUNT_AUTOPAY WHERE BOOKING_NUM = '%s' AND STATUS='Active'", inmateId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To update trust auto pay status active to inactive
	 * @param : String autopayId
	 * @return: List<JSONObject> get auto pay trust details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeUpdateAutoPayTrustQuery(String autopayId) {
		String query = String.format("UPDATE OFFENDERCONNECT.ACCOUNT_AUTOPAY SET STATUS ='Inactive' WHERE STATUS ='Active' AND AUTOPAY_ID='%s'", autopayId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To account auto pay details
	 * @param : String autopayId
	 * @return: List<JSONObject> get auto pay details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeAccountAutoPayQuery(String autopayId) {
		String query = String.format("SELECT * FROM OFFENDERCONNECT.ACCOUNT_AUTOPAY WHERE AUTOPAY_ID = '%s'", autopayId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get messaging transaction purchase details
	 * @param : String accountGuid
	 * @return: List<JSONObject> get messaging transaction purchase details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeMessagingTransactionPurchaseQuery(String accountGuid){
		String query = String.format("SELECT * FROM(SELECT TRANSACTION_DESCRIPTION,TRANSACTION_SERVICE_TYPE, CHARGE_AMOUNT, PAYMENT_DATE_TIME, PAYMENT_STATUS FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE ACCOUNT_GUID = '%s' AND TRANSACTION_DESCRIPTION = 'EMAIL PACKAGE PURCHASE' AND PAYMENT_STATUS = 'ACCEPTED' ORDER BY PAYMENT_DATE_TIME DESC) WHERE ROWNUM <2", accountGuid);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To update old password after change password
	 * @param : String userId, String password
	 * @return: List<JSONObject> update account password
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeUpdateAccountPasswordQuery(String userId, String password) {
		String query = String.format("UPDATE OFFENDERCONNECT.ACCOUNT SET PASSWORD ='%s' WHERE USER_ID ='%s'", password, userId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get messaging Send Email details
	 * @param : String accountGuid
	 * @return: List<JSONObject> get message send email details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private  List<JSONObject> executeMessagingSendEmailQuery(String accountGuid){
		String query = String.format("SELECT * FROM (SELECT ACCOUNT_GUID, CONTACT_ID, MESSAGE_COST FROM OFFENDERCONNECT.SENT_EMAILS ORDER BY DATE_TIME DESC) WHERE ROWNUM <2", accountGuid);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get master transaction details
	 * @param : String accountGuid, String serviceType
	 * @return: List<JSONObject> master transaction details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public synchronized  List<JSONObject> verifyMasterTransactionDetails(String accountGuid, String serviceType){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM(SELECT * FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE ACCOUNT_GUID='%s'  AND TRANSACTION_SERVICE_TYPE = '%s' AND PAYMENT_STATUS='ACCEPTED' ORDER BY PAYMENT_DATE_TIME DESC) WHERE ROWNUM=1", accountGuid, serviceType);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get corrections fee details
	 * @param : String facilityServiceId, String method, String tender, String OrderType
	 * @return: List<JSONObject> correction fee details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getCorrectionsFeeDetails(String facilityServiceId, String method, String tender, String OrderType) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM(SELECT * FROM OFFENDERCONNECT.CORRECTIONS_SERVICE_FEE where FACILITY_SERVICE_ID ='%s' and METHOD = '%s' and TENDER = '%s' ORDER BY MAX_AMOUNT %s) WHERE ROWNUM=1", facilityServiceId, method, tender, OrderType);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get stored card GUID
	 * @param : String accountNum
	 * @return: List<JSONObject> Get active stored card GUID details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeGetActiveStoredCardGuid(String accountNum) {
		String query = String.format("SELECT * from STORED_CREDIT_CARD where Account_number = '%s' and active ='1' order by CREDIT_CARD_GUID desc ", accountNum);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get declined stored card GUID
	 * @param : String accountNum, String Card last four digits
	 * @return: String Get declined stored card GUID details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public String getStoredCardGuidForCardNumber(String accountNum, String cardLastFourDigits) {
		String query = String.format("SELECT * from STORED_CREDIT_CARD where Account_number = '%s' and active ='1' and LAST_FOUR_DIGITS = '%s' order by CREDIT_CARD_GUID desc", accountNum,cardLastFourDigits);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList.get(0).getString("creditCardGuid");
	}


	/**
	 * @Purpose: To get minimum value of advance pay facility
	 * @param : String ocSiteId
	 * @return: List<JSONObject> Get declined stored card GUID details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeAdvPayFacilityMinValue(String ocSiteId) {
		String query = String.format("SELECT * FROM OFFENDERCONNECT.ADVANCE_PAY_SETTINGS WHERE OFFENDERCONNECTSITEID ='%s'",ocSiteId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get inmate GUID details
	 * @param : String inmateId
	 * @return: String Get inmate GUID details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public String executeGetInmateGuid(String inmateId) {
		//db.establishDatabaseConnection(gblConstants.dbName_IBanker);
		String query = String.format("SELECT INMATE_GUID FROM IBANKER.Inmate WHERE BOOKING_NUM='%s' and inmate_status = 'ACTIVE'",inmateId);
		List<JSONObject> resList = CNDatabases.iBanker().getQueryResponseAsJSON(query);
		return resList.get(0).getString("inmateGuid");
	}
	/**
	 * @Purpose: To get correction transaction details
	 * @param : String transactionId
	 * @return: List<JSONObject> Get correction transaction details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> executeGetCorrectionsTransactionDetails(String transactionId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM OFFENDERCONNECT.CORRECTIONS_TRANSACTION_ITEM WHERE TRANSACTION_ID = '%s'",transactionId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	/**
	 * @Purpose: To get correction depositor details
	 * @param : String transactionId
	 * @return: List<JSONObject> correction depositor details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> executeGetCorrectionsDepositorDetails(String transactionId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM OFFENDERCONNECT.CORRECTIONS_DEPOSITOR WHERE TRANSACTION_ID = '%s'",transactionId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get provider service transactions
	 * @param : String userId,String inmateId
	 * @return: List<JSONObject> provider service transaction details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeProviderServiceTransactionQuery(String userId,String inmateId) {
		// We cannot run the below Query to return everything as there is a blob that breaks the query
		String query = String.format("SELECT * FROM (SELECT * FROM OFFENDERCONNECT.PROVIDER_SERVICE_TRANSACTIONS WHERE ACCOUNT_GUID = (select ACCOUNT_GUID from account where user_id='%s') AND "
				+ "MEMBERSHIP_GUID=(SELECT MEMBERSHIP_GUID  FROM OFFENDERCONNECT.SERVICE_MEMBERSHIPS WHERE MEMBERSHIP_ID = '%s') ORDER BY PAYMENT_DATE_TIME DESC) WHERE ROWNUM =1",userId,inmateId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To get membership Guid using inmate ID for community Corrections
	 * @param : String membershipId
	 * @return: String member ship GUID
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public String executeGetMembershipGuid(String membershipId){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM (select MEMBERSHIP_GUID  from OFFENDERCONNECT.SERVICE_MEMBERSHIPS where  MEMBERSHIP_ID ='%s' ) WHERE ROWNUM =1",membershipId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		String a= resList.get(0).getString("membershipGuid").toString();
		return a;
	}

	/**
	 * @Purpose: To get membership GUID using inmate ID and provider services id for community Corrections
	 * @param : String membershipId,String providerServicesId
	 * @return: String member ship GUID
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public String executeGetMembershipGuid(String membershipId,String providerServicesId){
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM (select MEMBERSHIP_GUID  from OFFENDERCONNECT.SERVICE_MEMBERSHIPS where  MEMBERSHIP_ID ='%s' and PROVIDER_SERVICES_ID='%s' ) WHERE ROWNUM =1",membershipId,providerServicesId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		String a= resList.get(0).getString("membershipGuid").toString();
		return a;
	}

	/**
	 * @Purpose: To get values from system parameters table for provided inputs
	 * @param : String parameterId, String ocSiteId, String deviceId
	 * @return: List<JSONObject> system parameter details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	private List<JSONObject> executeSystemParameters(String parameterId, String ocSiteId, String deviceId) {
		String query = String.format("SELECT * from OFFENDERCONNECT.SYSTEMPARAMETERS where PARAMETERID ='%s' and OFFENDERCONNECTSITEID ='%s' and DEVICE_ID ='%s'",parameterId,ocSiteId,deviceId);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}

	/**
	 * @Purpose: To update time stamp of session id/Security token
	 * @author Jagadeesh, Boddeda
	 * @param String sessionId
	 * @return Returns the count of updated rows
	 */
	public int updateLastActionDateTime(String sessionId) {
		int rowAffected=0;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String query = String.format("UPDATE OFFENDERCONNECT.MOBILE_LOGINS SET LAST_ACTION_DATETIME = (SELECT Sysdate - 1 From Dual) WHERE LOGIN_TOKEN = '%s'",sessionId);
			rowAffected = CNDatabases.offenderConnect().executeUpdateQuery(query);
		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to update last action date time for provided session ID");
		}	
		return rowAffected;
	}

	/**
	 * @Purpose: To get the value from Facility Account table of OC_DB for provided column name
	 * @param : need to pass String userId, String columnName
	 * @return: String value
	 * @Author-Date: 02-14-2019
	 * @Reviewed-Date:
	 */
	public String getFacilityAdminAccountData(String userId, String columnName) {
		String value = null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			ResultSet OrcResultSet;
			String sqlQuery = "select * from FACILITYACCOUNT where USERID='" + userId + "'";
			OrcResultSet = CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			List<Map<String, String>> resList = CNDatabases.offenderConnect().getResultAsList(OrcResultSet);
			value = resList.get(0).get(columnName);
		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get value for "+columnName+" from database");
		}
		return value;
	}


	/**
	 * @Purpose: To get the WIR Fee
	 * @param : need to pass String FacilityName, String ServiceType
	 * @return: String WIRFee
	 * @AuthorName: Bhaskar Surisetti
	 * @AuthorDate: 02-28-2019
	 * @ReviewedDate:
	 */
	public synchronized String getWIRFee(String facilityName, String serviceType) {
		Float totalFee = null; 
		Float wirFee = null;
		Float vendorFee=null;
		String fee=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			ResultSet OrcResultSet;
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.WIR_FEE where service_id in (SELECT ID FROM OFFENDERCONNECT.CORRECTIONS_SERVICE_TYPE where SERVICE_NAME = '"+serviceType+"') and OC_SITE_ID in (SELECT OFFENDERCONNECTSITEID FROM OFFENDERCONNECT.FACILITYSERVICES where FACILITYNAME LIKE '%"+facilityName+"%')";
			OrcResultSet = CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			List<Map<String, String>> resList = CNDatabases.offenderConnect().getResultAsList(OrcResultSet);

			//get WIR FEE and VENDOR FEE from DB OFFENDERCONNECT.WIR_FEE table
			wirFee = Float.parseFloat(resList.get(0).get("WIR_FEE"));
			vendorFee = Float.parseFloat(resList.get(0).get("VENDOR_FEE"));

			//sum of WIR and Vendor fee
			totalFee = BigDecimal.valueOf(Float.sum(wirFee , vendorFee)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();
			fee=totalFee.toString();

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get value for from database");
		}
		return fee;
	}

	/**
	 * @Purpose: To get the Facility ID/OC Site ID
	 * @param : need to pass String FacilityName
	 * @return: String facilityId
	 * @AuthorName: Bhaskar Surisetti
	 * @AuthorDate: 02-28-2019
	 * @ReviewedDate:
	 */
	public String getFacilityId(String facilityName) {
		String facilityId=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			ResultSet OrcResultSet;
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.FACILITYSERVICES where FACILITYNAME LIKE '%"+facilityName+"%'";
			OrcResultSet = CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			List<Map<String, String>> resList = CNDatabases.offenderConnect().getResultAsList(OrcResultSet);			

			//get OFFENDERCONNECTSITEID 
			facilityId =resList.get(0).get("OFFENDERCONNECTSITEID");

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get value for from database");
		}
		return facilityId;
	}

	/**
	 * @Purpose: To generate testObj.report log for Master transaction 
	 * @param : need to pass List<JSONObject> for DB results, String FunctionalityName, String ServiceType, String depositeAmount,String phoneNo, String InmateId
	 * @AuthorName: Bhaskar Surisetti
	 * @AuthorDate: 03-20-2019
	 * @ReviewedDate:
	 */
	public synchronized void verifyMasterTransactionForAPI(List<JSONObject> resList,String functionalityName,String serviceType,String depositAmount,String phoneNo,String inmateGuid,AbstractTestEngine testObj) {

		String actualServiceTypeDB=resList.get(0).getString("transactionServiceType").toString();
		String actualAmountDB=null;

		testObj.reportStatus(actualServiceTypeDB.equalsIgnoreCase(serviceType), "Successfully verified service type in DB "+actualServiceTypeDB+ " matched with expected service type "+serviceType,
				"Failed to verify Service type in DB "+actualServiceTypeDB+ " matched with expected service type "+serviceType);

		if(functionalityName.equalsIgnoreCase("AdvancePay")) {
			String actualPhoneNoDB=resList.get(0).getString("phoneNumber").toString();

			testObj.reportStatus(actualPhoneNoDB.equalsIgnoreCase(phoneNo), "Successfully verified for Phone Number in DB "+actualPhoneNoDB+ " matched with expected phone number "+phoneNo,
					"Failed to verify Phone Number in DB "+actualPhoneNoDB+ " matched with expected phone number "+phoneNo);
		}

		else if(functionalityName.equalsIgnoreCase("Pindebit") && !serviceType.equals("PCS_PINDEBIT"))
		{
			String actualInmateId=resList.get(0).getString("inmateId").toString();
			testObj.reportStatus(actualInmateId.equalsIgnoreCase(inmateGuid), "Successfully verified for InmateId in DB "+actualInmateId+ " matched with expected InmateId "+inmateGuid,
					"Failed to verify InmateId in DB "+actualInmateId+ " matched with expected InmateId "+inmateGuid);

		}else if(!functionalityName.equalsIgnoreCase("Pindebit") && !functionalityName.equalsIgnoreCase("CommunityCorrections")) {
			String actualInmateGuid=resList.get(0).getString("inmateGuid").toString();
			testObj.reportStatus(actualInmateGuid.equalsIgnoreCase(inmateGuid), "Successfully verified for InmateGuid in DB "+actualInmateGuid+ " matched with expected InmateGuid "+inmateGuid,
					"Failed to verify InmateGuid in DB "+actualInmateGuid+ " matched with expected InmateGuid "+inmateGuid);
		}
		if(functionalityName.equalsIgnoreCase("DebitLInk") || functionalityName.equalsIgnoreCase("TrustFund") || functionalityName.equalsIgnoreCase("CommunityCorrections")) {
			//get credit amount
			actualAmountDB = resList.get(0).getString("creditAmount").toString();

		}else {
			//get charge amount
			actualAmountDB = resList.get(0).getString("chargeAmount").toString();
		}
		testObj.reportStatus(actualAmountDB.equalsIgnoreCase(depositAmount), "Successfully verified for deposit amount in DB "+actualAmountDB+ " matched with expected deposit amount "+depositAmount,
				"Failed to verify Deposit Amount in DB "+actualAmountDB+ " matched with expected deposit amount "+depositAmount);

	}


	/**
	 * @Purpose: To generate testObj.report log for provider service transactions 
	 * @param : need to pass List<JSONObject> for DB results, String depositAmount, String membershipGuid, testObj
	 * @AuthorName: Jagadeesh Boddeda
	 * @AuthorDate: 03-27-2019
	 * @ReviewedDate:
	 */
	public synchronized void verifyProviderServiceTransactions(List<JSONObject> resList,String depositAmount,String membershipGuid, AbstractTestEngine testObj){

		String actualDepositAmountDB=resList.get(0).getString("netAmount").toString();
		String actualMembershipGuid=resList.get(0).getString("membershipGuid").toString();

		testObj.reportStatus(membershipGuid.equals(actualMembershipGuid), "Successfully verified Membership Guid in DB "+actualMembershipGuid+ " matched with expected Membership Guid "+membershipGuid,
				"Failed to verify Membership Guid in DB "+actualMembershipGuid+ " matched with expected Membership Guid "+membershipGuid);

		testObj.reportStatus(actualDepositAmountDB.equalsIgnoreCase(depositAmount), "Successfully verified deposit Amount in DB "+actualDepositAmountDB+ " matched with expected deposit Amount "+depositAmount,
				"Failed to verify deposit Amount in DB "+actualDepositAmountDB+ " matched with expected deposit Amount "+depositAmount);

	}

	/**
	 * @Purpose: To get the value from Account table of OC_DB for provided column name
	 * @param : need to pass String type userId, String columnName
	 * @return: String value
	 * @Author-Date: 01-01-2019
	 * @Reviewed-Date:
	 */
	public String getAccountData(String userId, String columnName) {
		String value = null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			ResultSet OrcResultSet;
			String sqlQuery = "select * from ACCOUNT where USER_ID='" + userId + "'";
			OrcResultSet = CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			List<Map<String, String>> resList = CNDatabases.offenderConnect().getResultAsList(OrcResultSet);
			value = resList.get(0).get(columnName);
		} catch (Exception e) {
			LogManager.logException(e, className,"Exception to get value for "+columnName+" from database");
		}
		return value;
	}


	/**
	 * @Purpose: To get Result List from DB
	 * @param : need to pass String type dbName and dbQuery
	 * @return: List<String,String>
	 * @Author-Date: 01-01-2019
	 * @Reviewed-Date:
	 */
	public List<Map<String, String>> getResultList(String dbName,String dbQuery) {
		List<Map<String, String>> resList = new ArrayList<Map<String, String>>();
		ResultSet OrcResultSet =null;
		try {
			DatabaseVerification dbConnection = CNDatabases.DB.valueOf(dbName).getConnection();
			//db.establishDatabaseConnection(dbName);
			OrcResultSet = dbConnection.getQueryResponse(dbQuery);
			resList = dbConnection.getResultAsList(OrcResultSet);
		} catch (Exception e) {
			LogManager.logException(e, className,
					"Exception to get Result list from database");
		}
		return resList;
	}

	/**
	 * @Purpose: To delete advance pay number using user name
	 * @param : String userName
	 * @return: int count of deleted advance pay numbers
	 * @Author-Date: 01-01-2019
	 * @Reviewed-Date:
	 */
	public synchronized int removeAdvancePayNo(String userName) {
		int removedNo=0;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery = "delete from ITI_PHONE where ACCOUNT_NUMBER in (select ACCOUNT_NUMBER from account where USER_ID ='"+ userName + "')";
			removedNo= CNDatabases.offenderConnect().executeUpdateQuery(sqlQuery);
			LogManager.logInfo(className, "Successfully removed "+removedNo+" AdvancePay Numbers");
		} catch (Exception e) {
			LogManager.logException(e,className, "Exception to execute remove AdvancePay Numbers");
		}
		return removedNo;					
	}


	/**
	 * @Purpose: To get advance pay phone numbers
	 * @param : need to pass String country US/non-US
	 * @return: List<JSONObject>  advance pay numbers
	 * @AuthorDate: 08-29-2019
	 * @ReviewedDate:
	 */
	public List<JSONObject> getAdvPayNumbers(String country) {
		List<JSONObject> resList = null;
		String sqlQuery= "";
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			if(country.equalsIgnoreCase("US")) {

				sqlQuery="SELECT * FROM (SELECT * FROM OFFENDERCONNECT.ITI_PHONE WHERE LENGTH(PHONE_NUMBER)=10 AND PHONE_NUMBER NOT LIKE '0%') WHERE ROWNUM = 1";

			}else {
				sqlQuery="SELECT * FROM (SELECT * FROM OFFENDERCONNECT.ITI_PHONE WHERE PHONE_NUMBER LIKE '01191%' AND LENGTH(PHONE_NUMBER)=15) WHERE ROWNUM = 1";
			}
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get Advance Pay Numbers");
		}
		return resList;
	}




	/**
	 * @Purpose: To reset transactions for Trust fund in Ibanker.Deposits table
	 * @param :  String facility, String inmateId
	 * @return:  int count of updated reset trust transactions
	 * @Author-Date: 5-02-2019
	 * @Reviewed-Date:
	 */	
	public int resetTrustTransactionsInDB(String facility, String inmateId,String userName) {
		int rowUpdated=0;

		String sqlQuery= "update IBANKER.DEPOSIT set Date_Time =(Select Sysdate - 40 From Dual) WHERE Inmate_Guid = (select INMATE_GUID from inmate where site_key = (select SITE_KEY from ibanker.facility where NAME like '%FacilityName%') and booking_num like'%InmateID%') and email = (select EMAIL_ADDR from OFFENDERCONNECT.ACCOUNT where USER_ID = 'UserName') and date_time BETWEEN sysdate - INTERVAL '8' DAY AND sysdate";

		try {

			// Database object creation
			//db.establishDatabaseConnection(gblConstants.dbName_IBanker);
			sqlQuery = sqlQuery.replace("FacilityName", facility.subSequence(0, 4)).replace("InmateID", inmateId).replace("UserName", userName);
			rowUpdated = CNDatabases.iBanker().executeUpdateQuery(sqlQuery);

		}catch (Exception e) {
			LogManager.logException(e,className,"Exception to Updated trust fund transactions");
		}
		return rowUpdated;
	}


	/**
	 * @Purpose: To update row in a table 
	 * @author : Bhaskar Soorisetti
	 * @param :  String dbName, String tableName,String setTblColName,String setValData,String condTblColName,String condColData
	 * @return:  Int rowAffacted
	 * @Author-Date: 03-05-2019
	 * @Reviewed-Date:
	 */	
	public int executeUpdateQuery(String dbName,String tableName,String setValColName,String setValData,String condColName,String condColData)
			throws Exception {
		int rowAffected=0;
		String strUpdateStatement="";
		String sqlQuery= "Update tableName SET setValColName='setValData' where condColName='condColData'";
		try {
			strUpdateStatement=sqlQuery.replace("tableName",tableName).replace("setValColName", setValColName).replace("setValData", setValData).replace("condColName", condColName).replace("condColData", condColData);
			DatabaseVerification dbConnection = CNDatabases.DB.valueOf(dbName).getConnection();
			//db.establishDatabaseConnection(dbName);
			rowAffected=dbConnection.executeUpdateQuery(strUpdateStatement);			
		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to Updated values on table "+ tableName +" for db"+dbName);
		}		
		return rowAffected;
	}

	/**
	 * @Purpose: To update row in a table 
	 * @author : Bhaskar Soorisetti
	 * @param :  String dbName, String SqlQuery
	 * @return:  Int rowAffacted
	 * @Author-Date: 03-05-2019
	 * @Reviewed-Date:
	 */	
	public int executeUpdateQuery(String dbName,String sqlQuery)
			throws Exception {
		int rowAffected=0;
		try {			
			DatabaseVerification dbConnection = CNDatabases.DB.valueOf(dbName).getConnection();
			//db.establishDatabaseConnection(dbName);
			rowAffected=dbConnection.executeUpdateQuery(sqlQuery);			
		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to Updated values on table ");
		}		
		return rowAffected;
	}
	/**
	 * @Purpose: To get email content
	 * @param :  String userName
	 * @return:  String email content
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public synchronized String getEmailContent(String userName) {
		String emailContent="";
		try {
			// Database object creation
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String query="select * from (SELECT  * FROM Offenderconnect.Email_Engine where RECIPIENT = (select EMAIL_ADDR from Offenderconnect.ACCOUNT where USER_ID ='"+userName+"'  ) order by ORIG_DATE_TIME desc) where ROWNUM = 1";

			// Read the BLOB email content and publish to test results
			emailContent = CNDatabases.offenderConnect().getBlobData(CNDatabases.offenderConnect().getQueryResponse(query), "TEXT");

		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to get email content");
		}
		return emailContent;
	}
	/**
	 * @Purpose: To get WIR Fee details
	 * @param :  String serviceType,String Facility
	 * @return:  List<Map<String, String>> WIR Fee details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public List<Map<String, String>> getWIRFeeTbl(String serviceType,String Facility){
		List<Map<String, String>> resList=null;
		try {
			// Database object creation
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.WIR_FEE where service_id in (SELECT ID FROM OFFENDERCONNECT.CORRECTIONS_SERVICE_TYPE where SERVICE_NAME = '"+serviceType+"') and OC_SITE_ID in (SELECT OFFENDERCONNECTSITEID FROM OFFENDERCONNECT.FACILITYSERVICES where FACILITYNAME like '%"+Facility+"%')";
			ResultSet OrcResultSet = CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			resList = CNDatabases.offenderConnect().getResultAsList(OrcResultSet);

		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to get WIR fee");
		}
		return resList;
	}
	/**
	 * @Purpose: To get EMS credit fee details
	 * @return:  List<Map<String, String>> EMS credit fee details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<Map<String, String>> getEmsCreditFee(){
		List<Map<String, String>> resList=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_EMS);
			ResultSet OrcResultSet;
			String emsCreditFeeQuery = "select * from (select TRANSACTIONAMOUNT from ACCOUNTTRANSACTION where TRANSACTIONTYPE='CREDIT' And POSTEDDATETIME Between (Select Sysdate - INTERVAL '180' second From Dual) And (Select Sysdate - 0 From Dual) Order By POSTEDDATETIME desc)where rownum=1";	
			OrcResultSet = CNDatabases.ems().getQueryResponse(emsCreditFeeQuery);
			resList = CNDatabases.ems().getResultAsList(OrcResultSet);
		} catch (Exception e) {

			LogManager.logException(e,className,"Exception to get EMS Credit fee");
		}
		return resList;
	}
	/**
	 * @Purpose: To get EMS debit fee details
	 * @return:  List<Map<String, String>> EMS debit fee details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public List<Map<String, String>> getEmsDebitFee(){
		List<Map<String, String>> resList=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_EMS);
			ResultSet OrcResultSet;
			String emsDebitFeeQuery = "select * from (select TRANSACTIONAMOUNT from ACCOUNTTRANSACTION where TRANSACTIONTYPE='DEBIT' And POSTEDDATETIME Between (Select Sysdate - INTERVAL '180' second From Dual) And (Select Sysdate - 0 From Dual) Order By POSTEDDATETIME desc)where rownum=1";	
			OrcResultSet = CNDatabases.ems().getQueryResponse(emsDebitFeeQuery);
			resList = CNDatabases.ems().getResultAsList(OrcResultSet);
		} catch (Exception e) {

			LogManager.logException(e,className,"Exception to get EMS Debit fee");
		}
		return resList;
	}
	/**
	 * @Purpose: To get transaction details
	 * @param : String userName,String serviceType
	 * @return:  List<Map<String, String>> transaction details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public List<Map<String, String>> getTransactionDetails(String userName,String serviceType){
		List<Map<String, String>> resList=null;
		try {
			String accountGuId=getAccountGuidFromAccountTable(userName);
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery = "SELECT * FROM(SELECT * FROM OFFENDERCONNECT.TRANSACTIONS WHERE ACCOUNTGUID = '"
					+ accountGuId + "' AND SERVICETYPE = '" + serviceType
					+ "' AND DATETIMEPAYMENTMADE Between (Select Sysdate - INTERVAL '180' second From Dual) And (Select Sysdate - 0 From Dual)  ORDER BY DATETIMEPAYMENTMADE DESC) WHERE ROWNUM=1";
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			ResultSet OrcResultSet;
			OrcResultSet=CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			resList=CNDatabases.offenderConnect().getResultAsList(OrcResultSet);

		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to get Transaction table details");
		}
		return resList;
	}
	/**
	 * @Purpose: To get master transaction details
	 * @param : String userName,String serviceType
	 * @return:  List<Map<String, String>> master transaction details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public List<Map<String, String>> getMasterTransactionDetails(String userName,String serviceType){
		List<Map<String, String>> resList=null;
		try {
			String accountGuId=getAccountGuidFromAccountTable(userName);
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery = "SELECT * FROM(SELECT * FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE ACCOUNT_GUID = '"+ accountGuId + "' AND TRANSACTION_SERVICE_TYPE = '" + serviceType
					+ "' AND PAYMENT_DATE_TIME Between (Select Sysdate - INTERVAL '180' second From Dual) And (Select Sysdate - 0 From Dual)  ORDER BY PAYMENT_DATE_TIME DESC) WHERE ROWNUM=1";
			ResultSet OrcResultSet;
			OrcResultSet=CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			resList=CNDatabases.offenderConnect().getResultAsList(OrcResultSet);

		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to get Master Transaction table details");
		}
		return resList;
	}
	/**
	 * @Purpose: To get date format from master transaction details
	 * @param : String userName,String serviceType,String dateFormat
	 * @return:  String  date format for master transaction details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public String getDateFormatFromMasterTransaction(String userName,String serviceType,String dateFormat) {
		String date="";
		try {
			String accountGuId=getAccountGuidFromAccountTable(userName);
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery = "SELECT * FROM(SELECT * FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE ACCOUNT_GUID = '"+ accountGuId + "' AND TRANSACTION_SERVICE_TYPE = '" + serviceType
					+ "' AND PAYMENT_DATE_TIME Between (Select Sysdate - INTERVAL '180' second From Dual) And (Select Sysdate - 0 From Dual)  ORDER BY PAYMENT_DATE_TIME DESC) WHERE ROWNUM=1";
			date =readDateColumn(CNDatabases.offenderConnect().getQueryResponse(sqlQuery),"PAYMENT_DATE_TIME",dateFormat);	
		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to get Transaction table details");
		}
		return date;

	}
	/**
	 * @Purpose: To enable/disable round up of facility service
	 * @param : String facility,String roundupStatusCode
	 * @return:  String  date format for master transaction details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public int enableOrDisbleRoundUpTable(String facility,String roundupStatusCode) throws Exception {
		int rowAffected=0;
		String sqlQuery="UPDATE FACILITYSERVICES Set SHOW_ROUND_UP='"+roundupStatusCode+"' WHERE OFFENDERCONNECTSITEID in (SELECT OFFENDERCONNECTSITEID FROM OFFENDERCONNECT.FACILITYSERVICES where FACILITYNAME LIKE '%"+facility+"%' )";
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			rowAffected=CNDatabases.offenderConnect().executeUpdateQuery(sqlQuery);			
		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to Updated values on Facility Service table ");
		}		
		return rowAffected;
	}
	/**
	 * @Purpose: To enable/disable WIR payment
	 * @param : String facility,String serviceName,String statusCode
	 * @return:  int  count of enabled/disabled WIR payment
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public int enableOrDisbleWIRPayment(String facility,String serviceName,String statusCode) throws Exception {
		int rowAffected=0;
		String sqlQuery="UPDATE OFFENDERCONNECT.SERVICE_OPTION_CONFIG set enabled = '"+statusCode+"' where OCSITEKEY = (select OFFENDERCONNECTSITEID from facilityservices where facilityname like '%"+facility+"%') and service_type = (SELECT ID FROM OFFENDERCONNECT.CORRECTIONS_SERVICE_TYPE where SERVICE_NAME = '"+serviceName+"')";
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			rowAffected=CNDatabases.offenderConnect().executeUpdateQuery(sqlQuery);			
		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to Updated values on SERVICE_OPTION_CONFIG table ");
		}		
		return rowAffected;
	}
	/**
	 * @Purpose: To enable/disable WIR payment by serviceType
	 * @param : String serviceType,String statusCode
	 * @return:  boolean  enabled/disabled WIR payment
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public boolean enableOrDisbleWIRPaymentByServiceType(String serviceType,String statusCode) throws Exception {
		boolean blRes =false;
		String activeStatus =null;
		;   	String sqlQuery=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			sqlQuery="UPDATE OFFENDERCONNECT.SERVICE_OPTION_CONFIG set enabled = '"+statusCode+"' where  service_type = '"+serviceType+"'";
			CNDatabases.offenderConnect().getQueryResponse(sqlQuery);
			sqlQuery="SELECT ENABLED FROM OFFENDERCONNECT.SERVICE_OPTION_CONFIG WHERE service_type ='"+serviceType+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			activeStatus=resList.get(0).get("enabled").toString();
			if(statusCode.equalsIgnoreCase("1")) {
				blRes=(activeStatus.equalsIgnoreCase("1"));
			}else {
				blRes=(activeStatus.equalsIgnoreCase("0"));
			}
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to set  member ID to active/deactive");
		}
		return blRes;
	}

	/**
	 * @Purpose: To get results as list
	 * @author Satya, Gajula
	 * @param : ResultSet rs
	 * @return:  List<Map<String, String>>  results as list
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<Map<String, String>> getResultAsList(ResultSet rs) {
		List<Map<String, String>> resultSet = new ArrayList<Map<String, String>>();
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				Map<String, String> rowData= new HashMap<String, String>();
				for (Integer i = 1; i <= rsmd.getColumnCount(); i++) {
					rowData.put(rsmd.getColumnLabel(i), rs.getString(i));
				}
				resultSet.add(rowData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultSet;
	}
	/**
	 * @Purpose: To get results as list
	 * @param : String dbName,String sqlQuery
	 * @return:  List<Map<String, String>>  results as list
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<Map<String, String>> getResult(String dbName,String sqlQuery){
		List<Map<String, String>> resList=null;
		try {
			DatabaseVerification dbConnection = CNDatabases.DB.valueOf(dbName).getConnection();
			//db.establishDatabaseConnection(dbName);
			ResultSet OrcResultSet;
			OrcResultSet= dbConnection.getQueryResponse(sqlQuery);
			resList= dbConnection.getResultAsList(OrcResultSet);

		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to get result table details");
		}
		return resList;
	}

	/**
	 * @author Jagadeesh, Boddeda
	 * @param Result set object and Column name of BLOB
	 * @return Returns the blob data as String
	 * @throws Exception
	 */
	public synchronized String getBlobData(ResultSet OrcResultSet, String columnName) {
		String blobData = null;
		try {
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024*5]; //memory allocated
			while (OrcResultSet.next()) {
				InputStream inpStream = OrcResultSet.getBinaryStream(columnName);
				while (inpStream.read(buffer) > 0) {
					outStream.write(buffer,0,buffer.length);
				}
				blobData = new String(outStream.toByteArray(),Charset.defaultCharset()).replace("<br />", "").replace("<br/>", "");
			} 
			outStream.close();
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get Blob  data");
		}
		return blobData;
	}
	/**
	 * @Purpose: To get Query Response as ResultSet
	 * @param : String dbName,String sqlQuery
	 * @return:  ResultSet  result set
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public ResultSet getQueryResponse(String dbName,String sqlQuery) throws Exception {
		ResultSet rs=null;
		try {
			DatabaseVerification dbConnection = CNDatabases.DB.valueOf(dbName).getConnection();
			//db.establishDatabaseConnection(dbName);			
			rs=dbConnection.getQueryResponse(sqlQuery);
		}catch (Exception e) {
			LogManager.logException(e, className, "Exception to get response");
		}
		return rs;
	}


	/**
	 * @author Jagadeesh, Boddeda
	 * @param Result set object, Column name of Date field and required Date format
	 * @return Returns the String date as per the input format
	 * @throws Exception
	 */
	public String readDateColumn(ResultSet OrcResultSet, String ColumnName, String DateFormat) {
		String newDate = null;
		try {
			OrcResultSet.next();
			Date date = OrcResultSet.getDate(ColumnName);
			DateFormat dateFormat = new SimpleDateFormat(DateFormat);
			newDate = dateFormat.format(date);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return newDate;
	}

	/**
	 * @author Jagadeesh, Boddeda
	 * @param String facility, String service name, int status 1 or 0
	 * @return Returns the count of updated rows
	 * @throws Exception
	 */
	public int enableOrDisableServiceOfFacility(String facility,String serviceName,String status) throws Exception {
		int rowAffected=0;
		String sqlQuery="update OFFENDERCONNECT.FACILITYSERVICES set "+serviceName+" = '"+status+"' where FACILITYNAME like '%"+facility+"%'";
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			rowAffected=CNDatabases.offenderConnect().executeUpdateQuery(sqlQuery);	

		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to enable or disable "+serviceName+" in Facility Services table ");
		}		
		return rowAffected;
	}

	/**
	 * @purpose : to get Trust Token
	 * @return Trusted Token
	 * @author E005111(Vamshi Krishna Voddeti)
	 */
	public String getTrustedToken() {
		String trustedtoken=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT TRUSTED_TOKEN FROM OFFENDERCONNECT.CORRECTIONS_TRUSTED_TOKENS WHERE ACTIVE=1 FETCH FIRST ROW ONLY";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			trustedtoken=resList.get(0).get("trustedToken").toString();
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get Trusted Token info");
		}
		return trustedtoken;
	}

	/**
	 * @purpose : to get visitation id
	 * @param : String providerServicesId 
	 * @return visitation ID
	 * @author E005111(Vamshi Krishna Voddeti)
	 */
	public String getVisitationId(String providerServicesId) {
		String visitationid=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="select  m.MEMBERSHIP_ID from offenderconnect.service_memberships m left join\r\n" + 
					"(select max (t.payment_date_time) as last_payment_date_time, membership_guid from offenderconnect.provider_service_transactions t where t.payment_status = 'ACCEPTED' group by t. membership_guid) z\r\n" + 
					"on m.membership_guid = z.membership_guid\r\n" + 
					"inner join offenderconnect.provider_services ps on ps.provider_services_id = m.provider_services_id\r\n" + 
					"where m.provider_services_id = "+providerServicesId+" and status = 'ACTIVE' and last_payment_date_time is NULL";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			visitationid=resList.get(0).get("membershipId").toString();
			if(visitationid.startsWith("00")) {
				visitationid=visitationid.substring(2,visitationid.length());
			}
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get Trusted Token info");
		}
		return visitationid;
	}
	/**
	 * @Purpose: To get the Vendor Fee
	 * @param : need to pass String ServiceType
	 * @return: String Vendor Fee
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 06-20-2019
	 * @ReviewedDate:
	 */
	public synchronized String getVendorFee(String serviceType) {
		String vendorFee=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT VENDOR_FEE FROM OFFENDERCONNECT.WIR_FEe WHERE SERVICE_ID='"+serviceType+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			vendorFee=resList.get(0).get("vendorFee").toString();

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get vendor fee value  from database");
		}
		return vendorFee;
	}

	/**
	 * @Purpose: To get the WIR Fee
	 * @param : need to pass String ServiceType
	 * @return: String WIR Fee
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 06-20-2019
	 * @ReviewedDate:
	 */
	public synchronized String getWIRFee(String serviceType) {
		String wirFee=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT WIR_FEE FROM OFFENDERCONNECT.WIR_FEe WHERE SERVICE_ID='"+serviceType+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			wirFee=resList.get(0).get("wirFee").toString();

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get wirFee value  from database");
		}
		return wirFee;
	}

	/**
	 * @Purpose: To get the Total Fee
	 * @param : need to pass String ServiceType
	 * @return: String Total Fee
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 06-20-2019
	 * @ReviewedDate:
	 */
	public synchronized String getTotalFee(String serviceType) {
		Float wirFee;
		Float vendorFee;
		Float fee;
		String totalfee=null;
		try {
			//get WIR FEE and VENDOR FEE from DB OFFENDERCONNECT.WIR_FEE table
			wirFee = Float.parseFloat(getWIRFee(serviceType));
			vendorFee = Float.parseFloat(getVendorFee(serviceType));

			//sum of WIR and Vendor fee
			fee = BigDecimal.valueOf(Float.sum(wirFee , vendorFee)).setScale(2,BigDecimal.ROUND_HALF_UP).floatValue();
			totalfee=fee.toString();

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get Total Fee value  from database");
		}
		return totalfee;
	}

	/**
	 * @Purpose: To get the Payment Amount or Fee
	 * @param : String paramvalue,String providerServicesId
	 * @return: String Vendor Fee
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 06-20-2019
	 * @ReviewedDate:
	 */
	public  String getPaymentAmountOrFee(String paramvalue,String providerServicesId) {
		String paymentAmount=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT PARAMETER_VALUE FROM OFFENDERCONNECT.SERVICE_SYSTEM_PARAMETERS WHERE PARAMETER_ID = '"+paramvalue+"' AND PROVIDER_SERVICES_ID = '"+providerServicesId+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			paymentAmount=resList.get(0).get("parameterValue").toString();

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get payment Amount value  from database");
		}
		return paymentAmount;
	}

	/**
	 * @Purpose: To get the Payment Amount
	 * @param : need to pass String ServiceType
	 * @return: String Vendor Fee
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 06-20-2019
	 * @ReviewedDate:
	 */
	public  List<JSONObject> getUSSOAccountDetails(String param,String paramvalue) {
		List<JSONObject> accDetails=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_USSO);
			//String sqlQuery="SELECT * FROM usso.users";
			String sqlQuery="SELECT * FROM usso.users where "+param+" = '"+paramvalue+"'";
			accDetails = CNDatabases.usso().getQueryResponseAsJSON(sqlQuery);

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get payment Amount value  from database");
		}
		return accDetails;
	}

	/**
	 * @Purpose: To get account details from USSO Users table
	 * @param :  String dbName, String SqlQuery
	 * @return:  ResultSet
	 * @Author-Date: Jagadeesh Boddeda; 19-06-2019
	 * @Reviewed-Date:
	 */	
	public List<Map<String, String>> getUssoUsersAccountData(String email) {
		List<Map<String, String>> resList = null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_USSO);
			String query = String.format("SELECT * FROM usso.users where email='%s'", email);
			Thread.sleep(idbConstants.defSleepTime*2); //added sleep as record insertion is taking time
			ResultSet OrcResultSet = CNDatabases.usso().getQueryResponse(query);
			resList = CNDatabases.usso().getResultAsList(OrcResultSet);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get details from USSO Users table");
		}
		return resList;
	}

	/**
	 * @purpose : to get visitation id
	 * @param : String providerServicesId
	 * @return :  visitation ID
	 * @author : E005111(Vamshi Krishna Voddeti)
	 */
	public Map<String,String> getVisitationDetails(String providerServicesId) {
		String visitationid=null;
		String firstName=null;
		String lastName=null;
		String inmateId=null;
		String membershipGUID=null;
		Map<String,String> visitorDetails=new LinkedHashMap<String,String>();
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="select  m.FIRST_NAME,m.LAST_NAME,m.MEMBERSHIP_ID,m.SECOND_MEMBERSHIP_ID,m.MEMBERSHIP_GUID from offenderconnect.service_memberships m left join\r\n" + 
					"(select max (t.payment_date_time) as last_payment_date_time, membership_guid from offenderconnect.provider_service_transactions t where t.payment_status = 'ACCEPTED' group by t. membership_guid) z\r\n" + 
					"on m.membership_guid = z.membership_guid\r\n" + 
					"inner join offenderconnect.provider_services ps on ps.provider_services_id = m.provider_services_id\r\n" + 
					"where m.provider_services_id = "+providerServicesId+" and status = 'ACTIVE' and last_payment_date_time is NULL";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			visitationid=resList.get(0).get("membershipId").toString();
			if(visitationid.startsWith("00")) {
				visitationid=visitationid.substring(2,visitationid.length());
			}
			firstName=resList.get(0).get("firstName").toString();
			lastName=resList.get(0).get("lastName").toString();
			inmateId=resList.get(0).get("secondMembershipId").toString();
			membershipGUID=resList.get(0).get("membershipGuid").toString();
			if(inmateId.startsWith("00")) {
				inmateId=inmateId.substring(2,inmateId.length());
			}
			visitorDetails.put("visitorId", visitationid);
			visitorDetails.put("inmateId", inmateId);
			visitorDetails.put("firstName", firstName);
			visitorDetails.put("lastName", lastName);
			visitorDetails.put("membershipGUID", membershipGUID);

		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get Trusted Token info");
		}
		return visitorDetails;
	}

	/**
	 * @Purpose: To reset visitation transaction
	 * @param : need to pass String membershipGUID, providerservicesId, fee and amount
	 * @return: boolean 
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 06-24-2019
	 * @ReviewedDate:
	 */
	public  boolean resetTransaction(String providerServicesId, String membershipGUID,String amount, String fee) {
		boolean res=false;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			List<JSONObject> resList = executeUpdatePaymentstausQuery(providerServicesId, membershipGUID,amount,fee);
			int updCount = resList.get(0).getInt("UpdatedRows");
			if(updCount > 0) {
				res=true;
			}

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to reset the transaction from database");
		}
		return res;
	}

	/**
	 * @Purpose: To update payment status
	 * @param : need to pass String membershipGUID, providerservicesId, fee and amount
	 * @return: boolean 
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 06-24-2019
	 * @ReviewedDate:
	 */
	private List<JSONObject> executeUpdatePaymentstausQuery(String providerServicesId, String membershipGUID,String amount, String fee ) {
		String query = String.format("UPDATE OFFENDERCONNECT.PROVIDER_SERVICE_TRANSACTIONS SET ACH_STATUS ='', PAYMENT_STATUS='', PAYMENT_DATE_TIME='' WHERE ACH_STATUS='APPLIED' AND PAYMENT_STATUS='ACCEPTED' AND PROVIDER_SERVICES_ID ='%s' AND MEMBERSHIP_GUID='%s' AND  NET_AMOUNT='%s' AND FEE_AMOUNT='%s' ", providerServicesId,membershipGUID,amount,fee );
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}


	/**
	 * @purpose : to get Facility Service Details
	 * @param : need to pass String serviceId, String facilityId
	 * @return facility service details
	 * @author E005111(Vamshi Krishna Voddeti)
	 */
	public Map<String,String> getFacilityServiceDetails(String serviceId, String facilityId) {
		Map<String,String> facilityService=new LinkedHashMap<String,String>();
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.CORRECTIONS_FACILITY_SERVICE WHERE SERVICE_ID='"+serviceId+"' AND FACILITY_ID='"+facilityId+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);

			facilityService.put("hasAmountDue", resList.get(0).get("hasAmountDue").toString());
			facilityService.put("amountOwed", resList.get(0).get("amountOwed").toString());
			facilityService.put("serviceName", resList.get(0).get("serviceName").toString());

		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get Facility Service Details");
		}
		return facilityService;
	}

	/**
	 * @Purpose: To enable or disable Facility Service Amount Due column in DB
	 * @param : int enable or disable , String service Id , String facility Id
	 * @return: boolean 
	 * @AuthorDate: 07-14-2019
	 * @ReviewedDate:
	 */
	public  boolean updateFacilityServiceAmountDue(int flag,String serviceId, String facilityId) {
		boolean res=false;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String query = String.format("UPDATE OFFENDERCONNECT.CORRECTIONS_FACILITY_SERVICE SET HAS_AMOUNT_DUE ='"+flag+"' WHERE  SERVICE_ID ='%s' AND FACILITY_ID='%s' ", serviceId,facilityId );
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
			int updCount = resList.get(0).getInt("UpdatedRows");
			if(updCount > 0) {
				res=true;
			}

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to update facility service amount due column in DB");
		}
		return res;
	}

	/**
	 * @Purpose: To get amount due details
	 * @param : need to pass String serviceId, String facilityId, String principalId
	 * @return: String 
	 * @AuthorDate: 07-15-2019
	 * @ReviewedDate:
	 */
	public  String getCorrectionsAmountDue(String serviceId, String facilityId, String principalId) {
		String amountDue=null;
		String facServiceId=null;
		String memberId=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT MEMBER_ID FROM OFFENDERCONNECT.CORRECTIONS_MEMBERS WHERE FACILITY_ID='"+facilityId+"' AND PRINCIPAL_ID='"+principalId+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			memberId=resList.get(0).get("memberId").toString();

			sqlQuery="SELECT FACILITY_SERVICE_ID FROM OFFENDERCONNECT.CORRECTIONS_FACILITY_SERVICE WHERE SERVICE_ID='"+serviceId+"' AND FACILITY_ID='"+facilityId+"'";
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			facServiceId=resList.get(0).get("facilityServiceId").toString();

			sqlQuery="SELECT AMOUNT FROM OFFENDERCONNECT.CORRECTIONS_AMOUNT_DUE WHERE FACILITY_SERVICE_ID='"+facServiceId+"' AND MEMBER_ID='"+memberId+"'";
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			amountDue=resList.get(0).get("amount").toString();

		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get corrections amount due value from DB");
		}
		return amountDue;
	}

	/**
	 * @Purpose: To get number of records based on Transaction
	 * @param : need to pass String transactionId
	 * @return: int number of records based on transaction id
	 * @AuthorDate: 07-15-2019
	 * @ReviewedDate:
	 */
	public int getNoOfRecordsBasedOnTransactionId(String transactionId) {
		int count=0;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT COUNT(*) FROM OFFENDERCONNECT.CORRECTIONS_TRANSACTION WHERE TRANSACTION_ID = '"+transactionId+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			count=resList.get(0).getInt("count(*)");
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get records count based on Transaction Id");
		}
		return count;
	}

	/**
	 * @Purpose: To get originating IP Address from DB
	 * @param : need to pass String transactionId
	 * @return: String originating IP address
	 * @AuthorDate: 07-17-2019
	 * @ReviewedDate:
	 */
	public String getOriginatingIpAddress(String transactionId) {
		String ipAddress=null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT ORIGINATING_IP FROM OFFENDERCONNECT.CORRECTIONS_TRANSACTION WHERE TRANSACTION_ID = '"+transactionId+"'";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			ipAddress=resList.get(0).get("originatingIp").toString();
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get IP Address based on Transaction Id");
		}
		return ipAddress;
	}

	/**
	 * @Purpose: To get expired token TransactionId and Transaction Token
	 * @param : need to pass String transactionId
	 * @return: List<JSONObject> token expired transaction details 
	 * @AuthorDate: 07-17-2019
	 * @ReviewedDate:
	 */
	public List<JSONObject> getTokenExpiredTrasactionDetails() {
		List<JSONObject> resList = null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.CORRECTIONS_TRANSACTION_TOKENS WHERE TOKEN_EXPIRED = 1 AND TOKEN_STATUS = 1";
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get transaction details");
		}
		return resList;
	}

	/**
	 * @Purpose: To enable or disable Portal Services
	 * @param : int enable or disable , String PortalId
	 * @return: boolean enable/disable status based on flag
	 * @AuthorDate: 07-19-2019
	 * @ReviewedDate:
	 */
	public  boolean enableOrDisablePortalServices(String flag,String portalId) {
		boolean res=false;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String query = String.format("UPDATE OFFENDERCONNECT.CORRECTIONS_PORTAL SET PORTAL_ACTIVE ='"+flag+"' WHERE PORTAL_ID ='%s'", portalId );
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
			int updCount = resList.get(0).getInt("UpdatedRows");
			if(updCount > 0) {
				res=true;
			}

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to update portal service in DB");
		}
		return res;
	}

	/**
	 * @Purpose: To enable or disable corrections Services
	 * @param : String enable or disable,String portalId,String accountNumber,String activeStatus,String status
	 * @return: boolean enable/disable status
	 * @AuthorDate: 02-08-2019
	 * @ReviewedDate:
	 */
	public  boolean UpdateEnableOrDisablePortalServices(String enableordisable,String portalId,String accountNumber,String activeStatus,String status) {
		boolean res=false;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String query = String.format("update OFFENDERCONNECT.CORRECTIONS_FAC_SERVICE_PORTAL set FSP_ACTIVE='"+enableordisable+"' where PORTAL_ID='"+portalId+"' and FACILITY_SERVICE_ID in(SELECT DISTINCT cfsp.FACILITY_SERVICE_ID from corrections_members cm inner join corrections_account_member cam on cam.member_id=cm.member_id inner join account act on act.account_guid = cam.account_id inner join corrections_roster_members rm on cm.member_id = rm.member_id inner join corrections_roster cr on rm.roster_id = cr.roster_id inner join corrections_facility_service cfs on cr.roster_id = cfs.roster_id inner join corrections_fac_service_portal cfsp on cfsp.facility_service_id = cfs.facility_service_id left join corrections_portal cp on cfsp.portal_id = cp.portal_id where act.account_number = '"+accountNumber+"' and cp.portal_id = '"+portalId+"' and cfsp.fsp_active = '"+activeStatus+"' and cfs.active = '"+status+"' and cp.portal_active = '"+status+"') and rownum=1 ");
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
			int updCount = resList.get(0).getInt("UpdatedRows");
			if(updCount > 0) {
				res=true;
			}

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to update enable or disable portal service in DB");
		}
		return res;
	}

	/**
	 * @Purpose: To remove inmates from account
	 * @param : String username
	 * @AuthorDate: 07-23-2019
	 * @ReviewedDate:
	 */
	public void removeInmatesFromAccount(String username) {
		try {

			//establish database connection
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);

			//get inmates list
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(String.format("select * from ACCOUNT_INMATES WHERE ACTIVE_YN = 'Y' AND ACCOUNT_GUID in (select ACCOUNT_GUID from account where user_id ='%s')", username));

			//get inmates count
			int inmateCount = resList.size();

			//remove inmates if account has inmates
			if(inmateCount > 0) {
				String query = String.format("UPDATE ACCOUNT_INMATES SET ACTIVE_YN = 'N' WHERE ACCOUNT_GUID in (select ACCOUNT_GUID from account where user_id ='%s')", username );
				List<JSONObject> updList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
				int updCount = updList.get(0).getInt("UpdatedRows");

				if(updCount > 0) {
					LogManager.logInfo(CNDatabaseQueries.class.getName(), "Removed inmates successfully from user account");
				}
			}
		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to update inmates");
		}
	}

	/**
	 * @Purpose: To remove entity from Credit Bank Account
	 * @param : String username
	 * @return: boolean no of entities removed from credit bank
	 * @AuthorDate: 07-23-2019
	 * @ReviewedDate:
	 */
	public boolean removeEntityFromCreditBankAccount(String entity) {
		boolean res=false;
		try {
			//establish database connection
			//db.establishDatabaseConnection(gblConstants.dbName_CreditBank);

			String query = String.format("DELETE FROM CREDITBANK.ACCOUNTS WHERE ENTITY ='%s' AND CREATED_BY='1' AND OC_SITE_ID=25", entity );
			int updCount = CNDatabases.creditBank().executeUpdateQuery(query);
			if(updCount > 0) {
				res=true;
			}
		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to delete entity");
		}
		return res;
	}

	/**
	 * @Purpose: To get details from OC.Transactions table for Facility Admin test validations
	 * @param : String colName, Service Type, Facility Name, Start Date, DB transactions search filter
	 * @return: String  transaction details
	 * @AuthorDate: 23-08-2019
	 * @ReviewedDate:
	 */
	public String getTransactionsDetails(String colName, String serviceType, String facilityName, String date, String filter) {
		String value = null;
		ResultSet OrcResultSet = null;

		try {

			//establish database connection
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);

			//String Query
			String dbQuery = "SELECT "+colName+" FROM OFFENDERCONNECT.TRANSACTIONS WHERE SERVICETYPE = '"+ serviceType.replaceAll("\\s", "").toUpperCase() +"'" + 
					" AND OFFENDERCONNECTSITEID = (SELECT OFFENDERCONNECTSITEID FROM OFFENDERCONNECT.FACILITYSERVICES where FACILITYNAME like '%"+ facilityName +"%')" + 
					" AND STATUS = 'ACCEPTED' "+ filter +" AND DATETIMEPAYMENTMADE BETWEEN TO_DATE('"+ date +"', 'MM/DD/YYYY') AND CURRENT_DATE ORDER BY DATETIMEPAYMENTMADE DESC";

			//Execute Query and get response
			OrcResultSet = CNDatabases.offenderConnect().getQueryResponse(dbQuery);
			OrcResultSet.next();

			//Returns row count 
			if(colName.equals("COUNT(*)")) {
				value = OrcResultSet.getString("COUNT(*)");
			}else { //returns column value
				value = OrcResultSet.getString(colName);
			}
			OrcResultSet.close();
		} catch (Exception e) {

			LogManager.logException(e, className,"Exception to get transactions details from DB");
		}
		return value;
	}

	/**
	 * @Purpose: To get Facility Service by facilityId and serviceId and portal type details
	 * @param : String facilityId,String serviceId,String portalType
	 * @return: List<JSONObject> facility service details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getFacilityServiceDetailsByServiceAndFacilityId(String facilityId,String serviceId,String portalType) {
		
		//establish database connection
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);

		String query ="SELECT fs.state, fs.facilityname, fs.offenderconnectsiteid as \"OCID\", cfs.service_id, cfs.facility_service_id, cfs.display_name, p.portal_type, p.portal_location, t.trusted_token,\r\n" + 
				"csf.fee_id as \"* FEE ID \", csf.method, csf.tender, csf.is_additive, csf.min_amount, csf.max_amount, csf.flat_fee_amount as \"+ FlatFee\", csf.percentage_amount as \"+ Fee %\", cvr.velocity_rules_id as \" VelocityID *\", cvr.method, cvr.tender, cvr.daily_max as \"Daily\", cvr.weekly_max as \"Weekly\", cvr.monthly_max as \"Monthly\"\r\n" + 
				"from OFFENDERCONNECT.facilityservices fs\r\n" + 
				"left join OFFENDERCONNECT.corrections_facility_service cfs on cfs.facility_id = fs.offenderconnectsiteid\r\n" + 
				"left join OFFENDERCONNECT.corrections_fac_service_portal fac on fac.facility_service_id = cfs.facility_service_id\r\n" + 
				"left join OFFENDERCONNECT.corrections_portal p on p.portal_id = fac.portal_id\r\n" + 
				"left join OFFENDERCONNECT.corrections_trusted_tokens t on t.portal_id = p.portal_id\r\n" + 
				"left join offenderconnect.corrections_service_fee csf on csf.facility_service_id = cfs.facility_service_id and p.portal_type = csf.method\r\n" + 
				"left join OFFENDERCONNECT.corrections_velocity_rules cvr on cvr.facility_service_id = cfs.facility_service_id and cvr.method = csf.method and cvr.tender = csf.tender\r\n" + 
				"where fs.como_migrated = 1 AND fs.offenderconnectsiteid='"+facilityId+"' AND cfs.service_id='"+serviceId+"' AND p.portal_type='"+portalType+"'\r\n" + 
				"order by fs.state, fs.facilityname, p.portal_type, cfs.service_id";
		
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	
	/**
	 * @purpose : to get membership id
	 * @param : String getMembershipId 
	 * @return membership ID
	 */
	public String getMembershipId(String providerServicesId) {
		
		String getMembershipId=null;
		
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="select ps.provider_id, m.* from offenderconnect.service_memberships m left join\r\n" + 
					"(select max (t.payment_date_time) as last_payment_date_time, membership_guid from offenderconnect.provider_service_transactions t where t.payment_status = 'ACCEPTED' group by t. membership_guid) z\r\n" + 
					"on m.membership_guid = z.membership_guid\r\n" + 
					"inner join offenderconnect.provider_services ps on ps.provider_services_id = m.provider_services_id\r\n" + 
					"where m.MEMBERSHIP_ID like '00%' AND REGEXP_LIKE(m.MEMBERSHIP_ID, '^[[:digit:]]+$') AND m.provider_services_id = "+ providerServicesId +" and status = 'ACTIVE' and last_payment_date_time is NULL";
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			getMembershipId=resList.get(0).get("membershipId").toString();
			if(getMembershipId.startsWith("00")) {
				getMembershipId=getMembershipId.substring(2,getMembershipId.length());
			}
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get membershipId info");
		}
		
		return getMembershipId;
	}
	
	/**
	 * @Purpose: To get provider service transaction details
	 * @param : need to pass String memberShipGUID
	 * @return: List<JSONObject> provider service transaction details 
	 * @AuthorDate: 09-18-2019
	 * @ReviewedDate:
	 */
	public List<JSONObject> getProviderServicesTransactionDetails(String memberShipGUID) {
		List<JSONObject> resList = null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * from OFFENDERCONNECT.PROVIDER_SERVICE_TRANSACTIONS WHERE MEMBERSHIP_GUID='"+memberShipGUID+"' AND ACH_STATUS='APPLIED' order BY PAYMENT_DATE_TIME DESC";
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
			int iter=0;
			do {
					Thread.sleep(5000);//as db record updation taking time
					resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
					iter++;
					if(iter==6) {
						break;
					}
					
			}while(resList.isEmpty());
			
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get provider service transaction details");
		}
		return resList;
	}
	

	/**
	 * @Purpose: To get master transaction details
	 * @param : need to pass String transactionId
	 * @return: List<JSONObject> master transaction details 
	 * @AuthorDate: 09-18-2019
	 * @ReviewedDate:
	 */
	public List<JSONObject> getMasterTransactionDetails(String transactionId) {
		List<JSONObject> resList = null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE Transaction_Id='"+transactionId+"' order BY PAYMENT_DATE_TIME DESC";
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get master transaction details");
		}
		return resList;
	}

	/**
	 * @Purpose: To delete master transaction details
	 * @param : need to pass String transactionId
	 * @return: integer no of master transaction details deleted
	 * @AuthorDate: 09-19-2019
	 * @ReviewedDate:
	 */
	public synchronized int  deleteMasterTransactionDetails(String transactionId) {
		int removedNo = 0;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="DELETE FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE Transaction_Id='"+transactionId+"'";
			removedNo= CNDatabases.offenderConnect().executeUpdateQuery(sqlQuery);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get delete master transaction details");
		}
		return removedNo;
	}
	
	
	/**
	 * @Purpose: To reset visitation transaction
	 * @param : need to pass String membershipGUID, providerservicesId, fee and amount
	 * @return: boolean 
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 09-19-2019
	 * @ReviewedDate:
	 */
	public  boolean resetProviderServiceTransaction(String providerServicesId, String transactionId) {
		boolean res=false;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			List<JSONObject> resList = executeUpdatePaymentstausQuery(providerServicesId, transactionId);
			int updCount = resList.get(0).getInt("UpdatedRows");
			if(updCount > 0) {
				res=true;
			}

		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to reset the transaction from database");
		}
		return res;
	}
	
	/**
	 * @Purpose: To update payment status
	 * @param : need to pass String providerservicesId, transactionId
	 * @return: boolean 
	 * @AuthorName: Vamshi Krishna Voddeti
	 * @AuthorDate: 09-19-2019
	 * @ReviewedDate:
	 */
	private List<JSONObject> executeUpdatePaymentstausQuery(String providerServicesId, String transactionId ) {
		String query = String.format("UPDATE OFFENDERCONNECT.PROVIDER_SERVICE_TRANSACTIONS SET ACH_STATUS ='', PAYMENT_STATUS='', PAYMENT_DATE_TIME='' WHERE ACH_STATUS='APPLIED' AND PAYMENT_STATUS='ACCEPTED' AND PROVIDER_SERVICES_ID ='%s' AND TRANSACTION_ID='%s'", providerServicesId,transactionId );
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	
	/**
	 * @purpose : to get membership id
	 * @param : String getMembershipId 
	 * @return membership ID
	 */
	public List<JSONObject> getMembershipDetails(String providerServicesId) {
		
		List<JSONObject> getMembershipDetails=null;
		
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="select ps.provider_id, m.* from offenderconnect.service_memberships m left join\r\n" + 
					"(select max (t.payment_date_time) as last_payment_date_time, membership_guid from offenderconnect.provider_service_transactions t where t.payment_status = 'ACCEPTED' group by t. membership_guid) z\r\n" + 
					"on m.membership_guid = z.membership_guid\r\n" + 
					"inner join offenderconnect.provider_services ps on ps.provider_services_id = m.provider_services_id\r\n" + 
					"where m.MEMBERSHIP_ID like '00%' AND REGEXP_LIKE(m.MEMBERSHIP_ID, '^[[:digit:]]+$') AND m.provider_services_id = "+ providerServicesId +" and status = 'ACTIVE' and last_payment_date_time is NULL";
			getMembershipDetails = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get membershipDetails info");
		}
		
		return getMembershipDetails;
	}
	

	/**
	 * @Purpose: To update inmate status
	 * @param : String yn,String inmateId, String username
	 * @AuthorDate: 09-26-2019
	 * @ReviewedDate:
	 */
	public void updateInmateStatus(String yn,String inmateId, String username) {
		try {

			//establish database connection
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);

			//get inmates list
			List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(String.format("select * from ACCOUNT_INMATES WHERE ACCOUNT_GUID in (select ACCOUNT_GUID from account where user_id ='%s')", username));

			//get inmates count
			int inmateCount = resList.size();

			//update inmate status if account has inmates
			if(inmateCount > 0) {
				LogManager.logInfo(CNDatabaseQueries.class.getName(), "Account has "+inmateCount+" inmates with Y/N status");
				String query = String.format("UPDATE ACCOUNT_INMATES SET ACTIVE_YN = '%s' WHERE BOOKING_NUM='%s' AND ACCOUNT_GUID in (select ACCOUNT_GUID from account where user_id ='%s')", yn,inmateId,username );
				List<JSONObject> updList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
				
				int updCount = updList.get(0).getInt("UpdatedRows");

				if(updCount > 0) {
					LogManager.logInfo(CNDatabaseQueries.class.getName(), "Updated inmate \"" +inmateId+ "\" status to \""+ yn +"\" successfully for user \""+username+"\" ");
				}
			}else {
				LogManager.logInfo(CNDatabaseQueries.class.getName(), "Account has no inmates.");
			}
		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to update inmates");
		}
//		finally {
//			//close connection
//			db.dbConnection.closeConnection();
//		}
	}
	
	/**
	 * @Purpose: To get inmate presence status
	 * @param : need to pass String inmateId
	 * @return: List<JSONObject> get on demand calls status
	 * @AuthorDate: 09-26-2019
	 * @ReviewedDate:
	 */
	public List<JSONObject> getOnDemandCallsStatusDetails(String inmateId) {
		List<JSONObject> resList = null;
		try {
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String sqlQuery="SELECT * FROM ON_DEMAND_CALLS_STATUS where INMATE_ID = '"+inmateId+"' ORDER BY DATE_TIME desc";
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(sqlQuery);
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to on demand calls status details");
		}
		return resList;
	}
	
	/**
	 * @Purpose: To update credit card status
	 * @param : String status,String gtlBillingId, String user name
	 * @AuthorDate: 09-27-2019
	 * @ReviewedDate:
	 */
	public void updateCreditCardStatus(String status,String gtlBillingId, String username) {
		try {

			//establish database connection
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);

			String query = String.format("UPDATE OFFENDERCONNECT.STORED_CREDIT_CARD SET ACTIVE= '%s' WHERE GTL_BILLING_ID ='%s'", status,gtlBillingId,username );
			
			List<JSONObject> updList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);

			int updCount = updList.get(0).getInt("UpdatedRows");

			if(updCount > 0) {
				LogManager.logInfo(CNDatabaseQueries.class.getName(), "Updated credit card \"" +gtlBillingId+ "(gtlBillingId)\" status to \""+ status +"\" successfully for account \""+username+"\" ");
			}
		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to update credit card status.");
		}
//		finally {
//			//close connection
//			db.dbConnection.closeConnection();
//		}
	}

	/**
	 * @Purpose: To update credit card status
	 * @param : String username
	 * @return 
	 * @AuthorDate: 09-27-2019
	 * @ReviewedDate:
	 */
	public List<JSONObject> getCreditcardDetails(String username) {
		List<JSONObject> resList = null;
		try {

			//establish database connection
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			
			//get account details
			resList =getAccountDetails(username);
			
			//get account number
			String accNumber= resList.get(0).getString("accountNumber");

			//get linked credit cards list of an account
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(String.format("SELECT * FROM OFFENDERCONNECT.STORED_CREDIT_CARD WHERE ACCOUNT_NUMBER ='%s'", accNumber));

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get credit card status.");
		}
//		finally {
//			//close connection
//			db.dbConnection.closeConnection();
//		}
		return resList;
	}
	
	/**
	 * @Purpose: To get existing account details
	 * @return: List<JSONObject> account details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getExistingAccountDetails() {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = "SELECT * FROM OFFENDERCONNECT.ACCOUNT WHERE ROWNUM=1";
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	
	/**
	 * @purpose: To get active credit cards
	 * @param: String accountNumber
	 * @return: List<JSONObject> active rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getActiveCreditCards(String accountNumber) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM OFFENDERCONNECT.STORED_CREDIT_CARD WHERE ACCOUNT_NUMBER='%s' AND ACTIVE='1'", accountNumber);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	
	/**
	 * @Purpose: To get credit card details
	 * @param : String user name , String cardGuid
	 * @return : List<JSONObject> credit card details
	 * @ReviewedDate:
	 */
	public List<JSONObject> getCreditcardDetails(String username, String cardGuid) {
		List<JSONObject> resList = null;
		try {

			//establish database connection
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			
			//get account details
			resList =getAccountDetails(username);
			
			//get account number
			String accNumber= resList.get(0).getString("accountNumber");

			//get linked credit cards list of an account
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(String.format("SELECT * FROM OFFENDERCONNECT.STORED_CREDIT_CARD WHERE ACCOUNT_NUMBER ='%s' and CREDIT_CARD_GUID='%s'", accNumber,cardGuid));

		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to get credit card details.");
		}
//		finally {
//			//close connection
//			db.dbConnection.closeConnection();
//		}
		return resList;
	}
	
	/**
	 * @purpose: To get trust payment details
	 * @param: String sitekey
	 * @return: List<JSONObject> return rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getTrustPaymentDetails(String sitekey) {
		//db.establishDatabaseConnection(gblConstants.dbName_IBanker);
		String query = String.format("SELECT * FROM IBANKER.FACILITY WHERE SITE_KEY='%s'", sitekey);
		List<JSONObject> resList = CNDatabases.iBanker().getQueryResponseAsJSON(query);
		return resList;
	}
	
	/**
	 * @purpose: To get community corrections payment details
	 * @param: String site key
	 * @return: List<JSONObject> return rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getCommunityCorrectionsPaymentDetails(String sitekey) {
		//db.establishDatabaseConnection(gblConstants.dbName_IBanker);
		String query = String.format("SELECT * FROM Ibanker.facility WHERE SITE_KEY ='%s'", sitekey);
		//List<JSONObject> resList = db.runQuery(query);
		List<JSONObject> resList = CNDatabases.iBanker().getQueryResponseAsJSON(query);
		return resList;
	}
	
	/**
	 * @purpose: To get no account in CreditBank inmate details
	 * @param: String site key
	 * @return: List<JSONObject> return rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getNoAccountInCreditBankInmateDetails(String sitekey) {
		String ocUID= null;
		//db.establishDatabaseConnection(gblConstants.dbName_IBanker);
		String query = String.format("SELECT * FROM IBANKER.INMATE WHERE INMATE_STATUS='ACTIVE' and OC_UID is not NULL AND SITE_KEY = '%s' AND ROWNUM <100", sitekey);
		//List<JSONObject> ibResList = db.runQuery(query);
		List<JSONObject> ibResList = CNDatabases.iBanker().getQueryResponseAsJSON(query);
		int objSize=ibResList.size();

		for(int iter=0; iter<objSize ; iter++) {
			
			ocUID= ibResList.get(iter).get("ocUid").toString();
			//db.establishDatabaseConnection(gblConstants.dbName_CreditBank);
			query = String.format("SELECT ACCT.* FROM CREDITBANK.ACCOUNTS ACCT JOIN CREDITBANK.ENTITIES ENT ON ACCT.ENTITY=ENT.ID WHERE ENT.EXTERNAL_REFERENCE_ID= '%s'", ocUID);
			List<JSONObject> cbResList = CNDatabases.creditBank().getQueryResponseAsJSON(query);

			if(cbResList.isEmpty()) {
				break;
			}else {
				continue;
			}
		}
		return ocUID;
	}
	
	/**
	 * @Purpose: To update facility status
	 * @param : String status,String ocSiteId, String serviceColName ,String service
	 * @throws InterruptedException 
	 * @AuthorDate: 10-18-2019
	 * @ReviewedDate:
	 */
	public boolean updateFacilityServiceStatus(String status,String ocSiteId, String serviceColName,String service) throws InterruptedException {
		boolean blnRes= false;
		try {

			//establish database connection
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);

			String query = String.format("UPDATE OFFENDERCONNECT.FACILITYSERVICES SET %s= '%s' WHERE OFFENDERCONNECTSITEID = '%s'", serviceColName,status,ocSiteId );
			
			List<JSONObject> updList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);

			int updCount = updList.get(0).getInt("UpdatedRows");

			if(updCount > 0) {
				blnRes=true;
				LogManager.logInfo(CNDatabaseQueries.class.getName(), "Successfully updated facility service \"" +service+ "\" status to \""+ status +"\".");
			}
		} catch (Exception e) {
			LogManager.logException(e, CNDatabaseQueries.class.getName(), "Exception to update facility service status.");
		}
		return blnRes;
	}
	
	/**
	 * @purpose: To get facility service details
	 * @param: String facilityId ,serviceId
	 * @return: List<JSONObject> return rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getfacilityServiceDetails(String facilityId, String serviceId) {
		//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
		String query = String.format("SELECT * FROM OFFENDERCONNECT.CORRECTIONS_FACILITY_SERVICE WHERE FACILITY_ID='%s' AND SERVICE_ID='%s'", facilityId,serviceId);
		//List<JSONObject> resList = db.runQuery(query);
		List<JSONObject> resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		return resList;
	}
	
	/**
	 * @purpose: To get Inmate blocked/unblocked by facility and GTL
	 * @param: String gtlFundStatus, String facFundStatus
	 * @return: List<JSONObject> return rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getInmateByFacilityAndGTLStatus(String fac, String gtlFundStatus, String facFundStatus) {
		//db.establishDatabaseConnection(gblConstants.dbName_IBanker);
		String query = String.format("SELECT * FROM IBANKER.INMATE WHERE SITE_KEY='%s' AND GTL_FUNDABLE_STATUS='%s' AND FACILITY_FUNDABLE_STATUS='%s'",fac,gtlFundStatus,facFundStatus);
		//List<JSONObject> resList = db.runQuery(query);
		List<JSONObject> resList = CNDatabases.iBanker().getQueryResponseAsJSON(query);
		return resList;
	}
	
	/**
	 * @Purpose: To get master transaction details
	 * @param : String userid
	 * @return:  List<JSONObject> master transaction details
	 * @Author-Date:
	 * @Reviewed-Date:
	 */	
	public List<JSONObject> getMasterTransactionDetailsUsingAccGuid(String userid){
		List<JSONObject> resList=null;
		try {
			String accountGuId=getAccountGuidFromAccountTable(userid);
			//db.establishDatabaseConnection(gblConstants.dbName_OffenderConnect);
			String query = String.format("SELECT * FROM (SELECT * FROM OFFENDERCONNECT.MASTER_TRANSACTIONS WHERE ACCOUNT_GUID ='%s' ORDER BY PAYMENT_DATE_TIME DESC) WHERE ROWNUM =1",accountGuId);
			//resList = db.runQuery(query);
			resList = CNDatabases.offenderConnect().getQueryResponseAsJSON(query);
		} catch (Exception e) {
			LogManager.logException(e,className,"Exception to get Master Transaction details");
		}
		return resList;
	}
	
	/**
	 * @purpose: To get attachment details
	 * @param: String attachmentId
	 * @return: List<JSONObject> return rows
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public List<JSONObject> getAttachmentDetails(String attachmentId) {
		//db.establishDatabaseConnection(gblConstants.dbName_Attachments);
		String query = String.format("SELECT * FROM ATTACHMENTS.ATTACHMENT WHERE ID ='%s'", attachmentId);
		List<JSONObject> resList = CNDatabases.attachments().getQueryResponseAsJSON(query);
		return resList;
	}
	
}

