package idb.common.utils;

import java.util.ArrayList;

import idb.config.idbConstants;


public class CNConstants extends idbConstants{

	public static final String jiraUrl="https://hqtajira01:8443";
	//Basic Authentication
	public static final String authentication="YXV0b21hdG9yanRlOkB1dG8janRlMjAxOQ==";
	
	public static final String  projectId="16500";
	
	public static final String versionId="-1";
	
	public static final String cycleId="1443";
	
	public static final String cycleName="cycle1";

	
	public static final String WSDL="http://ws.cdyne.com/delayedstockquote/delayedstockquote.asmx";
	
	public static final ArrayList<String> issueKeys=new ArrayList<String>();

}
