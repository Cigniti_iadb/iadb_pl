package idb.common.utils;

import java.util.HashMap;

import idb.config.idbConstants;
import idb.core.database.DatabaseVerification;
import idb.core.logs.LogManager;

public class CNDatabases {
	
	enum DB{
		OC, AE, CB, EMS, IB, USSO;
		
		public DatabaseVerification getConnection() {						
			DatabaseVerification dbConnection = CNDatabases.establishDatabaseConnection(DB.valueOf(this.name()));
			return dbConnection;
		}
		
		public String getType() {
			String strDBType = idbConstants.suiteConfig.getdatabaseType(this.name());
			return strDBType;
		}
	}

	public static DatabaseVerification offenderConnect() {
		if (clients.get(DB.OC) == null) {
			establishDatabaseConnection(DB.OC);
		}
		return clients.get(DB.OC);
	}
	
	public static DatabaseVerification attachments() {
		if (clients.get(DB.AE) == null) {
			establishDatabaseConnection(DB.AE);
		}
		return clients.get(DB.AE);
	}
	
	public static DatabaseVerification creditBank() {
		if (clients.get(DB.CB) == null) {
			establishDatabaseConnection(DB.CB);
		}
		return clients.get(DB.CB);
	}
	
	public static DatabaseVerification ems() {
		if (clients.get(DB.EMS) == null) {
			establishDatabaseConnection(DB.EMS);
		}
		return clients.get(DB.EMS);
	}
	
	public static DatabaseVerification iBanker() {
		if (clients.get(DB.IB) == null) {
			establishDatabaseConnection(DB.IB);
		}
		return clients.get(DB.IB);
	}
	
	public static DatabaseVerification usso() {
		if (clients.get(DB.USSO) == null) {
			establishDatabaseConnection(DB.USSO);
		}
		return clients.get(DB.USSO);
	}

	private static HashMap<CNDatabases.DB, DatabaseVerification> clients = new HashMap<CNDatabases.DB, DatabaseVerification>();	

	private static synchronized DatabaseVerification establishDatabaseConnection(CNDatabases.DB dbName){
		try { 
			DatabaseVerification dbConnection = null;
			switch(dbName){
	           case OC:
	               if(clients.get(DB.OC) == null) {
	            	   dbConnection = new DatabaseVerification(
	                           idbConstants.suiteConfig.getdatabaseType(DB.OC.toString()),
	                           idbConstants.suiteConfig.getdatabaseServer(DB.OC.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginUser(DB.OC.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginPassword(DB.OC.toString() ));
	                   clients.put(DB.OC, dbConnection);
	               }
	               break;
	           case AE:
	        	   if(clients.get(DB.AE) == null) {
	        		   dbConnection = new DatabaseVerification(
	                           idbConstants.suiteConfig.getdatabaseType(DB.AE.toString()),
	                           idbConstants.suiteConfig.getdatabaseServer(DB.AE.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginUser(DB.AE.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginPassword(DB.AE.toString() ));
	        		   clients.put(DB.AE, dbConnection);
	        	   }
	        	   break;
	           case IB:
	               if(clients.get(DB.IB) == null) {
	            	   dbConnection = new DatabaseVerification(
	                           idbConstants.suiteConfig.getdatabaseType(DB.IB.toString()),
	                           idbConstants.suiteConfig.getdatabaseServer(DB.IB.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginUser(DB.IB.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginPassword(DB.IB.toString() ));
	                   clients.put(DB.IB, dbConnection);
	               }
	               break;               
	           case EMS:
	               if(clients.get(DB.EMS) == null) {
	            	   dbConnection = new DatabaseVerification(
	                           idbConstants.suiteConfig.getdatabaseType(DB.EMS.toString()),
	                           idbConstants.suiteConfig.getdatabaseServer(DB.EMS.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginUser(DB.EMS.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginPassword(DB.EMS.toString() ));
	                   clients.put(DB.EMS, dbConnection);
	               }
	               break;               
	           case USSO:
	        	   if(clients.get(DB.USSO) == null) {
	                   dbConnection = new DatabaseVerification(
	                           idbConstants.suiteConfig.getdatabaseType(DB.USSO.toString()),
	                           idbConstants.suiteConfig.getdatabaseServer(DB.USSO.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginUser(DB.USSO.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginPassword(DB.USSO.toString() ));
	                   clients.put(DB.USSO, dbConnection);
	        	   }
	               break;
	               
	           case CB:
	               if(clients.get(DB.CB) == null) {
	            	   dbConnection = new DatabaseVerification(
	                           idbConstants.suiteConfig.getdatabaseType(DB.CB.toString()),
	                           idbConstants.suiteConfig.getdatabaseServer(DB.CB.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginUser(DB.CB.toString()),
	                           idbConstants.suiteConfig.getdatabaseLoginPassword(DB.CB.toString() ));
	                   clients.put(DB.CB, dbConnection);
	               }
	               break;
	       }
		}catch(Exception e) {
			LogManager.logException(e, CNDatabases.class.getName(), "Exception " + e.getCause() + " establish DB Connection to " + dbName.toString());
		}
	   return clients.get(dbName);
    }
	
}

