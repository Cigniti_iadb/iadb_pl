package idb.web.pages;

import idb.config.idbConstants;
import idb.core.logs.LogManager;
import idb.web.engine.CNWebActionEngine;

public class HomePage {

	private CNWebActionEngine cnEngine = null;
//
//	public final String lblDashboard = "xpath=//span[contains(text(),'dashboard')]";
//	public final String lblMyInmates = "xpath=//div[@id='myInmates']/descendant::h2[contains(text(),'My Inmate')]";
//	public final String lblloginUser_css = "xpath=//form[@id='logout'] | //input[@id='submitBtn']";
//	public final String lnkMessaging_css = "css=a[href='/webapp/jsps/cn/messagelink/email.cn']";
//	public final String lnkAdvancePayPhonePage_css = "css=a[href='/webapp/jsps/cn/advancepay/advancepay.cn']";
//	public final String btnSignOut = "xpath=//form[@id='logout']//a";
//	public final String btnConfirmSignoutYes = "css=a#logout-yes";
//	public final String btnConfirmSignoutNo = "css=a#logout-no";
//    public final String changeEmailAlert="xpath=//div[@id='changeEmailAlertDialog']";
//    public final String btnchangeEmailAlertOk="xpath=//div[@id='changeEmailAlertDialog']/div//div/a";
//	public final String lnkPinDebit = "xpath=//div[@id='menu']//span[text()='PIN Debit']";
//	public final String lnkAdvancePayPhone ="xpath=//span[text()='AdvancePay Phone']";
//	public final String lnkMyInmates ="xpath=//div[@id='menu']//span[text()='My Inmates' or text()='Mis Reclusos']";
//	public final String lblDebitLink = "xpath=//div[@id='menu']//span[text()='Debit Link']";
//	public final String lblCommunityCorrections = "xpath=//div[@id='menu']//span[text()='Community Corrections']";
//	public final String lblTCL = "xpath=//div[@id='menu']//span[text()='TCL']";
//	public final String lblSelfRelease = "xpath=//div[@id='menu']//span[text()='Self Release']";
//	public final String lnkTrustFund ="xpath=//div[@id='menu']//span[text()='Trust Fund']";
//	public final String lblProfile = "xpath=//div[@id='menu']//span[text()='Profile' or text()='Perfil']";
//	public final String lnkDirectBill="xpath=//div[@id='menu']//span[text()='Direct Bill']";
//	public final String lnkTransactionHistory="xpath=//div[@id='menu']//span[text()='Transaction History' or text()='Historial de Transacción']";
//	public final String lnkPaymentMethods="xpath=//div[@id='menu']//span[text()='Payment Methods' or text()='Métodos de Pago']";
//	public final String lnkMyFacilities="xpath=//div[@id='menu']//span[text()='My Facilities' or text()='Mis Instalaciones']";
//	public final String btnSignIn= "xpath=//div[@class='sign_out']//a[text()='Sign In']";
//	public final String lnkAutomaticPayments="xpath=//div[@id='menu']//span[text()='Automatic Payments' or text()='Pagos Automáticos']";
//	public final String lnkRatesAndFees="xpath=//ul[@id='menu-footer-menu']//a[text()='Rates and Fees']";
//	public final String ddLanguage = "xpath=//select[@class='form-control']";
//	public final String lblId = "xpath=//span[@id='logout:user']";
//	public final String btnReturnToSignIn = "xpath=//a[@id='cnForm:returnToLogin']";
//	public final String linkprofileinhomescreen = "xpath=//ul/li[preceding-sibling::li='MY ACCOUNT' and following-sibling::li='My Inmates']";
//	public final String lblHeaderProfileInfo= "xpath=//h4[contains(text(),'Profile Information')]";
//	public final String lblDashboardNoInmates ="xpath=//div[@id='no-inmates-dashboard']";
//	public final String popSignOut ="xpath=//a[@id='logout-yes']/ancestor::div[contains(@role,'dialog')]";
//	
//	//
//	
	public final String textBox ="xpath=//input[@name='q']";
	public final String fbUserName ="xpath=//input[@id='email']";
	public final String fbPassword ="xpath=//input[@id='pass']";
	public final String logibBtn ="//input[@id='u_0_b']";
	
	public HomePage(CNWebActionEngine actEngine){
		cnEngine = actEngine;
		
	}
//	/**
//	 * @Purpose: To wait for page
//	 * @Constraints:
//	 * @return: boolean
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public boolean waitForPage(){
//		boolean blnRes =false;
//		try {
//			//commented the below code as of 27/Jun/2019 as the alert is not longer displayed
//			/*if(this.cnEngine.isElementPresent(changeEmailAlert, true)) {
//				this.cnEngine.sleep(gblConstants.defSleepTime);
//				this.cnEngine.click(btnchangeEmailAlertOk, "Change Email Alert Ok btn");
//			}*/
//			blnRes = this.cnEngine.waitForElementPresent(lblloginUser_css, gblConstants.wdWaitTimeout*2);
//			LogManager.logInfo(HomePage.class.getName(), "Navigated to connect network home page..");
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//			LogManager.logException(e, HomePage.class.getName(), "Exception to navigate to connect network home page");
//		}		
//		return blnRes;
//	}
//	/**
//	 * @Purpose: To navigate messaging
//	 * @Constraints:
//	 * @return: MessagingPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public MessagingPage navigateToMessaging() {
//		MessagingPage msgPage = null;		
//		try {
//			this.cnEngine.click(lnkMessaging_css, "Connect Network Messaging Link");
//			msgPage = new MessagingPage(this.cnEngine);
//		} catch (Exception e) {
//			e.printStackTrace();
//			LogManager.logException(e, HomePage.class.getName(), "Exception to click messaging link in home page");
//		}
//		return msgPage;
//	}
////	/**
////	 * @Purpose: To navigate Rates And Fees
////	 * @Constraints:
////	 * @return: RatesAndFeesPage
////	 * @Author-Date: 
////	 * @Reviewed-Date:
////	 */
////	public RatesAndFeesPage navigateToRatesAndFees() {
////		RatesAndFeesPage pgRAF = null;		
////		try {
////			this.cnEngine.scrollElementIntoView(lnkRatesAndFees);
////			this.cnEngine.sleep(gblConstants.defSleepTime-1);
////			this.cnEngine.click(lnkRatesAndFees, "Connect Network Rates And Fees Link");
////			pgRAF = new RatesAndFeesPage(this.cnEngine);
////		} catch (Exception e) {
////			e.printStackTrace();
////			LogManager.logException(e, HomePage.class.getName(), "Exception to click Rates and Fees link in home page");
////		}
////		return pgRAF;
////	}
//	
//
//	/**
//	 * Purpose: To select Sign out button in home page
//	 * Output: boolean
//	 * Author-Date: 11/9/2018
//	 * Reviewed-Date:
//	 */
//	public boolean clickSignOutButton(){
//		boolean blnRes=false;
//		try{
//			this.cnEngine.waitForElementPresent(btnSignOut, gblConstants.wdWaitTimeout);
//			blnRes=this.cnEngine.clickUsingEnter(btnSignOut, "Signout button");
//			this.cnEngine.waitForElementPresent(popSignOut, gblConstants.wdWaitTimeout);
//			LogManager.logInfo(HomePage.class.getName(), "Clicked Sign out button in Home Page");
//		}catch(Exception e){
//			LogManager.logException(e,HomePage.class.getName(), "Exception to click signout button in dashboard");
//		}
//		return blnRes;
//	}
//	
//	/**
//	 * Purpose: To select Yes button in sign out confirmation pop up
//	 * Output: boolean
//	 * Author-Date: 11/9/2018
//	 * Reviewed-Date:
//	 */
//	public boolean clickSignOutYes() {
//		boolean blnRes=false;
//		try{
//			this.cnEngine.waitForElementPresent(btnConfirmSignoutYes, gblConstants.wdWaitTimeout/5);
//			this.cnEngine.click(btnConfirmSignoutYes, "Yes to SignOut");
//			LogManager.logInfo(HomePage.class.getName(), "Clicked YES in sign out confirmation popup in Home Page");
//			this.cnEngine.waitForElementPresent(btnSignIn, gblConstants.wdWaitTimeout);
//			blnRes = this.cnEngine.isElementPresent(btnSignIn, "Sign In button", true);
//		}catch(Exception e){
//			LogManager.logException(e,HomePage.class.getName(), "Exception to click Yes button in sign out popup");
//		}
//		return blnRes;
//	}
//	
//	/**
//	 * Purpose: To select No button in sign out confirmation pop up
//	 * Output: boolean
//	 * Author-Date: 11/9/2018
//	 * Reviewed-Date:
//	 */
//	public boolean clickSignOutNo() {
//		boolean blnRes=false;
//		try{
//			this.cnEngine.waitForElementPresent(btnConfirmSignoutNo, gblConstants.wdWaitTimeout/5);
//			this.cnEngine.click(btnConfirmSignoutNo, "No in SignOut");
//			LogManager.logInfo(HomePage.class.getName(), "Clicked No in sign out confirmation popup in Home Page");
//			this.cnEngine.waitForElementPresent(btnSignOut, gblConstants.wdWaitTimeout);
//			blnRes =  this.cnEngine.isElementPresent(btnSignOut, "Sign out button", true);
//		}catch(Exception e){
//			LogManager.logException(e,HomePage.class.getName(), "Exception to click No button in sign out popup");
//		}
//		return blnRes;
//	}
////	/**
////	 * @Purpose: To navigate pin debit page
////	 * @Constraints:
////	 * @return: PinDebitPage
////	 * @Author-Date: 
////	 * @Reviewed-Date:
////	 */
////	public PinDebitPage navigateToPinDebitPage()
////	{
////		PinDebitPage pgPinDebit=null;
////		try {
////			this.cnEngine.click(lnkPinDebit, "Click Connect Network Pin Debit Link");
////			pgPinDebit = new PinDebitPage(this.cnEngine); 
////		}catch(Exception e){
////			LogManager.logException(e,HomePage.class.getName(), "Exception to click Pindebit link in home page.");
////		}
////		return pgPinDebit;
////	}
//
//	/**
//	 * @Purpose: To navigate My Inmate Page
//	 * @Constraints:
//	 * @return: MyInmatesPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public MyInmatesPage navigateToMyInmatePage()
//	{
//		MyInmatesPage pgMyInmates=null;
//		try {
//			this.cnEngine.sleep(gblConstants.defSleepTime);
//			this.cnEngine.click(lnkMyInmates, "Connect Network MyInmates Link");
//			pgMyInmates =new MyInmatesPage(this.cnEngine); 
//			LogManager.logInfo( HomePage.class.getName(), "Successfully Navigated to My Inmates Page.");
//		}catch(Exception e){
//			LogManager.logException(e,HomePage.class.getName(), "Exception to click MyInmates link in home page.");
//		}
//		return pgMyInmates;
//	}
//	/**
//	 * @Purpose: To navigate Debit Link
//	 * @Constraints:
//	 * @return: DebitLinkPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public DebitLinkPage navigateToDebitLink()
//	{
//		DebitLinkPage pgDebitLink=null;
//		try
//		{	this.cnEngine.waitForElementPresent(lblDebitLink, gblConstants.wdWaitTimeout);
//			this.cnEngine.click(lblDebitLink, "Connect Network Debit Link");
//			pgDebitLink = new DebitLinkPage(this.cnEngine);
//			LogManager.logInfo(HomePage.class.getName(), "Successfully Navigated to Debit Link page.");
//		}
//		catch(Exception e)
//		{
//			LogManager.logException(e,HomePage.class.getName(), "Exception to click Debit Link in home page.");
//		}
//		return pgDebitLink;
//	}
//	/**
//	 * @Purpose: To navigate  Community Corrections
//	 * @Constraints:
//	 * @return: CommunityCorrectionsPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public CommunityCorrectionsPage  navigateToCommunityCorrections()
//	{
//		CommunityCorrectionsPage pgCommunityCrtns=null;
//		
//		try
//		{	this.cnEngine.waitForElementPresent(lblCommunityCorrections, gblConstants.wdWaitTimeout);
//			this.cnEngine.click(lblCommunityCorrections, "Connect Network Community Corrections");
//			pgCommunityCrtns = new CommunityCorrectionsPage(this.cnEngine);
//			LogManager.logInfo( HomePage.class.getName(), "Successfully Navigated to Community Corrections page.");
//		}
//		catch(Exception e)
//		{
//			LogManager.logException(e, HomePage.class.getName(), "Unable to click Community Corrections link in home page");
//		}
//		return pgCommunityCrtns;
//	}
//	/**
//	 * @Purpose: To navigate TCL
//	 * @Constraints:
//	 * @return: CommunityCorrectionsPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public CommunityCorrectionsPage  navigateToTCL()
//	{
//		CommunityCorrectionsPage pgCommunityCrtns=null;
//		
//		try
//		{	this.cnEngine.waitForElementPresent(lblTCL, gblConstants.wdWaitTimeout);
//			this.cnEngine.click(lblTCL, "Connect Network TCL");
//			pgCommunityCrtns = new CommunityCorrectionsPage(this.cnEngine);
//			LogManager.logInfo( HomePage.class.getName(), "Successfully Navigated to TCL page.");
//		}
//		catch(Exception e)
//		{
//			LogManager.logException(e, HomePage.class.getName(), "Unable to click TCL link in home page");
//		}
//		return pgCommunityCrtns;
//	}
//	/**
//	 * @Purpose: To navigate Self Release
//	 * @Constraints:
//	 * @return: CommunityCorrectionsPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public CommunityCorrectionsPage  navigateToSelfRelease()
//	{
//		CommunityCorrectionsPage pgCommunityCrtns=null;
//		
//		try
//		{	this.cnEngine.waitForElementPresent(lblSelfRelease, gblConstants.wdWaitTimeout);
//			this.cnEngine.click(lblSelfRelease, "Connect Network Self Release");
//			pgCommunityCrtns = new CommunityCorrectionsPage(this.cnEngine);
//			LogManager.logInfo( HomePage.class.getName(), "Successfully Navigated to Self Release page.");
//		}
//		catch(Exception e)
//		{
//			LogManager.logException(e, HomePage.class.getName(), "Unable to click Self Release link in home page");
//		}
//		return pgCommunityCrtns;
//	}
//	/**
//	 * @Purpose: To navigate Trust Fund Page
//	 * @Constraints:
//	 * @return: TrustFundPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//
//	/**
//	 * @Purpose: To click Dash board
//	 * @Constraints:
//	 * @return: boolean
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public boolean clickDashboard()
//	{
//		boolean blnRes=false;
//		try {
//			this.cnEngine.waitForElementPresent(lblDashboard, gblConstants.wdWaitTimeout);
//			blnRes=this.cnEngine.click(lblDashboard,"Connect Network Dashboard");
//			LogManager.logInfo( HomePage.class.getName(), "Successfully clicked dashboard in home page");
//			this.cnEngine.waitForElementPresent(ddLanguage, gblConstants.wdWaitTimeout);
//		}catch (Exception e) {
//			LogManager.logException(e, HomePage.class.getName(), "Unable to click dashboard in home page");
//		}
//		return blnRes;
//	}
//	/**
//	 * @Purpose: To click Direct Bill Page
//	 * @Constraints:
//	 * @return: DirectBillPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public DirectBillPage navigatetoDirectBillPage()
//	{
//		DirectBillPage pgDirectBill=null;
//		try {
//			this.cnEngine.waitForElementPresent(lnkDirectBill, gblConstants.wdWaitTimeout);
//			this.cnEngine.click(lnkDirectBill,"Connect Network  DirectBill");
//			pgDirectBill =new DirectBillPage(this.cnEngine);
//			LogManager.logInfo( HomePage.class.getName(), "Successfully Navigated to DirectBill in home page");
//		}catch (Exception e) {
//			LogManager.logException(e, HomePage.class.getName(), "Unable to click DirectBill in home page");
//		}
//		return pgDirectBill;
//	}
//	/**
//	 * @Purpose: To navigate Profile Page
//	 * @Constraints:
//	 * @return: AccountsSettingsPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public AccountsSettingsPage navigateToProfile()
//	{
//		AccountsSettingsPage pgAccountStngs=null;
//		try
//		{
//			this.cnEngine.click(lblProfile, "Connect Network Profile Link");
//			pgAccountStngs = new AccountsSettingsPage(this.cnEngine);
//			LogManager.logInfo( HomePage.class.getName(), "Successfully clicked profile link in home page");
//		}
//		catch(Exception e)
//		{
//			LogManager.logException(e, HomePage.class.getName(), "Unable To navigate profile page in home page");
//		}
//		return pgAccountStngs ;
//	} 
//	/**
//	 * @Purpose: To navigate Transaction History Page
//	 * @Constraints:
//	 * @return: TransactionHistoryPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	
//	
//	/**
//	 * @Purpose: To navigate My Facilities Page
//	 * @Constraints:
//	 * @return: MyFacilitiesPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public MyFacilitiesPage navigateToMyFacilities()
//	{
//		MyFacilitiesPage pgMyFacilitiesPage=null;
//		try
//		{
//			this.cnEngine.clickusingJavaScript(lnkMyFacilities);
//			pgMyFacilitiesPage=new MyFacilitiesPage(this.cnEngine);
//			LogManager.logInfo(HomePage.class.getName(), "Successfully clicked my facilities in home page");
//		}catch(Exception e)
//		{
//			LogManager.logException(e,HomePage.class.getName(),"Unable to click my facilities in home page");
//		}
//		return pgMyFacilitiesPage;
//	}
//	/**
//	 * @Purpose: To navigate Automatic Payments Page
//	 * @Constraints:
//	 * @return: AutomaticPaymentsPage
//	 * @Author-Date: 
//	 * @Reviewed-Date:
//	 */
//	public AutomaticPaymentsPage navigatetoAutomaticPayments()
//	{
//		AutomaticPaymentsPage pgAutomaticPayments=null;
//		try {
//			this.cnEngine.waitForElementPresent(lnkAutomaticPayments, gblConstants.wdWaitTimeout);
//			this.cnEngine.click(lnkAutomaticPayments,"Connect Network  Automatic Payments");
//			pgAutomaticPayments =new AutomaticPaymentsPage(this.cnEngine);
//			LogManager.logInfo(HomePage.class.getName(), "Successfully Navigated to Automatic payments page");
//		}catch (Exception e) {
//			LogManager.logException(e, HomePage.class.getName(), "Unable to click Automatic Payments in home page");
//		}
//		return pgAutomaticPayments;
//	}
//	/**
//     * @Purpose: To verifying Messaging link dash board
//     * @Constraints
//     * @param : boolean
//     * @Author-Date: 18/09/2018
//     * @Reviewed-Date:
//     */
//	public boolean verifyMessaginglink()
//	{
//		boolean blnRes=false;
//		try {
//			blnRes=this.cnEngine.isElementPresent(lnkMessaging_css, "MessagingLink", true);
//			LogManager.logInfo(HomePage.class.getName(), "verifying Messaging link in my services menu in dashboard");
//			
//		}catch(Exception e)
//		{
//			LogManager.logException(e,HomePage.class.getName(),"unable to verify Messaging link in myservices menu in dashboard");
//		}
//		return blnRes;
//		
//	}
//	
//	/**
//     * @Purpose: To select language
//     * @Constraints
//     * @param: String language
//     * @return : boolean
//     * @Author-Date: 12/10/2018
//     * @Reviewed-Date:
//     */
//	public boolean selectLanguage(String language) {
//		boolean blnRes = false;
//		try {
//			this.cnEngine.sleep(gblConstants.defSleepTime);
//			this.cnEngine.scrollElementIntoView(ddLanguage);
//            this.cnEngine.clickusingJavaScript(ddLanguage);
//			this.cnEngine.sleep(gblConstants.defSleepTime*5);
//			this.cnEngine.selectByName(ddLanguage, language, "Select language");
//			this.cnEngine.sleep(gblConstants.defSleepTime);
//			LogManager.logInfo(HomePage.class.getName(),
//					"Successfully selected " + language + " language in Home Page .");
//			blnRes = true;
//		} catch (Exception e) {
//			LogManager.logException(e, HomePage.class.getName(),
//					"Exception to select " + language + " language in Home page");
//		}
//		return blnRes;
//	}
//	
	/**
	* @Purpose: To navigate USSO Profile page
	* @return: boolean
	* @Reviewed-Date:
	*/
	public boolean enterText(String text) {
		boolean blnRes = false;
		try {
			this.cnEngine.sleep(idbConstants.defSleepTime);
			this.cnEngine.type(textBox,text);
			LogManager.logInfo(HomePage.class.getName(),
					"Successfully entered " + text + "  in Home Page .");
			blnRes = true;
		} catch (Exception e) {
			LogManager.logException(e, HomePage.class.getName(),
					"Successfully entered " + text + "  in Home Page .");
		}
		return blnRes;
	}
	public boolean enterUserName(String text) {
		boolean blnRes = false;
		try {
			this.cnEngine.sleep(idbConstants.defSleepTime);
			this.cnEngine.type(fbUserName,text);
			LogManager.logInfo(HomePage.class.getName(),
					"Successfully entered username " + text + "  in Home Page .");
			blnRes = true;
		} catch (Exception e) {
			LogManager.logException(e, HomePage.class.getName(),
					"Successfully entered username " + text + "  in Home Page .");
		}
		return blnRes;
	}
	
	public boolean enterPassword(String text) {
		boolean blnRes = false;
		try {
			this.cnEngine.sleep(idbConstants.defSleepTime);
			this.cnEngine.type(fbPassword,text);
			LogManager.logInfo(HomePage.class.getName(),
					"Successfully entered Password " + text + "  in Home Page .");
			blnRes = true;
		} catch (Exception e) {
			LogManager.logException(e, HomePage.class.getName(),
					"Successfully entered Password " + text + "  in Home Page .");
		}
		return blnRes;
	}
	
	public boolean clickLogin() {
		boolean blnRes = false;
		try {
			this.cnEngine.sleep(idbConstants.defSleepTime);
			//this.cnEngine.type(logibBtn,text);
			
			this.cnEngine.click(logibBtn, "Click login button");
			LogManager.logInfo(HomePage.class.getName(),
					"Successfull login");
			blnRes = true;
		} catch (Exception e) {
			LogManager.logException(e, HomePage.class.getName(),
					"Successfull login");
		}
		return blnRes;
	}

}
