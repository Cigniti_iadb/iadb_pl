package idb.web.engine;

import java.io.File;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import idb.common.utils.CNConstants;
import idb.config.idbConstants;
import idb.core.accelerators.AbstractTestEngine;
import idb.core.accelerators.WebActionEngine;
import idb.core.logs.LogManager;
import idb.web.pages.HomePage;


public class CNWebActionEngine extends WebActionEngine {
	
	//To get index of matching inmate in dashboard
	public int dashboardInmateIndex = 0;
	public final String lblCalendar="xpath=//div[@id='ui-datepicker-div']";
	public final String calDatepicker="xpath=//span[@id='cnForm:startDateDatePicker']//button";
	public final String calPrevious="xpath=//div[@id='ui-datepicker-div']//span[text()='Prev']";
	public final String calTableBody="xpath=//div[@id='ui-datepicker-div']/table/tbody";
	public final String calMonth = "xpath=//div[@id='ui-datepicker-div']/div/div/span[@class='ui-datepicker-month']";
	public final String calYear = "xpath=//div[@id='ui-datepicker-div']/div/div/span[@class='ui-datepicker-year']";
	public final String popAddMsgContact = "xpath=//div[@id='cnForm:addInmateContactDialog']";
	public final String btnAddMsgContactLater = "xpath=//div[@id='cnForm:addInmateContactDialog']//a[contains(text(),'Later')]";
	public final String btnAdvPay = "xpath=//p[contains(text(),'"+idbConstants.replaceText+"')]/..//following-sibling::td/a";
	public final String btnInmate = "xpath=//td[contains(text(),'"+idbConstants.replaceText+"')]/..//following-sibling::a";
	public final String btnInmateName = "xpath=//*[contains(text(),'"+idbConstants.replaceText+"')]/ancestor::td/..//following-sibling::a";
	public final String lblDirectBill="xpath=//h3[contains(text(),'My Direct Bill')]";
	
	public ExtentTest tcReport = null;

	public ExtentTest getTcReport() {
		return tcReport;
	}

	public void setTcReport(ExtentTest tcReport) {
		this.tcReport = tcReport;
	}
	
	/*
	 * Purpose: To report status to current tcReport and debugLog
	 */
	public void reportStatus(Status result, String strLogMsg) {

		// Update HTML Report
		this.tcReport.log(result, strLogMsg);
	}

	/*
	 * Purpose: To report status to current tcReport and debugLog
	 */
	public void reportStatus(boolean blnRes, String message) {

		if (blnRes) {
			reportStatus(Status.PASS, message);
		} else {
			reportStatus(Status.FAIL, message);
			attachScreenshot(Status.FAIL, message.replaceAll( "[^a-zA-Z0-9 ]" , "" )); // Added to attach screen
														// shot on failure
		}
	}

	public boolean attachScreenshot(Status result, String strScreenshotName) {
		boolean blnRes = false;

		synchronized (strScreenshotName) {
			// If screenshot name is valid string, append a screenshot
			if (strScreenshotName.length() > 0) {
				blnRes = AbstractTestEngine.extentMngr.attachScreenshot(this.tcReport, result,
						captureScreenShot(idbConstants.reportsPath, strScreenshotName));
			}
		}
		return blnRes;
	}
	
	String className=CNWebActionEngine.class.getName();
	
	/**
	 *@purpose To wait while spinner is loading 
	 *@Wait while spinner is loading
	 *@return boolean
	 */
	public boolean waitWhileLoading(){
		boolean blnRes = false;
		try{
			final String imgSpinner_css = "css=img#spinner";
			waitForElementPresent(imgSpinner_css, 5);	
			blnRes = waitForElementNotPresent(imgSpinner_css, 180);			
		}catch(StaleElementReferenceException se){
			blnRes = true;
		}catch(Exception e){
			LogManager.logException(e, className, "Unable to wait for spinner loading");
			e.printStackTrace();
		}
		return blnRes;
	} 
	
	/**
	 *@Purpose: To select button based on the column name , column value and index of the button
	 *@param: String locator, String column name, String Column Value and int index of the button to be clicked 
	 *@return: boolean
	 *@Author-Date: 17-Aug-2018
	 *@Reviewed-Date:
	 */
	/*public boolean selectElementFromTable(String locator,String columnName,String columnData, int colSelectIndex) {
		boolean blnRes = false;
		int rowHeaderIndex=0;
		try {
			String Data = columnData.replace(".0", "").replaceAll("[^0-9a-zA-Z]", "");
			List<WebElement> tableElements = this.getAllElements(locator);
			List<WebElement> rowElements = tableElements.get(rowHeaderIndex).findElements(this.byLocator("tag=tr"));
			List<WebElement> rowheadings = rowElements.get(rowHeaderIndex).findElements(this.byLocator("tag=th"));
			for (int i = 0; i < rowheadings.size(); i++) {
				if (rowheadings.get(i).getText().equalsIgnoreCase(columnName)) {
					for (int j = 1; j < rowElements.size(); j++) {
						List<WebElement> colElements = rowElements.get(j).findElements(this.byLocator("tag=td"));
						String getInmateId = colElements.get(i).getText().replaceAll("[^0-9a-zA-Z]", "");
						if (Data.equalsIgnoreCase(getInmateId) && colSelectIndex>=0) {//to select inmate
							sleep(gblConstants.defSleepTime);
							click(colElements.get(colSelectIndex).findElement(By.tagName("a")));
							waitWhileLoading();
							blnRes = true;
							break;
						}else if (Data.equalsIgnoreCase(getInmateId) && colSelectIndex < 0) { //for inmate data verification 
							blnRes = true;
							break;
						}
					}
					break;
				}
			}
		} catch (Exception e) {
			blnRes = false;
			e.printStackTrace();
			LogManager.logException(e, className, "Unable to click button in table");
		}
		return blnRes;
	}*/
    
	/**
	 *@Purpose: To select button based on the column name , column value and index of the button
	 *@param:  String column name, String Column Value and int index of the button to be clicked 
	 *@return: boolean
	 *@Author-Date: 17-Aug-2018
	 *@Reviewed-Date:
	 */
	/*public boolean selectElementFromTable(String columnName,String columnData, int colSelectIndex) {
		boolean blnRes = false;
		int rowHeaderIndex=0;
		try {
			String Data = columnData.replace(".0", "").replaceAll("[^0-9a-zA-Z]", "");
			List<WebElement> tableElements = this.getAllElements("tag=table");
			if(!tableElements.isEmpty()) {
			List<WebElement> rowElements = tableElements.get(rowHeaderIndex).findElements(this.byLocator("tag=tr"));
			List<WebElement> rowheadings = rowElements.get(rowHeaderIndex).findElements(this.byLocator("tag=th"));
			for (int i = 0; i < rowheadings.size(); i++) {
				if (rowheadings.get(i).getText().equalsIgnoreCase(columnName)) {
					for (int j = 1; j < rowElements.size(); j++) {
						List<WebElement> colElements = rowElements.get(j).findElements(this.byLocator("tag=td"));
						String getInmateId = colElements.get(i).getText().replaceAll("[^0-9a-zA-Z]", "");
						if (Data.equalsIgnoreCase(getInmateId) && colSelectIndex>=0) { //to select inmate
							sleep(gblConstants.defSleepTime);
							click(colElements.get(colSelectIndex).findElement(By.tagName("a")));
							waitWhileLoading();
							blnRes = true;
							break;
						}else if (Data.equalsIgnoreCase(getInmateId) && colSelectIndex < 0) { //for inmate data verification 
							blnRes = true;
							break;
						}
					}
					break;
				}
			}}else {
				LogManager.logError(className, "Unable to find search results table for the provided search input " +columnData);
			}
		} catch (Exception e) {
			blnRes = false;
			LogManager.logException(e, className, "Unable to click button in table");
			e.printStackTrace();
		}
		return blnRes;
	}*/
	
	/**
	 *@Purpose: To navigate URL
	 *@param: String url
	 *@Author-Date:
	 *@Reviewed-Date:
	 */
	public void navigate(String url)
	{
		this.driver.navigate().to(url);
	}
	/**
	 *@Purpose: To refresh the page
	 *@Author
	 *@Reviewed-Date:
	 */
	public void refresh()
	{
		this.driver.navigate().refresh();
	}
	
	/**
	*@Purpose: To get row count from web table
	*@param:  String tableLoactor 
	*@return: int
	*@Author-Date: 16-Apr-2019
	*@Reviewed-Date:
	*/
	public int getRowCountFromTable(String locator) {
		int count=0;
		int tableIndex=0;
		try {
			this.scrollElementIntoView(locator);
			List<WebElement> table = this.getAllElements(locator);
			List<WebElement> rowElements = table.get(tableIndex).findElements(this.byLocator("tag=tr"));
			count=rowElements.size()-1;			
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get row count from table");
		}
		return count;
	}
	/**
	*@Purpose: To select table based on the provided table name and select a button based on the matching row data
	*@param:  String column name, String Column Value and int index of the button to be clicked 
	*@return: boolean
	*@Author-Date: 30-Aug-2018
	*@Reviewed-Date:
	*/
	public boolean selectTableMatchingTableName(String locator, String columnName, String columnData, int colSelectIndex, String operation) {
		boolean blnRes = false;
		int tableIndex=0;
		String getInmateId=null;
		try {
			this.waitForElementPresent(locator, idbConstants.wdWaitTimeout);
			this.scrollElementIntoView(locator);
			List<WebElement> table = this.getAllElements(locator);
			if(!table.isEmpty()) {
			List<WebElement> rowElements = table.get(tableIndex).findElements(this.byLocator("tag=tr"));
			List<WebElement> rowheadings = rowElements.get(tableIndex).findElements(this.byLocator("tag=th"));
			for (int i = 0; i < rowheadings.size(); i++) {
				if (rowheadings.get(i).getText().equalsIgnoreCase(columnName)) {
					
					for (int j = 1; j < rowElements.size(); j++) {
						List<WebElement> colElements = rowElements.get(j).findElements(this.byLocator("tag=td"));
						if(columnName.equalsIgnoreCase("Name")) { 
							String name=colElements.get(i).getText();
							if(name.equalsIgnoreCase(columnData)) { // added for message link contact validation
								String[] fullName=name.split(",");
								getInmateId=fullName[0]+","+fullName[1];
							}
							else { //for Debit link search validation
							String[] fullName=name.split(",");
							getInmateId=fullName[1]+" "+fullName[0];
							}
						}else {
						 getInmateId = colElements.get(i).getText().replace("$", "").replace(".00", "");
						}
						if (getInmateId.toLowerCase().contains(columnData.toLowerCase())||getInmateId.contains("*")) {
							switch (operation) {

							case "verify":
								blnRes = true;
								break;

							case "click":
								click(colElements.get(colSelectIndex).findElement(By.tagName("a")));
								waitWhileLoading();
								blnRes = true;
								break;

							default :
								LogManager.logError(className, "Unable to verify or click in table");
								break;
							}break;
						}
					}break;
				} 
			}}else {
				LogManager.logError(className, "Unable to find table for the provided search input " +columnData);
			}}catch (Exception e) {
				blnRes = false;
				LogManager.logException(e, className, "Unable to perform "+ operation +" operation  in table");
				e.printStackTrace();
			}
		return blnRes;
	}
	
	
	/**
     *@Purpose: To compare column names of web table with provided column names
     *@Constraints
     *@param:  String locator and String column name(Ex:Name,Facility)
     *@return: boolean
     *@Author-Date: 28/08/2018
     *@Reviewed-Date:
     */
	public boolean verifyTblColumnNames(String tblName,String columnNames,ExtentTest tcreport) {
		boolean res = false;
		List<String> reqlist = Arrays.asList(columnNames.split("\\s*,\\s*"));
		List<String> actlist = getcolumnnames(tblName);
		if (reqlist.equals(actlist)) {
			String actual = Arrays.toString(actlist.toArray());
			String exp = Arrays.toString(reqlist.toArray());
			LogManager.logInfo(className,"Successfully matched Expected Columns Names are: " + exp + "  and  Actual Columns Names are :" + actual);
			tcreport.log(Status.PASS, "Successfully matched Expected Columns Names are: " + exp + "  and  Actual Columns Names are :" + actual) ;  
			res = true;
		} else {

			String actual = Arrays.toString(actlist.toArray());
			String exp = Arrays.toString(reqlist.toArray());
			LogManager.logError(className,"Verification for Column Names Failed are Expected Names are  :" + exp + "   But Actual items are :" + actual);
			tcreport.log(Status.FAIL, "Verification for Column Names are Failed Expected Names are  :" + exp + "   But Actual items are :" + actual);
		}
		return res;
	}

      
   		/**
        *@Purpose: To get the column names from a table
        *@Constraints
        *@param:  String locator
        *@return: List<String>
        *@Author-Date: 28/08/2018
        *@Reviewed-Date:
        */
       public List<String> getcolumnnames(String tblname) {
    	   List<String> columnlist = new ArrayList<String>();
    	   try {
    		   String tbllist = tblname + "/thead[not(contains(@id,'cnForm:contactsTable_head'))]//tr/th";
    		   List<WebElement> headerlist = this.getAllElements(tbllist);
    		   for (int i = 1; i <= headerlist.size(); i++) {
    			   String colname = this.getVisibleText(tblname + "/thead[not(contains(@id,'cnForm:contactsTable_head'))]//tr/th[" + i + "]");
    			   if ((colname.matches(".*[ A-Za-z].*")) && (colname.length() > 1)) {
    				   columnlist.add(colname);
    			   java.util.Set<String> colnamenodups = new LinkedHashSet<String>(columnlist);
    			   columnlist.clear();
    			   columnlist.addAll(colnamenodups);
    			   }
    		   }
    	   } catch (Exception e) {
    		   LogManager.logException(e, className,
    				   "Exception to Get Column Names in Search results");
    	   }
    	   return columnlist;
       }
        /**
         *@Purpose: To get the string from search result table
         *@Constraints
         *@param:   String tablelocator, String searchcriteriadata, String sortingColumnName(ex:Name,ID number..)
         *@         ExtentTest Object
         *@return: string 
         *@Author-Date: 10/09/2018
         *@Reviewed-Date:
         */
	public String sortOperations(String tblLocator, String sortColumnName, ExtentTest tcReport) {
		int index = 0;
		boolean flag = false;
		int rowindex = 1;
		String getdata = null;
		int columnsize = 0;
		try {
			List<WebElement> table = this.getAllElements(tblLocator);
			List<WebElement> row = table.get(index).findElements(this.byLocator("tag=tr"));
			List<WebElement> headings = row.get(index).findElements(this.byLocator("tag=th"));
			List<WebElement> column = row.get(rowindex).findElements(this.byLocator("tag=td"));
			if (row.size() != 2) {
				for (int j = 0; j < headings.size(); j++) {

					if (headings.get(j).getText().equalsIgnoreCase(sortColumnName)) {
						columnsize = column.size();
						for (int i = 0; i < columnsize; i++) {
							if (!flag) {
								getdata = column.get(j).getText();
								flag = true;
							}
						}
					}
				}
			} else {
				tcReport.log(Status.INFO, "Successfully verified for result and result contain only one record");
			}
		} catch (Exception e) {
			LogManager.logException(e, className,
					"Exception to perform sort operation result table");
		}
		return getdata;
	}
    	
    	/**
         *@Purpose: To click sort button in search result table
         *@Constraints
         *@param:   String sortingColumnname, ExtentTest object
         *@return: boolean
         *@Author-Date: 10/09/2018
         *@Reviewed-Date:
         */
	public boolean clickSortBtn(String sortBtnLocator, String sortColumnName, ExtentTest tcReport) {
		boolean blnRes = false;
		try {
			this.click(sortBtnLocator.replace(idbConstants.replaceText, sortColumnName), "sort button in table");
			this.sleep(idbConstants.defSleepTime);
			this.click(sortBtnLocator.replace(idbConstants.replaceText, sortColumnName), "sort button in table");
			LogManager.logInfo(className,
					"Sucessfully clicked sort button for " + sortColumnName + " in Search result table");
			tcReport.log(Status.PASS,
					"Sucessfully clicked sort button for " + sortColumnName + " in Search result table");
			this.sleep(idbConstants.defSleepTime *3);
		} catch (Exception e) {
			LogManager.logInfo(className, "Unable to click sort button");
			tcReport.log(Status.FAIL, "Failed to click sort button for " + sortColumnName + " in Search result table ");
		}
		return blnRes;
	}

	/**
	 *@Purpose: to get the value of the provided input field name from email message content
	 *@Constraints
	 *@param:   String emailMessage, String name of the field
	 *@return: String value of the field
	 *@Author-Date: 25/09/2018
	 *@Reviewed-Date:
	 */
	public String getDataFromEmailContent(String emailMessage, String inputText) {
		String output = null;
		try {
			if (emailMessage.contains("activate")) {
				output = emailMessage.substring(emailMessage.indexOf(inputText + "=") + inputText.length() + 1).split(">")[0].replace("\"", "").trim();
				LogManager.logInfo(className, "Value of " + inputText +" is " +output);
			}
			else if (emailMessage.contains("Dear ConnectNetwork client")) {
				output = emailMessage.substring(emailMessage.indexOf(inputText + ":") + inputText.length() + 1).split("\n")[0].trim();
				LogManager.logInfo(className, "Value of " + inputText+" is " +output);
			}
		/*	else if(emailMessage.contains("=")){
				
				output = emailMessage.substring(emailMessage.indexOf(inputText + "=") + inputText.length() + 1).trim();
				output = output.split("\n")[0].trim();
				LogManager.logInfo(className, "Value of " + inputText); }*/
				
			else {
				output = emailMessage.substring(emailMessage.indexOf(inputText) + inputText.length() + 1).split("\n")[0].trim();
				LogManager.logInfo(className, "Value of " + inputText+" is " +output);
			}
		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get value of " + inputText);
		}
		return output;
	}
	
	/**
     *@Purpose: To compare actual and expected messages 
     *@Constraints
     *@param:  String actualMessageLocator and String ExpectedMessage and ExtentTest object
     *@return: boolean
     *@Author-Date: 28/09/2018
     *@Reviewed-Date:
     */
	public boolean compareMessages(String ActEleLocator,String ExpectedMsg,ExtentTest tcReport) {
		boolean res=false;
		try {
			this.waitForElementPresent(ActEleLocator, idbConstants.wdWaitTimeout);
			String actualMsg=this.getVisibleText(ActEleLocator);
			if(ExpectedMsg.equalsIgnoreCase(actualMsg.trim())) {
			tcReport.log(Status.PASS,"Successfully verified for actual message as [ "+actualMsg+" ] and expected message as ["+ExpectedMsg+" ]");
			LogManager.logInfo(className, "Successfully verified for Actual Message as [ "+actualMsg+" ] and Expected message as ["+ExpectedMsg+" ]");
			res=true;
			}else {
			tcReport.log(Status.FAIL,"Failed to verify actual message as [ "+actualMsg+" ] and expected message as ["+ExpectedMsg+" ]");
			LogManager.logInfo(className, "Failed to verify actual message as [ "+actualMsg+" ] and expected message as ["+ExpectedMsg+" ]");
			}
		}catch(Exception e) {
			LogManager.logException(e, className, "Exception to compare message ");
		}
		return res;
	}
	
		
	/**
     *@Purpose: To get the column names from a table
     *@Constraints
     *@param:  String locator
     *@return: List<String>
     *@Author-Date: 28/08/2018
     *@Reviewed-Date:
     */
    public List<String> getAllText(String locator) {
 	   List<String> columnlist = new ArrayList<String>();
 	   try {
 		   List<WebElement> headerlist = this.getAllElements(locator);
 		   for (int i = 0; i <headerlist.size(); i++) {
 			   String colname = headerlist.get(i).getText();
 			   if (colname.matches(".*[ A-Za-z].*") && colname.length() > 1)
 				   columnlist.add(colname);
 			   java.util.Set<String> colnamenodups = new LinkedHashSet<String>(columnlist);
 			   columnlist.clear();
 			   columnlist.addAll(colnamenodups);
 		   }
 	   } catch (Exception e) {
 		   LogManager.logException(e, className,
 				   "Exception to Get All Text ");
 	   }
 	   return columnlist;
    }
    
    /**
     *@Purpose: To compare column names of web table with provided column names
     *@Constraints
     *@param:  String locator and String column name(Ex:Name,Facility)
     *@return: boolean
     *@Author-Date: 28/08/2018
     *@Reviewed-Date:
     */
	public boolean compareGroupMessages(String locator,String allNames,ExtentTest tcreport) {
		boolean res = false;
		List<String> reqlist = Arrays.asList(allNames.split("\\s*,\\s*"));
		List<String> actlist = getAllText(locator);
		if (reqlist.equals(actlist)) {
			String actual = Arrays.toString(actlist.toArray());
			String exp = Arrays.toString(reqlist.toArray());
			LogManager.logInfo(className,"Successfully matched Expected Texts are: " + exp + "  and  Actual Texts are :" + actual);
			tcreport.log(Status.PASS, "Successfully matched Expected Texts are: " + exp + "  and  Actual Texts are :" + actual) ;  
			res = true;
		} else {

			String actual = Arrays.toString(actlist.toArray());
			String exp = Arrays.toString(reqlist.toArray());
			LogManager.logError(className,"Verification for Texts Failed are Expected Texts are  :" + exp + "   But Actual Texts are :" + actual);
			tcreport.log(Status.FAIL, "Verification for Texts are Failed Expected Texts are  :" + exp + "   But Actual Texts are :" + actual);
		}
		return res;
	}	
	
	/**
     *@Purpose: To wait for Page ready state
     *@param:  Driver instance
     *@return: NA
     */
  public void waitForPageLoad(WebDriver driver) {
      ExpectedCondition<Boolean> pageLoadCondition = new
              ExpectedCondition<Boolean>() {
                  public Boolean apply(WebDriver driver) {
                      return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                  }
              };
      WebDriverWait wait = new WebDriverWait(driver, idbConstants.implicitWaitTimeout);
      wait.until(pageLoadCondition);
  }
  
  
   
	/**
	*@Purpose: To get the latest modified/download file from Downloads DIR  
	*@param:  Downloads folder path as constant
	*@return: String filename
	*@Author-Date: 14/01/2019
	*@Reviewed-Date:
	*/
	public String getRecentDownloadedFilename(){
		   String fileName = "";
			
		   try{
			   File dir = new File(idbConstants.downloadsPath);
			   File[] files = dir.listFiles();
			   if (files == null || files.length == 0) {
		        return null;
			   }

		    File lastModifiedFile = files[0];
		    for (int i = 1; i < files.length; i++) {
		       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
		    	   if(lastModifiedFile.getName().endsWith(".part")){ //if filename ends with .part added waiting time 10 secs
		        	   this.sleep(idbConstants.wdWaitTimeout/2);
		        	   lastModifiedFile = files[i];
		           }else{
		        	   lastModifiedFile = files[i];
		           }
		       }
		    }
		    fileName = lastModifiedFile.getName();
		   }catch(Exception e) {
			   LogManager.logException(e, className, "Exception to get the lastest modified file name at specified directory: "+idbConstants.downloadsPath);
		   }
		    return fileName;
		}
	
	/**
	 *@Purpose: to get the value of the provided input field name from PDF content
	 *@Constraints
	 *@param:   String pdfContent, String name of the attribute
	 *@return: String value of the field
	 *@Author-Date: 29/01/2019
	 *@Reviewed-Date:
	 */
	public String getDataFromPDFContent(String pdfContent, String inputText) {
		String value = null;
		pdfContent = pdfContent.replaceAll("  ", " ");
		try {
			if(inputText.equalsIgnoreCase("Order Number")) {
			value = pdfContent.substring(pdfContent.indexOf(inputText + ":") + inputText.length() + 1).split("[A-Z][a-z]",0)[0].trim();
			}
			else if (pdfContent.contains("SPN Exception Report") && inputText.equalsIgnoreCase("Date")) {
				value = StringUtils.substringAfterLast(pdfContent, "#").split(" ", 0)[0].trim();
			}
			else {
				value = pdfContent.substring(pdfContent.indexOf(inputText + ":") + inputText.length() + 1).split("\n")[0].trim();
			}
			LogManager.logInfo(className, "Value of " + inputText +" is " +value);

		} catch (Exception e) {
			LogManager.logException(e, className, "Exception to get value of " + inputText);
		}
		return value;
	}
	
	/**
	 *@Purpose: to clear text fields 
	 *@param:  String locator of the field
	 *@return: boolean
	 *@Author-Date: 30/01/2019
	 *@Reviewed-Date:
	 */
	public boolean clearFields(String locator){
		boolean blnRes=false;
		try {
			this.scrollElementIntoView(locator);
			//this.click(locator, "click field");
			this.sleep(idbConstants.defSleepTime-1);
			this.driver.findElement(this.byLocator(locator)).clear();
			this.sleep(idbConstants.defSleepTime-1);
			//this.click(locator, "click field");
			if(this.getAttributeValue(locator, "value").replace("[(_-)]", "").equals("")){
				blnRes=true;
			}else{
				blnRes=false;
			}}catch (Exception e) {
				LogManager.logException(e,HomePage.class.getName(), "Unable to clear field");
			}
		return blnRes;
	}
	
	/**
	 *@Purpose: to enter text in text fields 
	 *@param:  String locator, String data
	 *@return: boolean
	 *@Author-Date: 8/02/2019
	 *@Reviewed-Date:
	 */
	public boolean typeText(String locator, String data) {
		boolean blnRes=false;
		try {
			this.waitForElementToBeClickable(locator, idbConstants.wdWaitTimeout);
			this.sleep(idbConstants.defSleepTime-1);
			clearFields(locator);
			this.sleep(idbConstants.defSleepTime-1);
			WebElement ele = getElement(locator);
			ele.sendKeys(data);
			waitWhileLoading();
			if(this.getAttributeValue(locator, "value").replace(".00", "").replace(",", "").replaceAll("[^.@0-9]","").equals(data.replace("$", "").replace(".00", ""))|| this.getAttributeValue(locator, "type").equalsIgnoreCase("password")) {
				blnRes = true;
			}
		}catch (Exception e) {
			LogManager.logException(e,HomePage.class.getName(), "Unable to type in web element");
		}
	return blnRes;
	}

	/**
	*@Purpose: To click On Calendar
	*@return: boolean
	*@Author-Date:16/10/2018
	*@Reviewed-Date:
	*/
	public boolean clickOnCalendar(String locator, String waitLocator)
	{
		boolean blnRes=false;
		try {
		blnRes=this.clickusingJavaScript(locator);
		LogManager.logInfo(className,"Clicked On calendar Button");
		this.waitForElementPresent(waitLocator, idbConstants.wdWaitTimeout);
		}catch(Exception e)
		{
			LogManager.logException(e,className, "Exception to click On calendar Button");
		}
		return blnRes;
	}
	
	
	/**
	*@Purpose: To Select Date From Calendar
	*@param:  String date in MM/DD/YYYY format
	*@return: boolean
	*@Author-Date:17/10/2018
	*@Reviewed-Date:
	*/
	public boolean selectDateFromCalendar(String date) {
		boolean blnRes = false;
		String[] selectDate = date.split("/");
		String month = String.valueOf(Month.of(Integer.parseInt(selectDate[0])));
		String day = selectDate[1];
		String year = selectDate[2];
		try {
			//click calendar icon
			this.clickusingJavaScript(calDatepicker);
			
			// condition to validate the year
			if (!(this.getVisibleText(calYear).equals(year))) {
				while (!(this.getVisibleText(calYear).equals(year))) {
					// select year in calendar
					this.click(calPrevious, "Calendar previous year");
				}
			}else{
				LogManager.logInfo(className,"Provided year matched Calendar year");
			}
			// condition to validate the month
			if (!(this.getVisibleText(calMonth).equalsIgnoreCase(month))) {
				while (!(this.getVisibleText(calMonth).equalsIgnoreCase(month))) {
					// select month in calendar
					this.click(calPrevious, "Calendar previous month");
				}
			}else{
				LogManager.logInfo(className,"Provided month matched Calendar month");
			}
			//wait for calendar body
			this.waitForElementPresent(calTableBody, idbConstants.defSleepTime);
			// get all elements of calendar table and get columns
			List<WebElement> tableElements = this.getAllElements(calTableBody);
			List<WebElement> columns = tableElements.get(0).findElements(this.byLocator("tag=td"));
			for (WebElement cell : columns) {
				// Select provided day in Calendar
				if (cell.getText().equals(day)) {
					cell.click();
					blnRes = true;
					break;
				}
			}	
		} catch (Exception e) {
			LogManager.logException(e, className,"Exception to select date from Calendar");
		}
		return blnRes;
	}

	/**
	*@Purpose: To get row data from the Table
	*@param:  String locator, String rowNo
	*@return: String
	*@Author-Date:22/02/2019
	*@Reviewed-Date:
	*/	
	public String getSingleRowDataFromTable(String locator,int rowNo){
		String rowData=null;
		int tableIndex=0;
		try {
			this.scrollElementIntoView(locator);
			List<WebElement> table = this.getAllElements(locator);
			List<WebElement> rowElements = table.get(tableIndex).findElements(this.byLocator("tag=tr"));
			rowData=rowElements.get(rowNo).getText();
			LogManager.logInfo(className, "Successfully get data from table");
		} catch (Exception e) {
			LogManager.logException(e, className,"Exception to get Row data");
		}
		return rowData;
	}
	
	/**
	*@Purpose: To get all options from a drop down
	*@param:  String locator
	*@return: List <WebElement>
	*@Author-Date: 25/02/2019
	*@Reviewed-Date:
	*/	
	public List<WebElement> getAllSelectOption(String locator){
		List<WebElement> alloptions=null;
		try {
			Select sel=new Select(getElement(locator));
			 alloptions = sel.getOptions();
			
		} catch (Exception e) {
			LogManager.logException(e, className, "Unable to get all options from drop down");
			
			
		}
		return alloptions;
	}
	
	/**
	*@Purpose: To mouse click and Hold Action
	*@param:  String locator and String locatorName
	*@author: Bhaskar Soorisetti
	*@return: boolean
	*@AuthorDate: 03/05/2019
	*@ReviewedDate:
	*/	
	synchronized public boolean mouseClickAndHold(String locator, String locatorName) {
		boolean flag = false;
		try {
			WebElement mo = getElement(locator);
			new Actions(this.driver).clickAndHold(mo).build().perform();
			flag = true;
            LogManager.logInfo(className, "Mouse click and Hold action is performed on " + locatorName);
		} catch (Exception e) {
        	LogManager.logException(e,className, "Mouse click and Hold is not perform on " + locatorName);
			return false;
		}
		return flag;
	}
	
	/**
	 * @Purpose: To get label value from payment confirmation page
	 * @Output: String ; returns the label value for the provided locator
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getLabelValue(String locator) {
		String value = null;
		try {
			value = this.getVisibleText(locator).replace("$", "").trim();
		} catch (Exception e) {
			LogManager.logException(e, className, 
					"Unable to get label value from confirmation page.");
		}
		return value;
	}
	/**
	 *@Purpose: To select the value from dropdown 
	 *@param: String locatorName, String value 
	 *@return: boolean
	 *@Author-Date: 17-Aug-2018
	 *@Reviewed-Date:
	 */
	public boolean selectByValue(String locatorName, String value) {
		boolean blnRes = false;
		try{
			this.waitForElementPresent(locatorName);
			Select selElement = new Select(getElement(locatorName));
			selElement.selectByValue(value);
			this.sleep(idbConstants.defSleepTime);
			blnRes=true;			      
		}catch(Exception e){
			blnRes = false;
    		LogManager.logException(e, className, "Exception to select option " + value + " from  dropdown" );
		}
		return blnRes;
	}
	
	 /**
     * @Purpose To add or subtract Year/month/day to current date
     * @Constraints 
     * @Input String  : int add/sub value , String add/sub and String year/month/day
     * @Output boolean : Returns true | false
     * @see: 1, Add, Year
     */
    
	 public String addOrSubDate(int num, String addOrSub, String addOrSubdateTo) {
		 Date date = new Date();
		 LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		 int value=0;
	        switch(addOrSubdateTo){
			case "Year":
				if(addOrSub.equalsIgnoreCase("add")) {
					value=localDate.getYear()+num;
				}else {
					value=localDate.getYear()-num;
				}
				break;
			case "Month":
				if(addOrSub.equalsIgnoreCase("add")) {
					value=localDate.getMonthValue()+num;
				}else {
					value=localDate.getMonthValue()-num;
				}
				break;
			case "Day":
				if(addOrSub.equalsIgnoreCase("add")) {
					value=localDate.getDayOfMonth()+num;
				}else {
					value=localDate.getDayOfMonth() -num;
				}
				break;
		}
	        System.out.println(String.valueOf(value));
	        return String.valueOf(value);
	 }
	 /**
	 *@Purpose: To enter text
	 *@param: String locatorName, String testdata 
	 *@return: boolean
	 *@Author-Date:
	 *@Reviewed-Date:
	 */
	 public boolean enterText(String locatorName,String testdata) {
			boolean res=false;
			try {
				this.waitForElementPresent(locatorName, idbConstants.wdWaitTimeout);
				this.click(locatorName, "Input Filed");
				res=this.typeText(locatorName, testdata);
				this.sleep(idbConstants.defSleepTime);
			} catch (Exception e) {
				LogManager.logException(e, className, "Unable enter text");
			}
			return res;
		}
		
		/**
		 * @Purpose: To click continue button 
		 * @Output: boolean 
		 * @Author-Date: 
		 * @Reviewed-Date:
		 */
		public boolean clickButton(String locator) {
			boolean blnRes = false;
			String btnName=" ";
			try {
				btnName=this.getAttributeValue(locator, "value");
				this.scrollElementIntoView(locator);
				blnRes =this.clickusingJavaScript(locator);
				this.waitWhileLoading();
				this.sleep(idbConstants.defSleepTime-1);
				LogManager.logInfo(className, "Successfully clicked "+btnName+" button.");
			} catch (Exception e) {
				blnRes = false;
				LogManager.logException(e, className, "Unable to click "+btnName+" button.");
			}
			return blnRes;
		}
		
		/**
		 * @Purpose presence of multiple locators 
		 * @param String multiple locators
		 * @return boolean value 
		 */	
		public boolean isElementPresent(AbstractTestEngine testObj, String... locators) {
			boolean status = false;
			int iter=0;
			String locName = "";
			try {
			int locsize=locators.length;
			for (iter=0; iter<locsize; iter++) {
				locName = locators[iter].substring(locators[iter].indexOf("'")+1, locators[iter].lastIndexOf("'"));
				scrollElementIntoView(locators[iter]);
				status=isElementPresent(locators[iter]);
				String value = getElement(locators[iter]).getText();
				if(value.equals("")) {
					value = getElement(locators[iter]).getAttribute("value");
				}
				if(status) {
					LogManager.logInfo(className, "Successfully verified presence of " + value +" in page");
					testObj.reportStatus(status, "Successfully verified presence of " + value +" in page","");
				}else {
					LogManager.logInfo(className, "Failed to verify presence of " + locName +" in page");
					testObj.reportStatus(status, "", "Failed to verify presence of " + locName +" in page");
				}
			}}catch(Exception e){
				testObj.reportStatus(Status.FAIL, "Failed to verify presence of " + locName +" in page");
				LogManager.logException(e, className, "Exception to verify element present in page");
			}
			return status;
		}
		
		
//		/**
//		 *@Purpose: To select button based on the column name , column value
//		 *@param:  String column name, String Column Value
//		 *@return: boolean
//		 *@Author-Date: 17-Aug-2018
//		 *@Reviewed-Date:
//		 */
//
//		public boolean selectElementFromTable(String columnName,String columnData) {
//			boolean blnRes = false;
//			int rowHeaderIndex=0;
//			String btnName = "";
//			String expData = null;
//			String uiData = null;
//			try {
//				if(columnName.equalsIgnoreCase(CNConstants.AdvPayColName)){
//					StringBuilder data = new StringBuilder();
//					if(!columnData.startsWith("(")) { //if Adv phone number is not formatted
//					data.append(columnData);
//					expData = data.insert(0, "(").insert(4, ")").insert(5, " ").insert(9, "-").toString();
//					}else {
//						expData = columnData;
//					}
//					
//				}else {
//					expData = columnData.replace(".0", "");
//				}
//				List<WebElement> tableElements = this.getAllElements("tag=table");
//				if(!tableElements.isEmpty()) {
//				List<WebElement> rowElements = tableElements.get(rowHeaderIndex).findElements(this.byLocator("tag=tr"));
//				List<WebElement> rowheadings = rowElements.get(rowHeaderIndex).findElements(this.byLocator("tag=th"));
//				for (int i = 0; i < rowheadings.size(); i++) {
//					if (rowheadings.get(i).getText().equalsIgnoreCase(columnName)) {
//						for (int j = 1; j < rowElements.size(); j++) {
//							List<WebElement> colElements = rowElements.get(j).findElements(this.byLocator("tag=td"));
//								uiData = colElements.get(i).getText().trim();
//							if (expData.equalsIgnoreCase(uiData)) { //match expected inmate in UI
//								sleep(idbConstants.defSleepTime);
//								//for Advance Pay phone number selection
//								if(columnName.equalsIgnoreCase(CNConstants.AdvPayColName) && isElementPresent(lblDirectBill, false)){
//									click(btnAdvPay.replace(idbConstants.replaceText, expData));
//								}else{
//									//to get name of the button
//									btnName = getAttributeValue(btnInmate.replace(idbConstants.replaceText, expData), "text");
//									if(btnName==null) {
//										btnName = getAttributeValue(btnInmateName.replace(idbConstants.replaceText, expData), "text");
//										click(btnInmateName.replace(idbConstants.replaceText, expData)); 
//									}else {
//									//click button
//									click(btnInmate.replace(idbConstants.replaceText, expData)); 
//									}
//									waitWhileLoading();
//									
//								//to click on later button when Add as messaging contact also popup is displayed
//								if((btnName.equalsIgnoreCase("Add") && (isElementPresent(popAddMsgContact, "Add Messaging Contact", true))))
//								{
//									click(btnAddMsgContactLater); //click later button in add as messaging contact
//									waitWhileLoading();
//								}}
//								blnRes = true;
//								break;
//							}
//						}
//						break;
//					}
//				}}else {
//					LogManager.logError(className, "Unable to find search results table for the provided search input " +columnData);
//				}
//			} catch (Exception e) {
//				blnRes = false;
//				LogManager.logException(e, className, "Exception to select Element From Table");
//			}
//			return blnRes;
//		}
//		
		
		public boolean IsReadOnlyTextBox(String locator) {
			boolean blnRes = false;
			try {
			    String readonly = getElement(locator).getAttribute("readonly");
			    if(readonly.equals("")||readonly.equalsIgnoreCase("readonly")|| readonly.equalsIgnoreCase("true") || readonly!= null) {
			    	LogManager.logInfo(className, "Successfully verified the text box is in read only mode");
					blnRes=true;
				}
			}catch(Exception e) {
				LogManager.logException(e, className, "Exception to identify text box is read only");
			}
			return blnRes;
		}
}
