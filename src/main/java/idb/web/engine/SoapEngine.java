
package idb.web.engine;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class SoapEngine {

	public static Map<String,String> readResonpseFromAllXmlTags(String xmlFile,String parentNodeName) {
		//Creating an instance of DocumentBuilderFactory
		DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
		Map<String,String> map= new HashMap<String,String>();
		try {
			//Creating an object of DocumentBuilder
			DocumentBuilder builder=factory.newDocumentBuilder();
			//Parse the xml file
			Document doc=builder.parse(xmlFile+".xml");
			//Getting all Node 
			NodeList quotesList=doc.getElementsByTagName(parentNodeName);
			for(int i=0;i<quotesList.getLength();i++) {
				Node p=quotesList.item(i);
				if(p.getNodeType()==Node.ELEMENT_NODE) {
					Element quotes=(Element)p;
					//Getting child node
					NodeList value=quotes.getChildNodes();
					for(int j=0;j<value.getLength();j++) {
						Node n=value.item(j);
						if((n.getNodeType()==Node.ELEMENT_NODE)) {
							Element nodevalue=(Element) n;
							System.out.println(nodevalue.getTagName()+"="+nodevalue.getTextContent());
							map.put(nodevalue.getTagName(),nodevalue.getTextContent());
						}
					}
				}
			}
			System.out.println(map);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;

	}

	public  static void getSoapResponse(String endPointURI,String requestedXMLName,String responeXMLName) throws Exception
	{		//String endpoint = "http://ws.cdyne.com/delayedstockquote/delayedstockquote.asmx";
		String endpoint =	endPointURI;
		//Give location of XML request file
		File requestFile = new File(System.getProperty("user.dir")+"//Response//"+requestedXMLName+".xml");
		HttpClient cilent = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(endpoint);
		post.setEntity(new InputStreamEntity(new FileInputStream(requestFile)));
		post.setHeader("Content-type","text/xml");
		HttpResponse response = cilent.execute(post);
		System.out.println(response.getStatusLine().getStatusCode());
		BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		String line = "";
		StringBuffer sb = new StringBuffer();
		while((line=br.readLine())!=null)
		{
			sb.append(line);
		}
		PrintWriter pw = new PrintWriter(System.getProperty("user.dir")+"//Response//"+responeXMLName+".xml");
		pw.write(sb.toString());
		DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource src = new InputSource();
		src.setCharacterStream(new StringReader(sb.toString()));
		builder.parse(src);					
		pw.close();
		pw.flush();
	}


	public static Document toXmlDocument(String str) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
		Document document = docBuilder.parse(new InputSource(new StringReader(str)));
		return document;
	}		
	/**
	 * 
	 */
	public static Map<String,String> readResponseFromXml(Document doc,String parentNodeName) throws ParserConfigurationException, SAXException, IOException {
		Map<String,String> map= new HashMap<String,String>();
		//Getting all Node 
		NodeList quotesList=doc.getElementsByTagName(parentNodeName);
		for(int i=0;i<quotesList.getLength();i++) {
			Node p=quotesList.item(i);
			if(p.getNodeType()==Node.ELEMENT_NODE) {
				Element quotes=(Element)p;
				//Getting child node
				NodeList value=quotes.getChildNodes();
				for(int j=0;j<value.getLength();j++) {
					Node n=value.item(j);
					if((n.getNodeType()==Node.ELEMENT_NODE)) {
						Element nodevalue=(Element) n;
						System.out.println(nodevalue.getTagName()+"="+nodevalue.getTextContent());
						map.put(nodevalue.getTagName(),nodevalue.getTextContent());
					}
				}
			}
		}

		System.out.println(map);
		return map;

	}

}