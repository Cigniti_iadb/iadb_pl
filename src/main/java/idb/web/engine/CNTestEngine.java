package idb.web.engine;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import org.apache.http.client.ClientProtocolException;
import org.testng.ITestContext;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;

import idb.common.utils.CNDatabaseQueries;
import idb.common.utils.CNDatabases;
import idb.config.idbConstants;
import idb.core.accelerators.AbstractTestEngine;
import idb.core.fileutils.JsonReader;
import idb.core.logs.LogManager;

import idb.core.testmanagement.JiraUtils;
import idb.web.pages.HomePage;

public class CNTestEngine extends AbstractTestEngine {

	public CNWebActionEngine cnActionEngine = null;

	//public BusinessFunctions businessFunctions = null;
	public ITestContext testContext = null;
	public CNDatabaseQueries query=null;
	//Login parameters
	public String loginUsrName="";
	public String loginEmailName="";
	public String loginPswd="";
	String className=CNTestEngine.class.getName();
	public String actualReturnMsg=null;
	public String actualReturnCode=null;
	//public static String testCaseName=null;

	@BeforeSuite(alwaysRun = true)
	@Parameters({ "ExecutionEnv", "BrowserUT", "AppUT", "AppTestEnv", "DeviceUT", "TestPlanId"})
	public void beforeSuite(ITestContext testContext, String execEnvName, @Optional("") String testBrowser, String appUT, String appTestEnv,
			@Optional("") String devUT, String trPlanId) {

		suiteName = testContext.getSuite().getName();
		execEnv = (System.getProperty("ExecutionEnv") !=null && System.getProperty("ExecutionEnv").trim().length() > 0) ? System.getProperty("ExecutionEnv").trim() : execEnvName;
		appUnderTest = (System.getProperty("AppUT") !=null && System.getProperty("AppUT").trim().length() > 0) ? System.getProperty("AppUT").trim() : appUT;
		browserUnderTest = (System.getProperty("BrowserUT") !=null && System.getProperty("BrowserUT").trim().length() > 0) ? System.getProperty("BrowserUT").trim() : testBrowser;
		testEnvName = (System.getProperty("AppTestEnv") !=null && System.getProperty("AppTestEnv").trim().length() > 0) ? System.getProperty("AppTestEnv").trim() : appTestEnv;
		deviceUnderTest = (System.getProperty("DeviceUT") !=null && System.getProperty("DeviceUT").trim().length() > 0) ? System.getProperty("DeviceUT").trim() : devUT;
		trTestPlanId = (System.getProperty("TestPlanId") !=null && System.getProperty("TestPlanId").trim().length() > 0) ? System.getProperty("TestPlanId").trim() : trPlanId;
		initializeReport();
		//initializeTestRailsAccess();
		idbConstants.suiteConfig.setEnvUnderTest(testEnvName); //set test env to get DBServerName
		reportTestAppEnvironmentSummary();
		//verify URL status and skip Test suites
		/*if(!(checkUrlStatus(testContext, gblConstants.suiteConfig.getTestAppUrl(appUnderTest, testEnvName)))) {
			throw new SkipException("Environment is down.. Suite execution is skipped");
		}*/		
	}

	@Override
	public void reportTestAppEnvironmentSummary() {
		if (browserUnderTest.length() > 0) {
			reportWebEnvironmentSummary();
		} else if (deviceUnderTest.length() > 0) {
			reportMobileEnvironmentSummary();
		} else {
			reportWebEnvironmentSummary();
		}
	}

	@BeforeMethod
	public void beforeMethod() {
		
	}

	@BeforeTest(alwaysRun = true)
	@Parameters({"testRailsID","UserName", "Password"})
	public void beforeTest(ITestContext testContext, @Optional("") String testRailsTCId,@Optional("") String lgnUserName, @Optional("") String lgnPwd) {

		//Reinitialize env variables for each test as for parallel test execution, 
		//testng can only share beforesuite instance to only one test
		this.testContext = testContext;
		this.execEnv = getExecutionParameter(testContext, "ExecutionEnv");
		this.appUnderTest = getExecutionParameter(testContext, "AppUT");
		this.testEnvName = getExecutionParameter(testContext, "AppTestEnv");
		this.trTestPlanId = getExecutionParameter(testContext, "TestPlanId");	
		this.browserUnderTest = getExecutionParameter(testContext, "BrowserUT");

		cnActionEngine = new CNWebActionEngine();
		query=new CNDatabaseQueries();
	

		//this.cnActionEngine = new CNWebActionEngine();
		loginUsrName = (lgnUserName.trim().length() > 0 ) ? lgnUserName : idbConstants.suiteConfig.getTestAppLoginUser(appUnderTest, testEnvName);
		loginPswd = (lgnPwd.trim().length() > 0 ) ? lgnPwd : idbConstants.suiteConfig.getTestAppLoginPwd(appUnderTest, testEnvName);	

		if (testRailsTCId != null && testRailsTCId.length() > 0) {
			this.tcReport = (testRailsTCId.length() > 0) ? startTestCaseReport(testContext, testRailsTCId) : null;
			cnActionEngine.setTcReport(this.tcReport);
		} else {
			this.tcReport = extentMngr.addTest(testContext.getCurrentXmlTest().getName(), testRailsTCId);
		}
	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod() {
		
	}

	@AfterTest(alwaysRun = true)
	@Parameters("testRailsID")
	public void afterTest(@Optional("") String testRailsTCId) {
		if (testRailsTCId != null && testRailsTCId.length() > 0 && tcReport != null) {
			endTestCaseReport(this.tcReport, testRailsTCId);
		} else {
			this.tcReport = null;
		}
		if (this.cnActionEngine != null) {
			this.cnActionEngine.closeDriver();
		}
	}

	@Override
	@AfterSuite(alwaysRun = true)
	public void afterSuite() {
		// Close report & driver
		extentMngr.flushReport();
		/*try {
			JiraUtils.addAttachmentToIssue(idbConstants.reportsPath + "\\ExecutionReport.html");
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	*/
		CNDatabases.offenderConnect().closeConnection();
		CNDatabases.attachments().closeConnection();
		CNDatabases.ems().closeConnection();
		CNDatabases.creditBank().closeConnection();
		CNDatabases.iBanker().closeConnection();
		CNDatabases.usso().closeConnection();
		/*
		 * if(this.cnActionEngine != null) { this.cnActionEngine.closeDriver(); }
		 */

		/*// Clean up
		killProcess("chromedriver");
		killProcess("geckodriver");
		killProcess("IEDriverServer");*/
	}

	
	

	

	@Override
	public boolean attachScreenshot(Status result, String strScreenshotName) {
		boolean blnRes = false;

		synchronized (strScreenshotName) {		
			// If screenshot name is valid string, append a screenshot
			if (strScreenshotName.length() > 0) {
				blnRes = extentMngr.attachScreenshot(tcReport, result,
						this.cnActionEngine.captureScreenShot(idbConstants.reportsPath, strScreenshotName));
			}
		}
		return blnRes;
	}

	
	/*
	 * 
	 * @purpose: To ReportMultiple Test Case Reports
	 * 
	 * @ Input params tcreportsList: List of Extent Test Objects,blnresult ,Strings
	 * success and Failure messages
	 * 
	 * @
	 */
	@Override
	public synchronized void reportStatus(ExtentTest[] tcreportsList, boolean blnRes, String successMsg, String failureMsg) {

		Status currTCStatus = Status.INFO;
		String logMessage = "";
		if (blnRes) {
			currTCStatus = Status.PASS;
			logMessage = successMsg;
		} else {
			currTCStatus = Status.FAIL;
			logMessage = failureMsg;
		}

		for (ExtentTest currReport : tcreportsList) {
			if (currReport != null) {
				currReport.log(currTCStatus, logMessage);
			}

			if (currTCStatus == Status.FAIL) {
				//attach Screenshot	
				extentMngr.attachScreenshot(currReport, Status.FAIL,
						this.cnActionEngine.captureScreenShot(idbConstants.reportsPath, failureMsg));
			}

		}
	}

	/*
	 * 
	 * @purpose: To ReportMultiple Test Case Reports
	 * 
	 * @ Input params tcreportsList: List of Extent Test Objects,blnresult ,Strings
	 * success and Failure messages
	 * 
	 * @
	 */
	@Override
	public synchronized void reportStatus(ExtentTest[] tcreportsList, Status status, String statusMsg) {

		//String logMessage = "";

		for (ExtentTest currReport : tcreportsList) {
			if (currReport != null) {
				currReport.log(status, statusMsg);
				if(status == Status.FAIL) {
					//attach Screenshot	
					extentMngr.attachScreenshot(currReport, Status.FAIL,
							this.cnActionEngine.captureScreenShot(idbConstants.reportsPath, statusMsg));
				}

			}

		}
	}



	/*
	 * 
	 * Purpose: To report log to current tcReport
	 * 
	 */

	public void reportLog(Status status, String reportLog) {

		reportStatus(status, reportLog);

	}

	
	@Override
	public void beforeTest(ITestContext testContext, String testRailsTCId) {

	}
}