package idb.web.engine;
//package cn.web.engine;
//
//import org.testng.ITestContext;
//import org.testng.SkipException;
//import org.testng.annotations.AfterSuite;
//import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeSuite;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Optional;
//import org.testng.annotations.Parameters;
//
//import com.aventstack.extentreports.Status;
//
//import cn.common.utils.CNDatabases;
//import idb.config.idbConstants;
//import idb.core.accelerators.AbstractTestEngine;
//
//public class CNAPITestEngine extends AbstractTestEngine {
//	
//	public CNRestServices cnRestServices = null;
//	public String actualReturnMsg=null;
//	public String actualReturnCode=null;
//	
//	@BeforeSuite(alwaysRun=true)
//	@Parameters({"ExecutionEnv","BrowserUT", "AppUT", "AppTestEnv", "ServicesUT", "TestPlanId"})
//	public void beforeSuite(ITestContext testContext, @Optional("local") String execEnvName, @Optional("") String testBrowser, @Optional(idbConstants.projConnectNetworkRewrite) String appUT, 
//			String appTestEnv,  @Optional("") String testService, String trPlanId) {
//		
//		suiteName = testContext.getSuite().getName();
//		execEnv = (System.getProperty("ExecutionEnv") !=null && System.getProperty("ExecutionEnv").trim().length() > 0) ? System.getProperty("ExecutionEnv").trim() : execEnvName;
//		apiUT = ( System.getProperty("ServicesUT") !=null && System.getProperty("ServicesUT").trim().length() > 0) ? System.getProperty("ServicesUT").trim() : testService;
//		appUnderTest = ( System.getProperty("AppUT") !=null && System.getProperty("AppUT").trim().length() > 0) ? System.getProperty("AppUT").trim() : appUT;
//		testEnvName = ( System.getProperty("AppTestEnv") !=null && System.getProperty("AppTestEnv").trim().length() > 0) ? System.getProperty("AppTestEnv").trim() : appTestEnv;
//		trTestPlanId = ( System.getProperty("TestPlanId") !=null && System.getProperty("TestPlanId").trim().length() > 0) ? System.getProperty("TestPlanId").trim() : trPlanId;
//
//		this.initializeReport();
//		this.initializeTestRailsAccess();
//		apiServerUrl = idbConstants.suiteConfig.getTestAPIServerEndpoint(appUnderTest, testEnvName);
//		idbConstants.suiteConfig.setEnvUnderTest(testEnvName); //set test env for getting DBServerName
//		reportTestAppEnvironmentSummary();
//		
//		//verify URL status and skip Test suites
//		if(!testEnvName.equals("stage-cnl")) {
//		if(!(checkUrlStatus(testContext, idbConstants.suiteConfig.getTestAPIServerEndpoint(appUnderTest, testEnvName)+"/Login"))) {
//			throw new SkipException("Environment is down.. Suite execution is skipped");
//		}}
//	}	
//
//	@Override
//	public void reportTestAppEnvironmentSummary() {
//		reportWebApiEnvironmentSummary();
//	}
//	
//	@Override
//	@AfterSuite(alwaysRun = true)
//	public void afterSuite() {		
//		extentMngr.flushReport();
//		CNDatabases.attachments().closeConnection();
//		CNDatabases.creditBank().closeConnection();
//		CNDatabases.ems().closeConnection();
//		CNDatabases.iBanker().closeConnection();
//		CNDatabases.offenderConnect().closeConnection();
//		CNDatabases.usso().closeConnection();
//	}
//
//
//	@Override
//	@BeforeTest
//	@Parameters({"testRailsID"})
//	public void beforeTest(ITestContext testContext, @Optional("") String testRailsTCId) {
//		
//		//Reinitialize env variables for each test as testng can only share beforesuite instance to only one test
//		this.execEnv = getExecutionParameter(testContext, "ExecutionEnv");
//		this.appUnderTest = getExecutionParameter(testContext, "AppUT");
//		this.testEnvName = getExecutionParameter(testContext, "AppTestEnv");		
//		this.apiUT  = getExecutionParameter(testContext, "ServicesUT");
//		this.trTestPlanId = getExecutionParameter(testContext, "TestPlanId");
//		this.apiServerUrl = gblConstants.suiteConfig.getTestAPIServerEndpoint(this.appUnderTest, this.testEnvName);
//		this.cnRestServices = new CNRestServices(apiServerUrl);
//		
//		// REVIEW for BackOffice AdvancePay web service 
//		if (testRailsTCId !=null && testRailsTCId.length() > 0) {
//			this.tcReport = startTestCaseReport(testContext, testRailsTCId);
//		}else{
//			this.tcReport = this.extentMngr.addTest(testContext.getCurrentXmlTest().getName(), testRailsTCId);
//		}			
//	}
//	
//	@Override
//	@BeforeTest(alwaysRun = true)
//	@Parameters({"testRailsID"})
//	public void beforeTest(ITestContext testContext, @Optional("") String testRailsTCId) {
//		
//		//Reinitialize env variables for each test as for parallel test execution, 
//		//testng can only share beforesuite instance to only one test
//		this.appUnderTest = getExecutionParameter(testContext, "AppUT");
//		this.testEnvName = getExecutionParameter(testContext, "AppTestEnv");
//		this.trTestPlanId = getExecutionParameter(testContext, "TestPlanId");
//		idbConstants.suiteConfig.setEnvUnderTest(testEnvName); //set test env to get correct DBServerdata
//		this.apiServerUrl=idbConstants.suiteConfig.getTestAPIServerEndpoint(appUnderTest, testEnvName);
//		
//		//this.cnRestServices = new CNRestServices(this.apiServerUrl);
//		
//		if (testRailsTCId != null && testRailsTCId.length() > 0) {
//			this.tcReport = (testRailsTCId.length() > 0) ? startTestCaseReport(testContext, testRailsTCId) : null;
//		} else {
//			this.tcReport = extentMngr.addTest(testContext.getCurrentXmlTest().getName(), testRailsTCId);
//		}
//	}
//
//	@Override
//	@AfterTest(alwaysRun = true)
//	@Parameters({"testRailsID"})	
//	public void afterTest(String testRailsTCId) {
//    	if(this.tcReport != null) {
//        	endTestCaseReport(this.tcReport, testRailsTCId);
//    	}
//    	extentMngr.flushReport();
//    	this.tcReport = null;		
//	}
//	
//	@Override
//	public boolean attachScreenshot(Status result, String screenshotName) {
//		boolean blnRes = extentMngr.attachScreenshot(this.tcReport, result, "Screenshot");
//		return blnRes;
//	}
//	
////	public void reportServiceLog(RestClient service, String expReturnCode, String expReturnMsg) {
////
////		actualReturnMsg=null;
////		actualReturnCode=null;
////		try {
////			if(service.recentResp != null && expReturnMsg.equals("null")) {
////				JsonReader jsResp = new JsonReader(service.recentResp);
////				actualReturnCode=  jsResp.evaluateJSONPath(".returnCode");
////
////				reportStatus(Status.INFO, "<B>Request JSON : </B><textarea>Request JSON : "+ service.recentReq+"</textarea>");
////				reportStatus(Status.INFO, "<B>Response JSON : </B><textarea>Response JSON : "+ service.recentResp +"</textarea>");
////
////				reportStatus(expReturnCode.equals(actualReturnCode), "Successfully verified return code as " + expReturnCode, "Failed to verify return code as " + expReturnCode );
////			}
////			else if(service.recentResp != null)
////			{
////				JsonReader jsResp = new JsonReader(service.recentResp);
////				actualReturnMsg = jsResp.evaluateJSONPath(".returnMessage").trim();
////				actualReturnCode=  jsResp.evaluateJSONPath(".returnCode");
////
////				reportStatus(Status.INFO, "<B>Request JSON : </B><textarea>Request JSON : "+ service.recentReq+"</textarea>");
////				reportStatus(Status.INFO, "<B>Response JSON : </B><textarea>Response JSON : "+ service.recentResp +"</textarea>");
////
////				reportStatus(expReturnCode.equals(actualReturnCode), "Successfully verified return code as " + expReturnCode, "Failed to verify return code as " + expReturnCode );
////				reportStatus(expReturnMsg.equalsIgnoreCase(actualReturnMsg), "Successfully verified return message as " + expReturnMsg, "Failed to verify return Message as " + expReturnMsg);
////			}
////			else {
////				reportStatus(Status.INFO, "<B>Request JSON : </B><textarea>Request JSON : "+ service.recentReq+"</textarea>");
////				reportStatus(Status.FAIL, "Failed to to get a valid response for the provided request");
////			}
////		}catch(Exception e) {
////			reportStatus(Status.FAIL, "Exception " + e.getMessage() + " to report recent service request / response details");
////		}
////	}
//
//   
//    	
//    
//	public boolean loginUsingDefaultUser() {
//		return false;
//	}
//
//}
