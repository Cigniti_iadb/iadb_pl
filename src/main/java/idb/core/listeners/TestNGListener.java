package idb.core.listeners;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;

import org.testng.*;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.model.Log;

import idb.common.utils.CNConstants;
import idb.core.logs.LogManager;
import idb.core.testmanagement.JiraUtils;
import idb.web.engine.CNTestEngine;

//public class ExecutionListener extends ActionsLibrary implements ITestListener, ISuiteListener, IInvokedMethodListener,IExecutionListener {
public class TestNGListener extends JiraUtils implements ITestListener, ISuiteListener, IInvokedMethodListener,IExecutionListener {
	private void LogInfo(String logMsg){
		LogManager.logInfo(TestNGListener.class.getName(), logMsg);
	}

	// This belongs to ISuiteListener and will execute before the Suite start
	@Override
	public void onStart(ISuite result) {
		LogManager.initialize();
		String logMsg = "About to begin executing Suite :- " + result.getName();
		Reporter.log(logMsg, true);
		LogInfo(logMsg);
	}

	// This belongs to ISuiteListener and will execute, once the Suite is finished
	@Override
	public void onFinish(ISuite result) {
		String logMsg = "About to end executing Suite :- " + result.getName();
		Reporter.log(logMsg, true);
		LogInfo(logMsg);
	}

	// This belongs to ITestListener and will execute before starting of Test set/batch
	public void onStart(ITestContext result) {
		String logMsg = "About to begin executing Test :- " + result.getName();		
		Reporter.log(logMsg, true);
		LogInfo(logMsg);
	}

	// This belongs to ITestListener and will execute, once the Test set/batch is finished
	public void onFinish(ITestContext result) {
		String logMsg = "Completed executing test :- " + result.getName();
		Reporter.log(logMsg, true);
		LogInfo(logMsg);
	}

	// This belongs to ITestListener and will execute before the main test start (@Test)
	public void onTestStart(ITestResult result) {
		String logMsg = "About to begin Test :- " + result.getName();
		Reporter.log(logMsg, true);
		LogInfo(logMsg);
	}

	// This belongs to ITestListener and will execute only when the test is pass
	public void onTestSuccess(ITestResult result) {	
		String logMsg = "   ##########   TEST CASE :   " + result.getName() + "   PASSED   ##########";
		Reporter.log(logMsg);
		LogInfo(logMsg);
		//CommonVariables.extentTest.get().log(LogStatus.PASS,"Status","TEST CASE PASSED.");


		try {
			JiraUtils.LoadJiraProperties();
			/*String cycleId = createCycle(cycleName);
			System.out.println(cycleId);
			ArrayList<String> issueKeys=new ArrayList<String>();
			issueKeys.add(testCaseName);
		//	issueKeys.add("ITF-2");
			addTestsToCycle(cycleId,issueKeys);*/
			String issueID = JiraUtils.geIssueId(projectId, versionId, cycleId, testCaseName);
			String executionId=JiraUtils.createExecutionId(projectId, issueID, cycleId);	
			JiraUtils.updateExecutionResult(executionId, issueID, "PASS", cycleId, "parimal");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// This belongs to ITestListener and will execute only on the event of fail test
	public void onTestFailure(ITestResult result) {
		String logMsg ="About to begin executing Test :- " + result.getName();
		Reporter.log(logMsg);
		LogInfo(logMsg);
		/*JiraUtils.LoadJiraProperties();
		String issueID = JiraUtils.getIssueID("RES", testCaseName);
		String executionID = JiraUtils.getExecutionID(issueID);*/
		try {
			JiraUtils.LoadJiraProperties();
		/*	String cycleId = createCycle(cycleName);
			System.out.println(cycleId);
			ArrayList<String> issueKeys=new ArrayList<String>();
			issueKeys.add(testCaseName);
		//	issueKeys.add("ITF-2");
			addTestsToCycle(cycleId,issueKeys);*/
			
			String issueID = JiraUtils.geIssueId(projectId, versionId, cycleId, testCaseName);
			String executionId=JiraUtils.createExecutionId(projectId, issueID, cycleId);	
			JiraUtils.updateExecutionResult(executionId, issueID, "FAIL", cycleId, "parimal");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// This belongs to ITestListener and will execute only if any of the main test(@Test) get skipped
	public void onTestSkipped(ITestResult result) {
		String logMsg = "About to begin executing Test :- " + result.getName();
		Reporter.log(logMsg);
		LogInfo(logMsg);
		//CommonVariables.extentTest.get().log(Status.SKIP,"Status");
		//CommonVariables.extentTest.get().log(Status.SKIP,"Status","TEST CASE SKIPPED.");
	}

	// This is just a piece of shit, ignore this
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		String logMsg = "About to begin executing Test :- " + result.getName();
		Reporter.log(logMsg);
		LogInfo(logMsg);
		//CommonVariables.extentTest.get().log(LogStatus.WARNING,"Status","Test Failed But Within Success Percentage : "+result.SUCCESS_PERCENTAGE_FAILURE);
	}

	// This belongs to IInvokedMethodListener and will execute before every method including @Before @After @Test
	public void beforeInvocation(IInvokedMethod result, ITestResult arg1) {
		String logMsg = "About to begin executing method :- " + returnMethodName(result.getTestMethod());
		Reporter.log(logMsg, true);
		LogInfo(logMsg);
		//CommonVariables.extentTest.get().log(LogStatus.INFO, textMsg + result.getTestMethod().getMethodName());
	}

	// This belongs to IInvokedMethodListener and will execute after every method including @Before @After @Test
	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
		String logMsg = "Completed executing method  :- " + returnMethodName(method.getTestMethod());
		Reporter.log(logMsg, true);
		LogInfo(logMsg);

		//Capture test case execution log
		ExtentTest tcLog = (ExtentTest) testResult.getTestContext().getAttribute("tcReport");

		//Capture run time exception in test method execution to extent report
		if (method.isTestMethod() && testResult.getThrowable() != null) {
			tcLog.log(Status.FATAL, "Exception to " + testResult.getThrowable().getClass() + " to execute test");
			testResult.getTestContext().setAttribute("tcReport", tcLog);
		}

		//Set TestNG - Test Result for failed test case
		if(method.isTestMethod() && tcLog != null) {

			String detailedLog="";
			for (Log stepLog : tcLog.getModel().getLogContext().getAll()) {
				detailedLog = detailedLog + stepLog.getDetails();
			}			

			//FAIL TestNG Test Case for Failed Test Cases
			Reporter.log(detailedLog);
			if(tcLog.getStatus() == Status.FAIL) {
				testResult.setStatus(testResult.FAILURE);
				AssertionError failureReason = new AssertionError(detailedLog);
				testResult.setThrowable(failureReason);
			}
		}
	}

	// This will return method names to the calling function
	private String returnMethodName(ITestNGMethod method) {
		return method.getRealClass().getSimpleName() + "." + method.getMethodName();
	}

	@Override
	public void onExecutionStart() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onExecutionFinish() {
		// TODO Auto-generated method stub
	}

}