package idb.core.database;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

import org.json.JSONObject;

import com.google.common.base.CaseFormat;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import idb.config.idbConstants;
import idb.core.logs.LogManager;

public class DatabaseVerification {
	
	String strDBDriverName = "" ;
	String strDBConnectString = "" ;
	String strDBServerName = "";
	String strDBName = "";
	String strDBUser = "";
	String strPassword = "";
	String strDBType="";
	Connection connection=null;
	
	public DatabaseVerification(String strDBType, String strDBServer,String strDBName, String strUserName, String strPassword){
		this.strDBType=strDBType;				
		switch (strDBType.toLowerCase()) {
			case "oracle" 		: this.strDBDriverName = idbConstants.dbDriver_Oracle; break;
			case "sqlserver" 	: this.strDBDriverName = idbConstants.dbDriver_SQLServer; break;
			case "mysql"		: this.strDBDriverName = idbConstants.dbDriver_MySQL; break;
		}
		this.strDBServerName = strDBServer.trim();
		this.strDBName = strDBName.trim();
		this.strDBUser = strUserName.trim();
		this.strPassword = strPassword.trim();			
	}		

	public DatabaseVerification(String strDBType, String strDBServer, String strUserName, String strPassword){
		this.strDBType=strDBType;				
		switch (strDBType.toLowerCase()) {
			case "oracle" 		: this.strDBDriverName = idbConstants.dbDriver_Oracle; break;
			case "sqlserver" 	: this.strDBDriverName = idbConstants.dbDriver_SQLServer; break;
			case "mysql"		: this.strDBDriverName = idbConstants.dbDriver_MySQL; break;
		}
		this.strDBServerName = strDBServer.trim();
		this.strDBName = strDBName.trim();
		this.strDBUser = strUserName.trim();
		this.strPassword = strPassword.trim();			
	}	
	
	public Connection getConnection(String strDBType) {
		try {
			Class.forName(strDBDriverName);
			if(strDBType.equalsIgnoreCase("SQLSERVER")) {
				String connectionUrl = strDBServerName + "; databaseName=" + strDBName + ";" + "user=" + this.strDBUser + ";"
						+ "password= "+ this.strPassword;
				connection = DriverManager.getConnection(connectionUrl);
			}else if(strDBType.equalsIgnoreCase("Oracle") || strDBType.equalsIgnoreCase("MySQL")) {
				connection = DriverManager.getConnection(strDBServerName,strDBUser,strPassword);
			}else {
				System.out.println("Check for correct Database Type....");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;		
	}
	
	
	public boolean closeConnection() {
		
		try {
			if(connection != null) {
				connection.close();
				connection = null;
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public synchronized ResultSet getQueryResponse(String queryString) throws Exception {
		//Statement statement = getConnection(strDBType).createStatement();
		int maxAttempts = 2;
		int retryCntr=1;
		ResultSet rs = null;
		try {
			if(connection == null ) {			
				connection = getConnection(this.strDBType);
			}
			Statement statement = connection.createStatement();
			rs = statement.executeQuery(queryString);
		}catch(CommunicationsException ce) {
			closeConnection();
			retryCntr++;
			if(retryCntr<=maxAttempts) {
				return getQueryResponse(queryString);
			}
		}
		return rs;
	}

	/*public ResultSet getQueryResponse(String strServerName, String strDB, String queryString) throws Exception {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		//updateProperty();
		String connectionUrl = strServerName + "; databaseName=" + strDB + ";" + "user=" + this.strDBUser + ";"
				+ "password= "+ this.strPassword;
		Connection conn = DriverManager.getConnection(connectionUrl);
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery(queryString);
		return rs;
	}*/
	
	/**
	 * @author Satya, Gajula
	 * @param rs
	 * @return
	 */
	public List<Map<String, String>> getResultAsList(ResultSet rs) {
		List<Map<String, String>> resultSet = new ArrayList<Map<String, String>>();
		try {
			ResultSetMetaData rsmd = rs.getMetaData();
			while (rs.next()) {
				Map<String, String> rowData= new HashMap<String, String>();
				for (Integer i = 1; i <= rsmd.getColumnCount(); i++) {
					rowData.put(rsmd.getColumnLabel(i), rs.getString(i));
				}
				resultSet.add(rowData);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultSet;
	}

	/**
	 * Purpose: Execute query and return response in JSON Format
	 * @param sqlQuery
	 * @return
	 */
	public List<JSONObject> getQueryResponseAsJSON(String sqlQuery){
		List<JSONObject> resList = null;
		try {
			 if(sqlQuery.startsWith("SELECT") || sqlQuery.startsWith("select")){
		            resList = convertResultSetToListOfJSON(getQueryResponse(sqlQuery));
		        }else if(sqlQuery.startsWith("UPDATE") || sqlQuery.startsWith("update")) {
		        	resList = new ArrayList<JSONObject>();
		        	JSONObject jsonObj = new JSONObject();
		        	String updCount = String.valueOf(executeUpdateQuery(sqlQuery));
		        	jsonObj.put("UpdatedRows", updCount);
		        	resList.add(jsonObj);
		        	
		        }else {
		            resList= new ArrayList<>();
		        }
	    } catch (Exception e) {
	        LogManager.logException(e, DatabaseVerification.class.getName(), "Exception " + e.getCause() + " to get query response");
	    	e.printStackTrace();
	    }
	    return resList;
	}
	
	/*
	 * Purpose: Private method to convert the ResultSet into JSON
	 * Author-date: Maheshwari, Surbhi 10/11/18
	 * @param resultSet
	 * Reviewer date:
	 */
	private List<JSONObject> convertResultSetToListOfJSON(ResultSet resultSet) {
	    List<JSONObject> resList = new ArrayList<>();
	    try {
	        ResultSetMetaData rsMeta = resultSet.getMetaData();
	        int columnCount = rsMeta.getColumnCount();
	        while(resultSet.next()){
	            JSONObject obj = new JSONObject();
	            for(int i=1; i<=columnCount; i++){
	                String key = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, rsMeta.getColumnName(i));
	                String value = resultSet.getString(i);
	                obj.put(key, value);
	            }
	            resList.add(obj);
	        }
	        resultSet.close();
	    } catch (SQLException e){
	        e.printStackTrace();
	    }        
	    return resList;
	}
	
	/**
	 * @author Satya, Gajulas
	 * @param DBName
	 * @param insertStatement
	 * @throws Exception
	 */
	public void insertInToDataBase(String insertStatement)
			throws Exception {
		Statement statement = getConnection(strDBType).createStatement();
		statement.executeUpdate(insertStatement);
	}
	
	/**
	 * @author Satya, Gajula
	 * @return Update Query returns the no of row affected
	 * @throws Exception
	 */
	public void insertInToDataBase(String strServerName, String strDB, String insertStatement)
			throws Exception {
		Statement statement = getConnection(strDBType).createStatement();
		statement.executeUpdate(insertStatement);
	}	

	/**
	 * @author Satya, Gajula
	 * @param queryString
	 * @return
	 * @throws Exception
	 */
	public int executeUpdateQuery(String strUpdateStatement) throws Exception {
		int rowAffected;
		Statement statement = getConnection(strDBType).createStatement();
		rowAffected= statement.executeUpdate(strUpdateStatement);
		return rowAffected;
	}
	
	/**
	 * @author Satya, Gajula
	 * @return Update Query returns the no of row affected
	 * @throws Exception
	 */
	public int executeUpdateQuery(String strServerName, String strDB, String strUpdateStatement)
			throws Exception {
		int rowAffected;
		Statement statement = getConnection(strDBType).createStatement();
		rowAffected= statement.executeUpdate(strUpdateStatement);
		return rowAffected;
	}
	
	/**
	 * @author Jagadeesh, Boddeda
	 * @param Result set object and Column name of BLOB
	 * @return Returns the blob data as String
	 * @throws Exception
	 */
	public String getBlobData(ResultSet OrcResultSet, String columnName) {
		String blobData = null;
		try {
			 ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024*5]; //memory allocated
				while (OrcResultSet.next()) {
				      InputStream inpStream = OrcResultSet.getBinaryStream(columnName);
				      while (inpStream.read(buffer) > 0) {
				    	  outStream.write(buffer,0,buffer.length);
				      }
				      blobData = new String(outStream.toByteArray(),Charset.defaultCharset()).replace("<br />", "").replace("<br/>", "");
				} 
				 outStream.close();
				} catch (Exception e) {
					e.printStackTrace();
					return blobData;
				}
				return blobData;
	}
	
	/**
	 * @author Jagadeesh, Boddeda
	 * @param Result set object, Column name of Date field and required Date format
	 * @return Returns the String date as per the input format
	 * @throws Exception
	 */
	public String readDateColumn(ResultSet OrcResultSet, String ColumnName, String DateFormat) {
		String newDate = null;
		try {
	    	OrcResultSet.next();
			Date date = OrcResultSet.getDate(ColumnName);
			DateFormat dateFormat = new SimpleDateFormat(DateFormat);
			newDate = dateFormat.format(date);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return newDate;
	}
	
}