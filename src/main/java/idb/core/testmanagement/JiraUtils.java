package idb.core.testmanagement;


import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.ParseException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import idb.common.utils.CNConstants;
import idb.core.fileutils.PropertiesFileManager;
public class JiraUtils  {
	static Response response = null;
	public static String jiraBaseUrl,ZAPI_UPDATE_EXECUTION;
	public static String projectId, versionId,authentication,cycleName,cycleId;	
	public static Map<String,String> headerMap = new HashMap<String,String>();
	public static Map<String,String> headerMap1 = new HashMap<String,String>();

	public static Map<String, String> map1 = new HashMap<>();
	public static String testCaseName=null;

	public static void main(String[] args) throws URISyntaxException, JSONException, InterruptedException, IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		LoadJiraProperties();
		/*String cycleids = getCycleId(projectId);
			System.out.println(cycleids);
		//	String issueId = geIssueId("16500", "-1", "1431", "ITF-4");
	//	String executionId=createExecutionId("16500", issueId, "1431");

		updateExecutionResult(executionId, issueId, "PASS", "1431", "parimal");
		System.out.println("done........................................");*/
		String cycleId = createCycle("cycle3");
		System.out.println(cycleId);
		ArrayList<String> issueKeys=new ArrayList<String>();
		issueKeys.add("ITF-4");
		/*issueKeys.add("");
		issueKeys.add("");
		issueKeys.add("");*/
		//	issueKeys.add("ITF-2");
		addTestsToCycle(cycleId,issueKeys);
		String issueId = geIssueId("16500", "-1", cycleId, "ITF-4");	
		System.out.println("issue id ===================================>" + issueId);
		String executionId = createExecutionId("16500", issueId, cycleId);
		System.out.println("execution id ===================================>" + executionId);
		updateExecutionResult(executionId, issueId, "PASS", cycleId, "parimal Diwan");
		System.out.println("done.........");
	}
	public static void LoadJiraProperties(){
		RestAssured.useRelaxedHTTPSValidation();  
		jiraBaseUrl=CNConstants.jiraUrl;
		projectId=CNConstants.projectId;
		versionId=CNConstants.versionId;
		cycleName=CNConstants.cycleName;
		cycleId=CNConstants.cycleId;
		authentication = CNConstants.authentication;
		ZAPI_UPDATE_EXECUTION = jiraBaseUrl+"/rest/zapi/latest/execution/";
		headerMap.put("Content-Type", "application/json");
		headerMap.put("Authorization", "Basic SllPVElSTUFZQU46Q2lnbml0aUAxMjM=");

	}

	public static String createExecutionId(String projectId,String issueId, String cycleId) throws URISyntaxException, JSONException, IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		RestAssured.useRelaxedHTTPSValidation(); 
		String uri =  jiraBaseUrl + "/rest/zapi/latest/execution/";
		JSONObject executeTestsObj = new JSONObject();
		executeTestsObj.put("projectId", projectId);
		executeTestsObj.put("status", "1");
		executeTestsObj.put("issueId", issueId);
		executeTestsObj.put("cycleId",cycleId );
		StringEntity executeTestsJSON = null;
		try {
			executeTestsJSON = new StringEntity(executeTestsObj.toString());
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		HttpResponse response1 = POST(uri, executeTestsJSON);
		RestAssured.useRelaxedHTTPSValidation(); 
		System.out.println(response1.getStatusLine());
		String json = EntityUtils.toString(response1.getEntity());
		System.out.println(json);
		int json1 = json.indexOf(":");
		json = json.substring(2, json1-1).trim();
		int statusCode = response1.getStatusLine().getStatusCode();
		return json;

	}
	public static String updateExecutionResult(String executionID,String issueId,String issueStatus, String cycleId, String createdBy) throws URISyntaxException, JSONException, IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {  
		final String updateExecutionUri =  ZAPI_UPDATE_EXECUTION + executionID+"/execute";
		String testResult;
		switch(issueStatus.toLowerCase()){
		case "pass":
			testResult="1";
			break;
		case "fail":
			testResult="2";
			break;
		default:
			testResult="-1";
			break;
		}
		JSONObject executeTestsObj = new JSONObject();
		executeTestsObj.put("status", testResult);
		executeTestsObj.put("cycleId", cycleId);
		executeTestsObj.put("projectId", projectId);
		executeTestsObj.put("versionId", versionId);
		executeTestsObj.put("comment", "COMMENTS");
		executeTestsObj.put("issueId", issueId);
		executeTestsObj.putOpt("createdBy",createdBy );
		StringEntity executeTestsJSON = null;
		try {
			executeTestsJSON = new StringEntity(executeTestsObj.toString());
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		HttpResponse response = PUT(updateExecutionUri, executeTestsJSON);
		RestAssured.useRelaxedHTTPSValidation();
		int statusCode = response.getStatusLine().getStatusCode();
		String statusCode1 = String.valueOf(statusCode);
		return statusCode1;
	}

	public static String createCycle(String name) throws URISyntaxException, JSONException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		String uri =jiraBaseUrl+"/rest/zapi/latest/cycle?";
		JSONObject createCycleObj = new JSONObject();
		createCycleObj.put("name", name);
		createCycleObj.put("description", "automation");
		createCycleObj.put("projectId", projectId);
		createCycleObj.put("versionId", versionId);
		StringEntity cycleJSON = null;
		try {
			cycleJSON = new StringEntity(createCycleObj.toString());
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		HttpResponse response = POST(uri, cycleJSON);
		int statusCode = response.getStatusLine().getStatusCode();
		System.out.println(statusCode);
		String cycleId = "-1";
		if (statusCode >= 200 && statusCode < 300) {
			HttpEntity entity = response.getEntity();
			try {
				JSONObject cycleObj = new JSONObject(EntityUtils.toString(entity));
				cycleId = cycleObj.getString("id");
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			try {
				throw new ClientProtocolException("Unexpected response status: " + statusCode);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			}
		}
		return cycleId;
	}

	public static void addTestsToCycle(String cycleId,ArrayList<String> issueKeys) throws URISyntaxException, JSONException, InterruptedException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		String uri = jiraBaseUrl+"/rest/zapi/latest/execution/addTestsToCycle/";
		//** Create JSON object by providing input values *//*
		JSONObject addTestsObj = new JSONObject();
		addTestsObj.put("cycleId", cycleId);
		addTestsObj.put("projectId", projectId);
		addTestsObj.put("versionId", versionId);
		addTestsObj.put("method", "1");
		addTestsObj.put("issues", issueKeys);
		StringEntity addTestsJSON = null;
		try {
			addTestsJSON = new StringEntity(addTestsObj.toString());
			addTestsJSON.setContentType("application/json");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		HttpResponse response = POST(uri, addTestsJSON);
		Thread.sleep(10000);
		int statusCode = response.getStatusLine().getStatusCode();
		System.out.println(statusCode);
		String temp = null;
		if (statusCode >= 200 && statusCode < 300) {
			HttpEntity entity = response.getEntity();
			try {
				temp = EntityUtils.toString(entity);
				System.out.println(temp);
				JSONObject cycleObj = new JSONObject(temp);
				System.out.println(cycleObj.toString());
				System.out.println("Tests added Successfully");
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				throw new ClientProtocolException("Unexpected response status: " + statusCode);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			}
		}
	}
	public static HttpResponse GET(String url) throws URISyntaxException {
		URI uri = new URI(url);
		System.out.println("FINAL API : " +uri.toString());
		HttpResponse response = null;
		HttpClient restClient = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet(uri);
		getRequest.addHeader("authorization", "Basic "+authentication);
		getRequest.addHeader("Content-Type", "application/json");
		try {
			response = restClient.execute(getRequest);
		} catch (ClientProtocolException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return response;
	}

	public static HttpResponse PUT(String uriStr, StringEntity payload) throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		URI uri = new URI(uriStr);
		System.out.println(uri.toString());
		HttpResponse response = null;
		HttpClient restClient = HttpClients
				.custom()
				.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
				.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
				.build();
		HttpPut putRequest = new HttpPut(uri);
		putRequest.addHeader("authorization", "Basic "+authentication);
		putRequest.addHeader("Content-Type", "application/json");
		putRequest.setEntity(payload);
		try {
			response = restClient.execute(putRequest);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	public static HttpResponse POST(String uriStr, StringEntity entityJSON) throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		HttpClient restClient = HttpClients
				.custom()
				.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
				.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
				.build();
		URI uri = new URI(uriStr);
		System.out.println(uri.toString());
		HttpResponse response = null;
		HttpPost postRequest = new HttpPost(uri);
		postRequest.setHeader("Content-Type", "application/json");
		postRequest.setHeader( "Authorization","Basic "+authentication);
		postRequest.setEntity(entityJSON);
		try {
			response = restClient.execute(postRequest);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}

	public static String getAllTestCasesName(String projectName,  Map<String,String> headerMap) {
		String url=jiraBaseUrl+"/rest/api/2/search?jql=issuetype=TEST&project="+projectName+"";
		Response response = null;
		String testcaseName = null;
		try{
			RestAssured.useRelaxedHTTPSValidation();  
			response = RestAssured.given().log().all().headers(headerMap).get(url);
			response.prettyPrint();
			JsonPath jsonPathEvaluator = response.jsonPath();
			testcaseName = jsonPathEvaluator.getString("issues.key");
			System.out.println(testcaseName);
		} catch (Exception e) {
			System.out.println("Error ======> " + e.getMessage());
		}
		return testcaseName;
	}

	public static String getExecutionID(String issueID) {
		String url=jiraBaseUrl+"/rest/zapi/latest/execution?issueId="+issueID+"";
		Response response = null;
		String executionID=null;
		try{
			RestAssured.useRelaxedHTTPSValidation();  
			response = RestAssured.given().log().all().headers(headerMap).get(url);
			response.prettyPrint();
			JsonPath jsonPathEvaluator = response.jsonPath();
			executionID = jsonPathEvaluator.getString("executions[0].id");
			System.out.println(executionID);
		} 
		catch (Exception e) {
			System.out.println("Error ======> " + e.getMessage());
		}
		return executionID;
	}

	public static String getCycleId(String projectId) {
		String url=jiraBaseUrl+"/rest/zapi/latest/cycle?projectId="+projectId+"";
		Response response = null;
		String cycleName=null;
		try{
			RestAssured.useRelaxedHTTPSValidation();  
			response = RestAssured.given().log().all().headers(headerMap).get(url);
			response.prettyPrint();
			JsonPath jsonPathEvaluator = response.jsonPath();
			cycleName = jsonPathEvaluator.getString("-1[0]");
			System.out.println(cycleName);
		} 
		catch (Exception e) {
			System.out.println("Error ======> " + e.getMessage());
		}
		return cycleName;
	}

	public static String getAllTestCaseFromCycle(String projectId,String versionId , String cycleId) {
		String url=jiraBaseUrl+"/rest/zapi/latest/execution?projectId="+projectId+"&versionId="+versionId+"&cycleId="+cycleId+"";
		String testCaseNames=null;
		try{
			response = RestAssured.given().log().all().headers(headerMap).get(url);
			response.prettyPrint();
			JsonPath jsonPathEvaluator = response.jsonPath();
			testCaseNames = jsonPathEvaluator.getString("executions.issueKey"); // update it
			System.out.println(testCaseNames);
		} 
		catch (Exception e) {
			System.out.println("Error ======> " + e.getMessage());
		}
		return testCaseNames;
	}

	public static String geIssueId(String projectId,String versionId , String cycleId, String testCaseName) {
		String url=jiraBaseUrl+"/rest/zapi/latest/execution?projectId="+projectId+"&versionId="+versionId+"&cycleId="+cycleId+"";
		String testCaseName1=null;
		RestAssured.useRelaxedHTTPSValidation();  
		Map<String,String> testCaseNames = new HashMap<String,String>();
		response = RestAssured.given().log().all().headers(headerMap).get(url);
		response.prettyPrint();
		JsonPath jsonPathEvaluator = response.jsonPath();
		testCaseName1 = jsonPathEvaluator.getString("executions.issueKey"); // update it
		String[] tcName  = testCaseName1.split(",");
		int lenght = tcName.length;
		for  (int i=0; i < lenght ;i++) {
			testCaseNames.put(jsonPathEvaluator.getString("executions["+i+"].issueKey"), jsonPathEvaluator.getString("executions["+i+"].issueId"));
			System.out.println(testCaseNames);
		}
		String getIssueID = testCaseNames.get(testCaseName);
		return getIssueID ;

	}
	public static String getEntityID(String testCaseName) {
		String url=jiraBaseUrl+"/rest/zapi/latest/audit";
		String testCaseNames=null;
		Response response = null;
		JsonPath jsonPathEvaluator =null;
		RestAssured.useRelaxedHTTPSValidation();  

		Map<String,String> testCaseIssueID = new HashMap<String,String>();
		response = RestAssured.given().log().all().headers(headerMap).get(url);
		response.prettyPrint();
		jsonPathEvaluator = response.jsonPath();
		String issueKey = jsonPathEvaluator.getString("auditLogs.issueKey");
		System.out.println(issueKey);
		String[] issueKey1  = issueKey.split(",");
		int lenght = issueKey1.length;
		for  (int i=0; i < lenght ;i++) {
			testCaseIssueID.put(jsonPathEvaluator.getString("auditLogs.issueKey["+i+"]"), jsonPathEvaluator.getString("auditLogs.entityId["+i+"]"));
		}
		System.out.println(testCaseIssueID);

		String getIssueID = testCaseIssueID.get(testCaseName);
		return getIssueID ;
	}

	public static void attachFile() {
		String url=jiraBaseUrl+"/rest/zapi/latest/attachment?entityId=43&entityType=Execution";
		/*String testCaseNames=null;
		JsonPath jsonPathEvaluator =null;
		Map<String,String> testCaseIssueID = new HashMap<String,String>();*/
		RestAssured.useRelaxedHTTPSValidation();  
		Response response = null;
		headerMap1.put("Content-Type", "multipart/form-data");
		headerMap1.put("Authorization", "Basic cGFyaW1hbC5kaXdhbjpDaWduaXRpQDEyMw==");
		String payload = "{\"File\": \"C:\\ss-writs-scripts_04092019\\WRITS\\selenium\\notes.txt\"}";
		response = RestAssured.given().log().all().headers(headerMap1).post(url);
		response.prettyPrint();
		System.out.println("response code========================> "+response.getStatusCode());
	}

	public static boolean addAttachmentToIssue(String issueKey, String path){
		// String auth = new String(org.apache.commons.codec.binary.Base64.encodeBase64((user+":"+pass).getBytes()));
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://localhost:8080/"+"issue/"+issueKey+"/attachments");
		httppost.setHeader("X-Atlassian-Token", "nocheck");
		// httppost.setHeader("Authorization", "Basic "+auth);	
		httppost.setHeader("Authorization", "Basic cGFyaW1hbC5kaXdhbjpDaWduaXRpQDEyMw==");
		MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
		File fileToUpload = new File(path);
		FileBody fileBody = new FileBody(fileToUpload, "application/octet-stream");
		entity.addPart("file", fileBody);
		httppost.setEntity(entity);
		HttpResponse response = null;
		try {
			response = httpclient.execute(httppost);
		} catch (ClientProtocolException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		HttpEntity result = response.getEntity();
		if(response.getStatusLine().getStatusCode() == 200)
			return true;
		else
			return false;
	}
}