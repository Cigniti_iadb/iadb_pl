package idb.core.fileutils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustAllStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

import idb.common.utils.CNConstants;

public class JiraCreateCycle {
	static Response response = null;
	public static String jiraBaseUrl,ZAPI_UPDATE_EXECUTION;
	public static String projectId, versionId,authentication,cycleName;	
	public static Map<String,String> headerMap = new HashMap<String,String>();
	public static Map<String,String> headerMap1 = new HashMap<String,String>();
	public static Map<String, String> map1 = new HashMap<>();

	public static void main(String[] args) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, JSONException, InterruptedException, FileNotFoundException, IOException {
		LoadJiraProperties();
		String cycleId = createCycle("cycle2");
		System.out.println(cycleId);
		ArrayList<String> issueKeys=new ArrayList<String>();
		ExcelReader reader = new ExcelReader("C:\\Users\\PARIMALD\\git\\iadb-testing-framework_test\\TestData\\UI\\testcycle.xlsx", "testcase");
		System.out.println(reader.getNoOfRows());
		for (int i=1; i <=reader.getNoOfRows();i++) {
			issueKeys.add(ExcelReader.getData(i, 0));
		}
		System.out.println(issueKeys);
		//issueKeys.add("ITF-4");

		addTestsToCycle(cycleId, issueKeys);
	}

	public static String createCycle(String name) throws URISyntaxException, JSONException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		String uri =jiraBaseUrl+"/rest/zapi/latest/cycle?";
		JSONObject createCycleObj = new JSONObject();
		createCycleObj.put("name", name);
		createCycleObj.put("description", "automation");
		createCycleObj.put("projectId", projectId);
		createCycleObj.put("versionId", versionId);
		StringEntity cycleJSON = null;
		try {
			cycleJSON = new StringEntity(createCycleObj.toString());
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		HttpResponse response = POST(uri, cycleJSON);
		int statusCode = response.getStatusLine().getStatusCode();
		System.out.println(statusCode);
		String cycleId = "-1";
		if (statusCode >= 200 && statusCode < 300) {
			HttpEntity entity = response.getEntity();
			try {
				JSONObject cycleObj = new JSONObject(EntityUtils.toString(entity));
				cycleId = cycleObj.getString("id");
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} else {
			try {
				throw new ClientProtocolException("Unexpected response status: " + statusCode);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			}
		}
		return cycleId;
	}

	public static void addTestsToCycle(String cycleId,ArrayList<String> issueKeys) throws URISyntaxException, JSONException, InterruptedException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		String uri = jiraBaseUrl+"/rest/zapi/latest/execution/addTestsToCycle/";
		//** Create JSON object by providing input values *//*
		JSONObject addTestsObj = new JSONObject();
		addTestsObj.put("cycleId", cycleId);
		addTestsObj.put("projectId", projectId);
		addTestsObj.put("versionId", versionId);
		addTestsObj.put("method", "1");
		addTestsObj.put("issues", issueKeys);
		StringEntity addTestsJSON = null;
		try {
			addTestsJSON = new StringEntity(addTestsObj.toString());
			addTestsJSON.setContentType("application/json");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		HttpResponse response = POST(uri, addTestsJSON);
		Thread.sleep(10000);
		int statusCode = response.getStatusLine().getStatusCode();
		System.out.println(statusCode);
		String temp = null;
		if (statusCode >= 200 && statusCode < 300) {
			HttpEntity entity = response.getEntity();
			try {
				temp = EntityUtils.toString(entity);
				System.out.println(temp);
				JSONObject cycleObj = new JSONObject(temp);
				System.out.println(cycleObj.toString());
				System.out.println("Tests added Successfully");
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				throw new ClientProtocolException("Unexpected response status: " + statusCode);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			}
		}
	}
	public static void LoadJiraProperties(){
		RestAssured.useRelaxedHTTPSValidation();  
		jiraBaseUrl=CNConstants.jiraUrl;
		projectId=CNConstants.projectId;
		versionId=CNConstants.versionId;
		cycleName=CNConstants.cycleName;
		authentication = CNConstants.authentication;
		ZAPI_UPDATE_EXECUTION = jiraBaseUrl+"/rest/zapi/latest/execution/";
		headerMap.put("Content-Type", "application/json");
		headerMap.put("Authorization", "Basic SllPVElSTUFZQU46Q2lnbml0aUAxMjM=");

	}
	public static HttpResponse POST(String uriStr, StringEntity entityJSON) throws URISyntaxException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		HttpClient restClient = HttpClients
				.custom()
				.setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, TrustAllStrategy.INSTANCE).build())
				.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
				.build();
		URI uri = new URI(uriStr);
		System.out.println(uri.toString());
		HttpResponse response = null;
		HttpPost postRequest = new HttpPost(uri);
		postRequest.setHeader("Content-Type", "application/json");
		postRequest.setHeader( "Authorization","Basic "+authentication);
		postRequest.setEntity(entityJSON);
		try {
			response = restClient.execute(postRequest);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}
}
