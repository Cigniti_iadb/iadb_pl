package idb.core.accelerators;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import idb.config.idbConstants;
import idb.core.listeners.WebDriverListener;
import idb.core.logs.LogManager;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

public class WebActionEngine  {

	//Declare Drivers
	public WebDriver webDriver = null;
	public EventFiringWebDriver driver = null;
	public RemoteWebDriver remoteDriver = null;    

	//Declare Loggers
	private final String logClassName = "ActionEngine";

	//Declare log msg strings
	private final String msgClickSuccess = "Successfully Clicked On ";
	private final String msgClickFailure = "Unable To Click On ";
	private final String msgTypeSuccess = "Successfully Typed On ";
	private final String msgTypeFailure = "Unable To Type On ";
	private final String msgIsElementFoundSuccess = "Successfully Found Element ";
	private final String msgIsElementFoundFailure = "Unable To Found Element ";    


	public EventFiringWebDriver getDriver(){
		return this.driver;
	}

	public void closeDriver(){
		try{
			this.driver.quit();    		
			Thread.sleep(idbConstants.defSleepTime);
			LogManager.logInfo(WebActionEngine.class.getName(), "Closed web driver");
		}catch(Exception e){
			LogManager.logException(e, WebActionEngine.class.getName(), "Exception to close web driver");
		}
	}

	public RemoteWebDriver getRemoteDriver(){
		return this.remoteDriver;
	}

	/**
	 * @param inpPlatform
	 * @return
	 */
	protected Platform getPlatformName(String inpPlatform) {
		Platform targetPlatformName = Platform.ANY;
		inpPlatform = inpPlatform.trim().toUpperCase();

		if (inpPlatform.startsWith("WIN") || inpPlatform.equals("VISTA") ) {
			targetPlatformName = Platform.WINDOWS;
		} else if (inpPlatform.equals("LINUX")) {
			targetPlatformName = Platform.LINUX;
		} else if (inpPlatform.equals("UNIX")) {
			targetPlatformName = Platform.UNIX;
		} else if (inpPlatform.equals("MAC")) {
			targetPlatformName = Platform.MAC;
		} else if (inpPlatform.contains("DROID")) {
			targetPlatformName = Platform.ANDROID;
		} else if (inpPlatform.equals("IOS")) {
			targetPlatformName = Platform.IOS;
		}
		return targetPlatformName;
	}

	public EventFiringWebDriver setWebdriverForGrid(String gridhuburl, String browser) throws IOException, InterruptedException {
		String os = ( System.getProperty("OS") !=null && !"I don't care".equalsIgnoreCase(System.getProperty("OS")) ) ? System.getProperty("OS").trim() : null;	

		switch (browser.trim().toLowerCase()) {
		case "firefox":
			System.setProperty("webdriver.gecko.driver", idbConstants.firefoxDriverPath);
			FirefoxOptions fOptions = new FirefoxOptions();
			if(os !=null) fOptions.setCapability(CapabilityType.PLATFORM_NAME,os);
			webDriver = new RemoteWebDriver(new URL(gridhuburl), fOptions);
			break;
		case "internet explorer":
		case "internetexplorer":
		case "ie":
			InternetExplorerOptions ieOptions = new InternetExplorerOptions();
			/*ieOptions.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
        	//ieOptions.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
        	ieOptions.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true); 
        	ieOptions.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
        	//ieOptions.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
        	ieOptions.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
        	//ieOptions.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);
        	ieOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);    */

			ieOptions.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			ieOptions.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, UnexpectedAlertBehaviour.DISMISS);
			ieOptions.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true); 
			ieOptions.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			ieOptions.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			ieOptions.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
			//ieOptions.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			ieOptions.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);     	
			//ieOptions.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
			ieOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); 
			ieOptions.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "normal");
			//	ieOptions.setCapability(CapabilityType.PLATFORM_NAME,"Windows");

			if(os !=null) ieOptions.setCapability(CapabilityType.PLATFORM_NAME,os);

			//Clear browsing history
			try{
				Process p = Runtime
						.getRuntime()
						.exec("RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 255");
				p.waitFor();}
			catch(Exception e){
				System.out.println(e);
			}

			File driverExe = new File(idbConstants.ieDriverPath);
			System.setProperty("webdriver.ie.driver", driverExe.getAbsolutePath());
			webDriver = new RemoteWebDriver(new URL(gridhuburl), ieOptions);
			Thread.sleep(1000);
			break;

		case "chrome":
			System.setProperty("webdriver.chrome.driver", idbConstants.chromeDriverPath);
			ChromeOptions options = new ChromeOptions();
			options.addArguments("test-type");
			options.addArguments("--disable-extensions");                
			options.addArguments("--dns-prefetch-disable");
			options.addArguments("--start-maximized");
			options.addArguments("--disable-infobars");   
			if(os !=null) options.setCapability(CapabilityType.PLATFORM_NAME,os);
			webDriver = new RemoteWebDriver(new URL(gridhuburl), options);
			break;

		case "Safari":
			SafariOptions soptions = new SafariOptions();
			if(os !=null) soptions.setCapability(CapabilityType.PLATFORM_NAME,os);
			webDriver = new RemoteWebDriver(new URL(gridhuburl), soptions);
		}
		driver = new EventFiringWebDriver(this.webDriver);
		WebDriverListener myListener = new WebDriverListener();
		driver.register(myListener); 

		driver.manage().window().maximize();

		return this.driver;
	}

	public EventFiringWebDriver setWebDriverForLocal(String browser) throws IOException, InterruptedException {

		switch (browser.trim().toLowerCase()) {
		case "firefox":
			System.setProperty("webdriver.gecko.driver", idbConstants.firefoxDriverPath);
			FirefoxProfile ffProfile = new FirefoxProfile();
			ffProfile.setPreference("browser.download.dir", idbConstants.downloadsPath);
			ffProfile.setPreference("browser.download.folderList", 2);
			ffProfile.setPreference("browser.download.manager.showWhenStarting",false);
			ffProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf;application/xls;text/csv"); 
			ffProfile.setPreference("pdfjs.disabled", true);
			ffProfile.setPreference("pdfjs.enabledCache.state", false);
			FirefoxOptions ffoptions = new FirefoxOptions().setProfile(ffProfile);
			webDriver = new FirefoxDriver(ffoptions);
			break;

		case "ie":
			InternetExplorerOptions ieOptions = new InternetExplorerOptions();
			ieOptions.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			ieOptions.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, UnexpectedAlertBehaviour.DISMISS);
			ieOptions.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true); 
			ieOptions.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			ieOptions.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, false);
			ieOptions.setCapability(InternetExplorerDriver.NATIVE_EVENTS, true);
			//ieOptions.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
			ieOptions.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true);     	
			//ieOptions.setCapability(InternetExplorerDriver.FORCE_CREATE_PROCESS, true);
			ieOptions.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); 
			ieOptions.setCapability(CapabilityType.PAGE_LOAD_STRATEGY, "normal");
			//	ieOptions.setCapability(CapabilityType.PLATFORM_NAME,"Windows");

			//Clear browsing history
			Process p = Runtime
					.getRuntime()
					.exec("RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 255");
			p.waitFor();	

			File driverExe = new File(idbConstants.ieDriverPath);
			System.setProperty("webdriver.ie.driver", driverExe.getAbsolutePath());
			webDriver = new InternetExplorerDriver(ieOptions);
			Thread.sleep(1000);

			break;

		case "chrome":
			System.setProperty("webdriver.chrome.driver", idbConstants.chromeDriverPath);
			//DesiredCapabilities chromeCaps = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("test-type");
			options.addArguments("--disable-extensions");                
			options.addArguments("--dns-prefetch-disable");
			options.addArguments("--start-maximized");
			options.addArguments("--disable-infobars");   
			//chromeCaps.setCapability(ChromeOptions.CAPABILITY, options);

			Map<String, Object> prefs = new LinkedHashMap<>();
			prefs.put("plugins.always_open_pdf_externally", true);
			prefs.put("download.default_directory", idbConstants.downloadsPath);
			options.setExperimentalOption("prefs", prefs);

			//webDriver = new ChromeDriver(chromeCaps);
			webDriver = new ChromeDriver(options);

			break;

		case "Safari":
			webDriver = new SafariDriver();

			/*	for(int i=1;i<=10;i++){

						try{
							WebDriver driver=new SafariDriver();
							//WebDriver=new SafariDriver();

							break;
						}catch(Exception e1){
							Runtime.getRuntime().exec("taskkill /F /IM Safari.exe");
							Thread.sleep(3000);
							Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
							Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe"); 

							continue;   

						}

					}*/
		}
		driver = new EventFiringWebDriver(this.webDriver);
		WebDriverListener myListener = new WebDriverListener();
		driver.register(myListener);

		this.driver.manage().timeouts().pageLoadTimeout(idbConstants.pageLoadTimeout, TimeUnit.SECONDS);
		this.driver.manage().timeouts().implicitlyWait(idbConstants.implicitWaitTimeout, TimeUnit.SECONDS);	
		return this.driver;
	}

	public void setRemoteWebDriverForCloudSauceLabs() throws IOException, InterruptedException {
		//        if (this.browser.equalsIgnoreCase("Safari")) {
		//            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		//            desiredCapabilities.setCapability(CapabilityType.BROWSER_NAME, this.browser);
		//            desiredCapabilities.setCapability(CapabilityType.VERSION, this.version);
		//            desiredCapabilities.setCapability(CapabilityType.PLATFORM, this.platform);
		//            desiredCapabilities.setCapability("username", this.userName);
		//            desiredCapabilities.setCapability("accessKey", this.accessKey);
		//            desiredCapabilities.setCapability("accessKey", this.accessKey);
		//            desiredCapabilities.setCapability("name", this.executedFrom + " - " /*+ this.jobName + " - " + this.buildNumber*/ + this.platform + " - " + this.browser);
		//            URL commandExecutorUri = new URL("http://ondemand.saucelabs.com/wd/hub");
		//            for (int i = 1; i <= 10; i++) {
		//
		//                try {
		//                    this.WebDriver = new RemoteWebDriver(commandExecutorUri, desiredCapabilities);
		//
		//                    break;
		//                } catch (Exception e1) {
		//                    Runtime.getRuntime().exec("taskkill /F /IM Safari.exe");
		//                    Thread.sleep(3000);
		//                    Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
		//                    Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe");
		//
		//                    continue;
		//
		//                }
		//            }
		//        } else {
		//
		//            DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		//            desiredCapabilities.setCapability(CapabilityType.BROWSER_NAME, this.browser);
		//            desiredCapabilities.setCapability(CapabilityType.VERSION, this.version);
		//            desiredCapabilities.setCapability(CapabilityType.PLATFORM, this.platform);
		//            desiredCapabilities.setCapability("username", this.userName);
		//            desiredCapabilities.setCapability("accessKey", this.accessKey);
		//            desiredCapabilities.setCapability("accessKey", this.accessKey);
		//            desiredCapabilities.setCapability("name", this.executedFrom + " - " /*+ this.jobName + " - " + this.buildNumber*/ + this.platform + " - " + this.browser);
		//            URL commandExecutorUri = new URL("http://ondemand.saucelabs.com/wd/hub");
		//            this.WebDriver = new RemoteWebDriver(commandExecutorUri, desiredCapabilities);
		//            this.Driver = new EventFiringWebDriver(this.WebDriver);
		//            WebDriverListener myListener = new WebDriverListener();
		//            this.Driver.register(myListener);               
		//        }
	}    

	public boolean loadPage(String pageUrl, String strExpTitle) {
		try{		
			this.driver.manage().window().maximize();
			this.driver.get(pageUrl);			

			String strActTitle = this.driver.getTitle().trim();

			LogManager.logInfo(WebActionEngine.class.getName(), "Actual Title::"+strActTitle);
			LogManager.logInfo(WebActionEngine.class.getName(), "Expected Title::"+strExpTitle);

			if(strActTitle.equalsIgnoreCase(strExpTitle.trim())){
				return true;
			}else{
				return false;
			}    		
		}catch(WebDriverException e){
			LogManager.logException(e, WebActionEngine.class.getName(), "Exception to load web page using url " + pageUrl);
			return false;
		}
	}


	/**
	 * purpose: To wait for page load
	 * @param driver
	 * @param timeOutInSeconds
	 */
	public void jsWaitForPageToLoad(int timeOutInSeconds) {
		boolean flag=false;
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			String jsCommand = "return document.readyState";

			// Validate readyState before doing any waits
			if (js.executeScript(jsCommand).toString().equals("complete")) {
				flag=true;
			} else {
				for (int i = 0; i < timeOutInSeconds; i++) {
					Thread.sleep(500);
					flag=js.executeScript(jsCommand).toString().equals("complete")?true:false;
					if(flag)break;
				} //for-loop
			} //if-else

			if(!flag) {
				LogManager.logError(WebDriverListener.class.getName(), "Failed to Load Complete Page....");
			}
		} catch (Exception e) {
			LogManager.logException(e, WebDriverListener.class.getName(), "Exception to Load Complete Page....");
		}
	}


	public boolean selectByIndex(By locator, int index, String locatorName)  {
		boolean blnRes = false;
		try {
			Select selElement = new Select(driver.findElement(locator));
			sleep(idbConstants.defSleepTime);
			selElement.selectByIndex(index);
			// sleep(gblConstants.defSleepTime);
			List<WebElement> options = selElement.getOptions();
			WebElement seletedOption = selElement.getFirstSelectedOption();

			for(int cntr=0;cntr<options.size();cntr++){
				if (seletedOption.getText().trim().equalsIgnoreCase( options.get(cntr).getText().trim() )) {
					seletedOption.click();
					LogManager.logInfo(WebActionEngine.class.getName(), 
							"Successfully selected option " + seletedOption.getText() + " using index " + index + " from " + locatorName);
					blnRes = true;
					break;
				}else{
					blnRes = false;            		
				}
			}            
		}catch (Exception e) {
			LogManager.logException(e, logClassName, "Unable to select index " + index + " from " + locatorName);
			LogManager.logInfo(WebActionEngine.class.getName(), 
					"Failed to select option at index " + index + " from " + locatorName);        	
			blnRes = false;
		} 
		return blnRes;
	}

	public boolean selectByName(String locator, String optionName, String eleName){
		boolean blnRes = false;

		try{
			Select selElement = new Select(getElement(locator));
			List<WebElement> options = selElement.getOptions();
			for(WebElement option:options){
				if (option.getText().trim().equalsIgnoreCase(optionName.trim())){
					option.click();
					blnRes = true;
					LogManager.logInfo(WebActionEngine.class.getName(), "Successfully selected option - " + optionName + " from " + eleName);
					break;
				}
			}
			//selElement.selectByVisibleText(optionName);
		}catch(Exception e){
			blnRes = false;
			LogManager.logException(e, WebActionEngine.class.getName(), "Exception to select option " + optionName + " from " + eleName);
		}
		return blnRes;
	}

	/**
	 * @Purpose: To get selected value from drop down 
	 * @Output: boolean 
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getSelectedValue_Dropdown(String locator){
		String value=null;
		try {
			Select sel = new Select(getElement(locator));
			value = sel.getFirstSelectedOption().getText();
		}
		catch (Exception e) {
			LogManager.logException(e,WebActionEngine.class.getName(), "Exception to get selected value from dropdown");
		}
		return value;
	}

	public boolean click(By locator, String locatorName) throws Exception {
		boolean status = false;
		try {            
			WebElement ele = this.driver.findElement(locator);
			scrollElementIntoView(ele);
			if (ele.isDisplayed() && ele.isEnabled()){
				ele.click();
				status = true;
				LogManager.logInfo(logClassName, this.msgClickSuccess + locatorName);            
			}
		} catch (Exception e) {
			status = false;
			LogManager.logException(e, logClassName, this.msgClickFailure + locatorName);
			throw e;
		}
		return status;
	}

	public boolean click(String locator, String elementName){
		boolean status = false;
		try {
			WebElement ele = getElement(locator);
			//scrollElementIntoView(locator);
			if (ele.isDisplayed() && ele.isEnabled()){
				jsWaitForPageToLoad(idbConstants.wdWaitTimeout);
				ele.click();
				status = true;
				LogManager.logInfo(logClassName, this.msgClickSuccess + elementName);
			}
		} catch (Exception e) {
			status = false;
			LogManager.logException(e, logClassName, this.msgClickFailure + elementName);
			throw e;
		}
		return status;
	}    

	public boolean waitAndClick(String locator, String locatorName) throws Exception {
		boolean status = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, idbConstants.wdWaitTimeout);
			By eleLocator = byLocator(locator);
			WebElement ele = wait.until(ExpectedConditions.elementToBeClickable(eleLocator));

			if (ele.isDisplayed() && ele.isEnabled()){
				ele.click();
				status = true;
				LogManager.logInfo(logClassName, this.msgClickSuccess + locatorName);
			}
		} catch (Exception e) {
			status = false;            
			LogManager.logException(e, logClassName, this.msgClickFailure + locatorName);
			throw e;
		}
		return status;
	}

	public boolean clickUsingEnter(String locator, String elementName) throws Exception {
		boolean status = false;
		try {
			WebElement ele = getElement(locator);
			scrollElementIntoView(locator);
			if (ele.isDisplayed() && ele.isEnabled()){
				ele.sendKeys(Keys.ENTER);
				status = true; 
				try {
					Thread.sleep(1000);
					if (ele.isDisplayed() && ele.isEnabled())
						ele.sendKeys(Keys.ENTER);
				} catch (Exception e) {

				}
				LogManager.logInfo(logClassName, this.msgClickSuccess + elementName);
			}
		} catch (Exception e) {
			status = false;
			LogManager.logException(e, logClassName, this.msgClickFailure + elementName);
			throw e;
		}
		return status;
	}    

	/**
	 * If more than one element present
	 *
	 * @param locator
	 * @param index
	 * @return
	 * @throws Throwable
	 */
	public boolean click(By locator, int index, String locatorName)  {
		boolean status = false;
		try {
			WebElement ele = this.driver.findElements(locator).get(index);
			scrollElementIntoView(ele);
			if (ele.isDisplayed() && ele.isEnabled()){
				ele.click();
				status = true;
				LogManager.logInfo(logClassName, this.msgClickSuccess + locatorName);
			}
		} catch (Exception e) {
			status = false;
			LogManager.logException(e, logClassName, this.msgClickFailure + locatorName);            
		}
		return status;
	}

	public boolean clickusingJavaScript(String locator) {
		boolean blnRes = false;
		try{
			Thread.sleep(idbConstants.defSleepTime);
			WebElement element = this.driver.findElement(byLocator(locator));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
			LogManager.logInfo(logClassName, "Used java script executor to click element " + locator);
			blnRes = true;
		}catch(Exception e){
			blnRes = false;
			LogManager.logError(logClassName, "Unable to click " + locator + " using java script executor" + locator);
		}
		return blnRes;
	}

	public boolean clickUsingJavaScript(WebElement element) {
		boolean blnRes = false;
		try{
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
			LogManager.logInfo(logClassName, "Used java script executor to click element " + element);
			blnRes = true;
		}catch(Exception e){
			blnRes = false;
			LogManager.logError(logClassName, "Unable to click " + element + " using java script executor" + element);
		}
		return blnRes;
	}

	public void runJavaScriptMethod(String exeJavaScript){
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript(exeJavaScript, "");
	}

	public boolean isElementPresent(String locator, String locatorName, boolean expected)  {
		boolean status = false;
		try {
			WebElement ele = getElement(locator);
			status = (ele.isDisplayed())? true : false;
		} catch (Exception e) {
			status = false;
		}
		if (!(expected ^ status)) {
			status = true;
			LogManager.logInfo(logClassName, this.msgIsElementFoundSuccess + locatorName + " displayed as " + expected);                
		} else {
			status = false;
			LogManager.logError(logClassName, this.msgIsElementFoundFailure + locatorName + " displayed as " + expected);
		}        
		return status;
	}

	/**
	 * Check If Field is Enabled
	 * @param locator
	 * @param locatorName
	 * @return
	 */
	public boolean isElementEnabled(String locator,String locatorName, boolean expected) {
		boolean status = false;
		try {
			WebElement ele = getElement(locator);
			status = (ele.isEnabled())? true : false;

			if (!(expected ^ status)) {
				status = true;
				LogManager.logInfo(logClassName, this.msgIsElementFoundSuccess + locatorName + " enabled status as " + expected);                
			} else {
				status = false;
				LogManager.logError(logClassName, this.msgIsElementFoundFailure + locatorName + " enabled status as " + expected);
			}  			
		} catch (Exception e) {
			status = false;
		}
		return status;
	}	

	/**
	 * Switch to frame
	 * @return
	 */
	public boolean switchToIFrame() {
		boolean flag=false;
		try {
			WebElement iframe =	this.driver.findElement(By.tagName("iframe"));
			this.driver.switchTo().frame(iframe);
			flag=true;
		}catch(Exception e) {
			flag=false;
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to swith to frame");
			e.printStackTrace();
		}
		return flag;
	}

	/*//**
	 * Wait for js to load --
	 * @return
	 *//*
	public Boolean waitForJStoLoad() {
		boolean flag = false;
		String pageLoadStatus;
		JavascriptExecutor js = (JavascriptExecutor) driver;
		pageLoadStatus = (String) js.executeScript("return document.readyState");

		do {
			driver.manage().timeouts().implicitlyWait(gblConstants.defSleepTime, TimeUnit.SECONDS);
			pageLoadStatus = (String) js.executeScript("return document.readyState");
			if (pageLoadStatus.equals("complete")) {
				flag = true;
			}
		} while (!(pageLoadStatus.equalsIgnoreCase("complete")));

		return flag;
	} */

	//	/**
	//	 * Method to verify page load with explicit wait time set
	//	 *//*
	//	public void waitForPageLoad(int secs) {
	//		JavascriptExecutor  js = (JavascriptExecutor) driver;
	//		WebDriverWait wait = new WebDriverWait(driver, secs);
	//		wait.until(ExpectedConditions.jsReturnsValue("return document.readyState == \"complete\";"));
	//	}*/

	public boolean type(By locator, String testdata, String locatorName)  {
		boolean status = false;
		try {
			WebElement ele = driver.findElement(locator);
			ele.clear();
			ele.sendKeys(testdata);

			String strActValue = ele.getAttribute("value").trim(); 
			if (strActValue.equals(testdata)){
				status = true;
				LogManager.logInfo(logClassName, this.msgTypeSuccess + locatorName);
			}
		} catch (Exception e) {
			status = false;            
			LogManager.logException(e, logClassName, this.msgTypeFailure + locatorName);
		}
		return status;
	}

	/**
	 * @Purpose To set checkbox status to checked (true) or unchecked (false)
	 * @Constraints 
	 * @Input String  : Locator and Locator name
	 * @Output boolean : Returns true if checked or else false
	 */   
	public boolean setCheckbox(String locator, boolean blnEnabled, String locatorName){
		boolean blnRes = false;	
		WebElement ele = getElement(locator);    	
		if (ele !=null && (ele.isSelected() ^ blnEnabled) ) {
			ele.click();
			LogManager.logInfo(WebActionEngine.class.getName(), "Successfully updated checkbox " + locatorName + " as "+ blnEnabled);
		}

		if(getCheckboxStatus(locator, locatorName) == blnEnabled) {
			blnRes = true;
		}
		return blnRes;
	}

	/**
	 * @Purpose To get checkbox status
	 * @Constraints 
	 * @Input String  : Locator and Locator name
	 * @Output boolean : Returns true if selected or else false
	 */    
	public boolean getCheckboxStatus(String locator, String locatorName) {
		boolean blnRes = false;
		try {
			WebElement ele = getElement(locator);
			if (ele.isSelected()) {
				blnRes = true;
				LogManager.logInfo(WebActionEngine.class.getName(),
						"Successfully captured checkbox status of " + locatorName);
			}
		} catch (Exception e) {
			blnRes = false;
		}
		return blnRes;
	}

	/**
	 * Moves the mouse to the middle of the element. The element is scrolled
	 * into view and its location is calculated using getBoundingClientRect.
	 *
	 * @param locator     : Action to be performed on element (Get it from Object
	 *                    repository)
	 * @param locatorName : Meaningful name to the element (Ex:link,menus etc..)
	 */
	synchronized public boolean mouseHover(String locator, String locatorName) {
		boolean flag = false;
		try {
			WebElement mo = getElement(locator);
			new Actions(this.driver).moveToElement(mo).build().perform();
			flag = true;
			LogManager.logInfo(WebActionEngine.class.getName(), "MouseOver action is performed on " + locatorName);
		} catch (Exception e) {
			LogManager.logException(e, logClassName, "MouseOver action is not perform on " + locatorName);
			return false;
		}
		return flag;
	}

	synchronized public boolean mouseClick(String locator, String locatorName) {
		boolean flag = false;
		try {
			WebElement ele = getElement(locator);
			new Actions(this.driver).click(ele).build().perform();
			Thread.sleep(idbConstants.defSleepTime/2);
			flag = true;
			LogManager.logInfo(WebActionEngine.class.getName(), "MouseOver action is performed on " + locatorName);
		} catch (Exception e) {
			LogManager.logException(e, logClassName, "MouseOver action is not perform on " + locatorName);
			return false;
		}
		return flag;
	}

	public void sleep(int time) {
		try{
			Thread.sleep(time);        	
		}catch(InterruptedException ie){
			System.out.println("Exception to sleep for " + time + " msec");
		}
	}

	public boolean waitForElementPresent(By by, String locator, int secs)  {
		boolean status = false;

		try {

			WebDriverWait wait = new WebDriverWait(this.driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(by));
			/*((JavascriptExecutor) Driver).executeScript(
					"arguments[0].scrollIntoView();", by);*/

			for (int i = 0; i < secs / 2; i++) {
				List<WebElement> elements = this.driver.findElements(by);
				if (elements.size() > 0) {
					status = true;
					return status;

				} else {
					this.driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				}
			}


		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Exception to wait for element " + locator + " to be present");
			return status;
		}

		return status;

	}

	/**
	 * Binding to get Xpath, CSS, Link, Partial link element
	 *
	 * @param locator locator of element in xpath=locator; css=locator etc
	 * @return found WebElement
	 */
	public WebElement getElement(final String locator) {
		return getElement(locator, true);
	}


	/**
	 * Get "By" object to locate element
	 *
	 * @param locator locator of element in xpath=locator; css=locator etc
	 * @return by object
	 */
	public By byLocator(final String locator) {
		String prefix = locator.substring(0, locator.indexOf('='));
		String suffix = locator.substring(locator.indexOf('=') + 1);

		switch (prefix) {
		case "xpath":
			return By.xpath(suffix);
		case "css":
			return By.cssSelector(suffix);
		case "link":
			return By.linkText(suffix);
		case "partLink":
			return By.partialLinkText(suffix);
		case "id":
			return By.id(suffix);
		case "name":
			return By.name(suffix);
		case "tag":
			return By.tagName(suffix);
		case "className":
			return By.className(suffix);
		default:
			return null;
		}
	}

	/**
	 * @param locator          locator of element in xpath=locator; css=locator etc
	 * @param screenShotOnFail make screenshot on failed attempt
	 * @return found WebElement
	 */
	protected WebElement getElement(final String locator, boolean screenShotOnFail) {
		try {
			return driver.findElement(byLocator(locator));
		} catch (NoSuchElementException nse) {
			return null;
		} catch (Exception e) {
			if (screenShotOnFail) ;
			throw e;
		}	
	}
	/**
	 * Takes screenshot with default name
	 *
	 * @return url (or path for local machine) to saved screenshot
	 */
	/*protected String takeScreenshot() {
        return takeScreenshot(randomStringTime(23));
    }

	 *//**
	 * Takes screenshot of current page. Screenshots are placed in /screenshots directory of project's root
	 *
	 * @param fileName name to give for screenshot.
	 * @return url (or path for local machine) to saved screenshot
	 *//*
	 *//* protected String takeScreenshot(String fileName) {
        try {
            //Capture Screenshot
            TakesScreenshot driver = !getGrid() || getMobile() ?
                    (TakesScreenshot) Driver :
                    (TakesScreenshot) new Augmenter().augment(Driver);

            File tempFile = driver.getScreenshotAs(OutputType.FILE);
            saveAllureScreenshot(driver.getScreenshotAs(OutputType.BYTES));
            //Name and save file
            String path = getRelativeScreenshotPath(fileName);
            File screenShotFile = new File(path);
            FileUtils.copyFile(tempFile, screenShotFile);

            String strace = "";
            for (StackTraceElement el : Thread.currentThread().getStackTrace())
                strace += el.toString() + System.lineSeparator()
                        ;
            log.debug(strace);


            //Create link to screenshot
            String url = getScreenshotUrl(screenShotFile, fileName);
            log.info("SCREENSHOT: " + url);
            return url;

        } catch (Exception e) {
            //Suppress exception no need to fail test
            log.warn("takeScreenshot failed:", e);
            return e.getMessage();
        }
    }


	  *//**//**
	  * Composes url to screenshot that will be shown in build log
	  * In case of local build - result is screenshot local path
	  * In case of jenkins build - points to jenkins build artifacts
	  * <p>
	  * NOTE: url will become valid only after build is complete and results are archived
	  *
	  * @param screenShotFile
	  * @param fileName       name to give for screenshot.
	  * @return url (or path for local machine) to saved screenshot
	  *//**//*
    String getScreenshotUrl(File screenShotFile, String fileName) throws IOException {
        String url = null;
        if (isLocal()) {
            url = screenShotFile.getCanonicalPath();
        } else {
            url = System.getenv("BUILD_URL") + "/artifact/test_automation/" + getRelativeScreenshotPath(fileName);
        }

        return url;
    }

	   *//**/

	/**
	 * Composes relative path to screenshot file
	 *
	 * @param
	 * @return path
	 *//**//*
    String getRelativeScreenshotPath(String fileName) {
        String path = "screenshots/" + getScreenShotsDir() +
                "/" + Thread.currentThread().getName().replaceAll("\\(|\\)", "") + "/" + fileName + ".png";
        return path;
    }*/
	public boolean waitForElementPresent(String locator, int secs) {
		boolean status = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, secs);
			WebElement reqEle = wait.until(ExpectedConditions.visibilityOfElementLocated(byLocator(locator)));            
			status = (reqEle != null) ? true : false;
			//this.reporter.SuccessReport("Checked element present", "Successfully checked element present " + locator, Driver);
			//this.extentMngr.logResult(extentTC, Status.PASS, "Successfully checked element present " + locator);
			LogManager.logInfo(logClassName, "Successfully checked element present " + locator);
		} catch (TimeoutException te) {
			LogManager.logInfo(logClassName, "Timeout to check element present " + locator);
			status = false;
		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Exception to wait for element " + locator + " to be present");
			e.printStackTrace();
			status = false;
		}
		return status;
	}

	public boolean waitForElementNotPresent(String locator, int secs)  {
		boolean status = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, secs);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(byLocator(locator)));
			status = true;
			//this.reporter.SuccessReport("Checked element absence", "Successfully checked element absence " + locator, Driver);
			//this.extentMngr.logResult(extentTC, Status.PASS, "Successfully checked element absence " + locator);
			LogManager.logInfo(logClassName, "Successfully checked element absence " + locator);
		} catch(TimeoutException te) { 
			LogManager.logInfo(logClassName, "Timeout to check element absence " + locator);
		}catch (Exception e) {
			//this.reporter.failureReport("Checked element absence", "Failed - " + locator + "present", Driver);            
			//this.extentMngr.logResult(extentTC, Status.FAIL, "Failed to checked element absence " + locator);
			LogManager.logException(e, logClassName, "Failed to checked element absence " + locator);
			e.printStackTrace();
		}
		return status;
	}

	public boolean waitForElementToBeClickable(String locator, int secs)  {

		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(driver, secs);
			wait.until(ExpectedConditions.elementToBeClickable(byLocator(locator)));
			flag = true;
		} catch(TimeoutException te) {
			LogManager.logInfo(logClassName, "Timeout to wait for element " + locator + " to be clickable");
		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Exception to wait for element " + locator + " to be clickable");
			e.printStackTrace();
		}
		return flag;
	}

	public void waitForElementToBeClickable(String locator)  {
		waitForElementToBeClickable(locator,idbConstants.wdWaitTimeout);
	}


	/**
	 * Soft wait for visibility of element with default timeout
	 *
	 * @param locator locator of element to wait for
	 * @return true if element is present and visible / false otherwise
	 */
	protected boolean waitForElementPresent(final String locator)  {
		return waitForElementPresent(locator, idbConstants.wdWaitTimeout);
	}

	/**
	 * Wait until element is invisible/not present on the page with default timeout
	 *
	 * @param locator locator to element
	 */
	protected void waitForElementNotPresent(final String locator)  {
		waitForElementNotPresent(locator, idbConstants.wdWaitTimeout);
	}


	/**
	 * Wait for invisibility of specific object on page
	 *
	 * @param locator of object that we wait for invisibility
	 */
	protected void waitForInvisibility(final String locator) {

		WebDriverWait wait = new WebDriverWait(driver, idbConstants.wdWaitTimeout);
		try {
			wait.until(ExpectedConditions.invisibilityOfElementLocated(byLocator(locator)));
		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to wait for element invisibility");
			//log.info("Try to wait little more (wait for invisibility)");
		}
	}

	/**
	 * Verifies whether element is present and displayed
	 *
	 * @param locator locator for element to verify
	 * @return true if present; false otherwise
	 */
	protected boolean isElementPresent(final String locator) {
		try {
			return isElementPresent(getElement(locator, false));
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @Purpose To get Element presence 
	 * @Constraints 
	 * @Input String  : LocatorName and boolean presence
	 * @Output boolean : Returns true | false
	 */
	public boolean isElementPresent(String locatorName, boolean expected)  {
		boolean status = false;
		try {
			WebElement ele = getElement(locatorName);
			status = (ele.isDisplayed())? true : false;
		} catch (Exception e) {
			status = false;
		}
		if (!(expected ^ status)) {
			status = true;
			LogManager.logInfo(logClassName, "ElementFoundSuccess" + locatorName + " diplayed as " + expected);                
		} else {
			status = false;
			LogManager.logError(logClassName, "ElementFoundFailure" + locatorName + "Not Displayed " + expected);
		}        
		return status;
	}

	/**
	 * Binding to click the webElement
	 *
	 * @param we webElement to click
	 */
	protected void click(final WebElement we) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", we);
			we.click();
		} catch (Exception e) {
			//log.error("Failed to click:", e);
			//takeScreenshot();
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to click button");
			throw e;
		}
	}


	/**
	 * Binding to click Xpath, CSS, Link, Partial link element
	 *
	 * @param locator locator of the element in format xpath=locator; css=locator  etc
	 */
	protected void click(final String locator) {
		click(getElement(locator));
	}

	/**
	 * Verifies whether element is displayed
	 *
	 * @param we webelement to verify
	 * @return true if present; false otherwise
	 */

	protected boolean isElementPresent(final WebElement we) {

		try {

			return we.isDisplayed();

		} catch (Exception e) {

			return false;


		}

	}

	public void type(final WebElement we, String testdata)  {

		try {
			//((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", we);
			we.clear();
			we.click();
			we.sendKeys(testdata);
		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to enter text");
			throw e;
		}
	}

	public boolean type(String locatorName, String testdata)  {
		boolean blnRes = false;
		WebElement ele = getElement(locatorName);
		type(ele, testdata);
		LogManager.logInfo(WebActionEngine.class.getName(), "Successfully entered  " + testdata + " in element " + locatorName);
		if(ele.getAttribute("value").trim().equals(testdata.trim())){
			blnRes = true;
		}else{
			blnRes = false;
		}
		return blnRes;
	}

	public void type(String locatorName, String testdata, boolean clear, boolean keyClear)  {

		typeKeys(getElement(locatorName), testdata, clear, keyClear);
	}

	public void clickEnter(String locatorName)  {
		try {
			WebElement ele = getElement(locatorName);
			scrollElementIntoView(ele);
			ele.sendKeys(Keys.ENTER);
			LogManager.logInfo(logClassName, this.msgClickSuccess + locatorName);
		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to perform click  and enter");
			e.printStackTrace();
		}
	}

	/**
	 * @param locatorName: webelement having multiple elements
	 * @return list of webelements
	 * @throws Throwable
	 */
	public List<WebElement> getAllElements(String locatorName)  {


		List<WebElement> elements = new ArrayList<WebElement>();
		try {
			elements = driver.findElements(byLocator(locatorName));
			return elements;

		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to get elements");
			e.printStackTrace();
		}
		return elements;
	}

	/**
	 * Binding to check Checkbox
	 *
	 * @param we webElement of checkbox to check
	 */
	protected void check(final WebElement we) {
		if (!we.isSelected()) {
			we.click();
		}
	}

	/**
	 * Binding to check checkbox
	 *
	 * @param locator locator of checkbox to check
	 */
	protected void check(final String locator) {
		check(getElement(locator));
	}

	/**
	 * Binding to clear text field and enter text
	 *
	 * @param we       webElement to type to
	 * @param value    value to type into the field
	 * @param clear    true to clear the field first; false to enter text without clearing field
	 * @param keyClear true to clean using keyboard shortcut; false without clean;
	 * @return webElement with edited text
	 */
	protected WebElement typeKeys(final WebElement we, final String value, final boolean clear, final boolean keyClear) {
		if (clear) we.clear();
		if (keyClear) {
			we.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			we.sendKeys(Keys.chord(Keys.DELETE));
		}
		we.sendKeys(value);
		return we;
	}

	public String getVisibleText(final String locator)  {
		String text = "";
		try {
			WebElement ele = getElement(locator); 
			text = ele.getText();
			return text;
		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to get visibile text");
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * @Purpose: To selected attribute value 
	 * @Output: boolean 
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getAttributeValue(String locator,String expectedValue){
		String message=null;
		try {
			WebElement element=getElement(locator);
			message=element.getAttribute(expectedValue);
			LogManager.logInfo(WebActionEngine.class.getName(), "Successfully got attribute value "+message+" from page");
		}
		catch (Exception e) {
			LogManager.logException(e,WebActionEngine.class.getName(), "Exception to get attribute value from page");
		}
		return message;
	}

	/**
	 * Binding to select item in dropdown which needs to be clicked for edit mode.
	 * Fills actual value and presses "TAB" to submit, otherwise value could be not saved
	 *
	 * @param clickE        webElement of the field to click
	 * @param selectLocator locator of the dropdown
	 * @param value         value to select
	 */
	protected void clickToSelect(final WebElement clickE, final String selectLocator, final String value) {
		click(clickE);
		selectDropDown(getElement(selectLocator), value);
		getElement(selectLocator).sendKeys(Keys.TAB);
	}

	/**
	 * Binding to select item in dropdown by value
	 *
	 * @param we     WebElement of the dropdown
	 * @param option value to select in the dropdown
	 */
	protected void selectDropDown(final WebElement we, final String option) {

		try {
			getSelect(we).selectByVisibleText(option);
		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to select option from dropdown");
			//takeScreenshot();
			throw e;
		}
	}

	protected void selectDropDown(By locator, String option, String locatorName)  {

		try {
			WebDriverWait wait = new WebDriverWait(driver, idbConstants.wdWaitTimeout);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			new Select(driver.findElement(locator)).selectByVisibleText(option);
			//this.reporter.SuccessReport("Drop Down Selection " + locatorName, "Successfully Selected " + option + " in drop down " + locatorName, this.Driver);
			//this.extentMngr.logResult(extentTC, Status.PASS, "Successfully Selected " + option + " in drop down " + locatorName);
			LogManager.logInfo(logClassName, "Successfully Selected " + option + " in drop down " + locatorName);
			//logger.log(Status.PASS, "Successfully Selected " + option + " in drop down " + locatorName, logger.addScreenCapture(capture(Driver, "DropdownPass")));
		} catch (Exception e) {
			//this.reporter.failureReport("Drop Down Selection " + locatorName, "Failed to select value" + option + " in drop down " + locatorName, this.Driver);
			//this.extentMngr.logResult(extentTC, Status.FAIL, "Failed to select value" + option + " in drop down " + locatorName);
			LogManager.logException(e, logClassName, "Failed to select value" + option + " in drop down " + locatorName);
			throw e;
		}
	}

	protected Select getSelect(final WebElement we) {
		return new Select(we);
	}

	public void scrollElementIntoView(String locator) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", getElement(locator));
	}

	public void scrollUpElementIntoView(String locator) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(false);", getElement(locator));
	}

	public void scrollElementIntoView(WebElement elementToScroll) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elementToScroll);
	}

	public String timeStamp() {
		/*java.util.Date today = new java.util.Date();
		return new java.sql.Timestamp(today.getTime()).toString();*/
		Date now = new Date();
		String time = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH).format(now);
		System.out.println(time);
		return time;
	}

	public void typeUsingJavaScript(String locator, String data) {

		WebElement element = this.driver.findElement(byLocator(locator));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].value='" + data + "';", element);
	}

	public void typeUsingJavaScriptInHTMLInnerText(String locator, String data) {

		WebElement element = this.driver.findElement(byLocator(locator));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].innerHTML ='" + data + "';", element);
	}

	//    public void assertTrue(ExtentTest extentTC, boolean condition, String message, String strStepName, String failedMessage, String passMessage) throws Throwable {
	//        if (!condition) {
	//        	//this.reporter.failureReport(strStepName, failedMessage, Driver);        	
	//            this.extentMngr.logResult(extentTC, Status.FAIL, failedMessage);
	//            this.extentMngr.attachScreenshot(extentTC, Status.FAIL, captureScreenShot("assertTrue_Fail"));
	//            LogManager.logError(logClassName, "Assertion failure with message " + failedMessage);
	//            Assert.assertTrue(condition, message);
	//        } else {
	//            this.extentMngr.logResult(extentTC, Status.PASS, failedMessage);
	//            LogManager.logInfo(logClassName, "Assertion pass with message " + passMessage);
	//        	//this.reporter.SuccessReport(strStepName, passMessage, Driver);
	//        }
	//    }
	//
	//    public void assertFalse(ExtentTest extentTC, boolean condition, String message, String strStepName, String failedMessage, String passMessage) throws Throwable {
	//        if (condition) {
	//            //this.reporter.failureReport(strStepName, failedMessage, Driver);
	//            this.extentMngr.logResult(extentTC, Status.FAIL, failedMessage);
	//            this.extentMngr.attachScreenshot(extentTC, Status.FAIL, captureScreenShot("assertFalse_Fail"));
	//            LogManager.logError(logClassName, "Assertion failure with message " + failedMessage);
	//            Assert.assertFalse(condition, message);
	//        } else {
	//            //this.reporter.SuccessReport(strStepName, passMessage, Driver);        	
	//            this.extentMngr.logResult(extentTC, Status.PASS, passMessage);
	//            LogManager.logInfo(logClassName, "Assertion pass with message " + passMessage);        	
	//        }
	//    }
	//
	//    public void assertEqual(ExtentTest extentTC, String actual, String expected, String message, String strStepName, String failedMessage, String passMessage) throws Throwable {
	//        if (!actual.equals(expected)) {
	//            //this.reporter.failureReport(strStepName, failedMessage, Driver);
	//            this.extentMngr.logResult(extentTC, Status.FAIL, failedMessage);
	//            this.extentMngr.attachScreenshot(extentTC, Status.FAIL, captureScreenShot("assertEqual_Fail"));
	//            LogManager.logError(logClassName, "Assertion failure with message " + failedMessage);        	
	//            Assert.assertEquals(actual, expected, message);
	//        } else {
	//        	//this.reporter.SuccessReport(strStepName, passMessage, Driver);
	//        	this.extentMngr.logResult(extentTC, Status.PASS, passMessage);
	//            LogManager.logInfo(logClassName, "Assertion pass with message " + passMessage);         	            
	//        }
	//    }
	//
	//    public void assertEqual(ExtentTest extentTC, int actual, int expected, String message, String strStepName, String failedMessage, String passMessage) throws Throwable {
	//        if (actual == expected) {
	//            //this.reporter.failureReport(strStepName, failedMessage, Driver);
	//            this.extentMngr.logResult(extentTC, Status.FAIL, failedMessage);
	//            this.extentMngr.attachScreenshot(extentTC, Status.FAIL, captureScreenShot("assertEqual_Fail"));
	//            LogManager.logError(logClassName, "Assertion failure with message " + failedMessage);         	
	//            Assert.assertEquals(actual, expected, message);
	//        } else {
	//        	//this.reporter.SuccessReport(strStepName, passMessage, Driver);
	//        	this.extentMngr.logResult(extentTC, Status.PASS, passMessage);
	//            LogManager.logInfo(logClassName, "Assertion pass with message " + passMessage);            	
	//        }
	//    }

	public void highlight(String locator) {
		WebElement elem = getElement(locator);
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='6px solid yellow'", elem);
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='6px solid red'", elem);
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='6px solid blue'", elem);
		((JavascriptExecutor) driver).executeScript("arguments[0].style.border='0px solid green'", elem);
	}

	public void waitForGivenNumberOfWindowsToOpen(int expectedWindowsCount, int timeUnitsInSec) throws InterruptedException {
		int actualWindowsCount = driver.getWindowHandles().size();
		for (int i = 0; i < timeUnitsInSec; i++) {
			Thread.sleep(1000);
			if (expectedWindowsCount == actualWindowsCount)
				break;
		}
	}

	/**
	 *@Purpose: To capture screenshot of webpage 
	 *@Dependancy: Using 3rd party library AShot
	 *@param: String reportsPath, String screenshot Name
	 *@Output: String updated screenshot Name
	 *@Author-Date: 
	 *@Reviewed-Date:
	 */
	public String captureScreenShot(String strReportsPath, String strSSName) {
		try{
			strSSName = strSSName.replaceAll("[^a-zA-Z0-9]", "").replaceAll("\\s+","_");
			//TakesScreenshot ts = (TakesScreenshot) this.driver;
			//used 3rd party library AShot to capture full page screenshot
			Screenshot ts=new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver); 
			//File source = ts.getScreenshotAs(OutputType.FILE);
			String epoch2 = String.valueOf(System.currentTimeMillis() / 1000);
			//String strSSRelativePath = "/extent_reports/screenshots/" + screenShotName + "_" + epoch2 + "_" + ".png";
			//String strSSRelativePath = "." + File.separator + gblConstants.screenshotDirName + 
			//		File.separator + strSSName + "_" + epoch2 + "_" + ".png";
			if(strSSName.replaceAll(" ", "").length()>30 ){
				strSSName = strSSName.replaceAll("\\s*", "").substring(0, 30) + "_" + epoch2 + ".png";
			}else {
				strSSName = strSSName + "_" + epoch2 + ".png";
			}
			String dest = strReportsPath + File.separator + idbConstants.screenshotDirName + File.separator + strSSName;
			//File destination = new File(dest);
			//FileUtils.copyFile(source, destination);
			ImageIO.write(ts.getImage(),"PNG",new File(dest));

			return strSSName;
		}catch(IOException io){
			LogManager.logException(io, WebActionEngine.class.getName(), "Exception to capture screenshot");
			return "";
		}catch(Exception e){
			LogManager.logException(e, WebActionEngine.class.getName(), "Exception to capture screenshot");
			return "";
		}
	}

	public boolean switchToWindowContainingTitle(String expTitle, boolean refresh){
		boolean blnRes = false;
		//Work around in OMS - Monitor for consistency
		if(refresh){
			driver.manage().window().maximize();
		}
		System.out.println(driver.getWindowHandles().size());
		Iterator<String> wndHandles = driver.getWindowHandles().iterator(); 
		int ii = 0;
		while(wndHandles.hasNext()){
			String wndHandle = wndHandles.next();
			String currWndTitle = "";

			this.driver.switchTo().window(wndHandle);
			sleep(1000);
			currWndTitle = this.driver.getTitle().trim(); ii++;
			LogManager.logInfo(WebActionEngine.class.getName(), "Window Title -" + ii +"->  " + currWndTitle);
			if(currWndTitle.toLowerCase().contains(expTitle.trim().toLowerCase())){
				blnRes = true;
				LogManager.logInfo(WebActionEngine.class.getName(), "Switching to window containing title " + expTitle);
				break;
			}
		}
		if(blnRes == false){
			LogManager.logInfo(WebActionEngine.class.getName(), "Unable to Switch to window containing title " + expTitle);
		}
		return blnRes;
	}

	public boolean switchToWindowContainingNoTitle(boolean refresh) {
		boolean blnRes = false;
		//Work around in OMS - Monitor for consistency
		if(refresh){
			driver.manage().window().maximize();
		}
		System.out.println(driver.getWindowHandles().size());
		Iterator<String> wndHandles = driver.getWindowHandles().iterator(); 
		int ii = 0;
		while(wndHandles.hasNext()){
			String wndHandle = wndHandles.next();
			this.driver.switchTo().window(wndHandle);
			String  currWndTitle = this.driver.getTitle().trim(); ii++;
			LogManager.logInfo(WebActionEngine.class.getName(), "Window Title -" + ii +"->  " + currWndTitle);
			if (currWndTitle.toLowerCase().trim().isEmpty()) {
				blnRes = true;
				LogManager.logInfo(WebActionEngine.class.getName(), "Switching to window having no title");
				break;
			}
		}
		if (blnRes == false) {
			LogManager.logInfo(WebActionEngine.class.getName(), "Unable to Switch to window no title ");
		}
		return blnRes;
	}

	public String switchToDefaultContent(){
		this.driver.switchTo().defaultContent();
		return this.driver.getTitle();
	}

	public boolean typeUsingKeyBoard(String locatorName, String testdata)  {
		boolean blnRes = false;
		WebElement ele = getElement(locatorName);
		ele.sendKeys(Keys.SHIFT,testdata);
		LogManager.logInfo(WebActionEngine.class.getName(), "Successfully entered  " + testdata + " in element " + locatorName);

		if(ele.getAttribute("value").trim().equalsIgnoreCase(testdata.trim())){
			blnRes = true;
		}else{
			blnRes = false;
		}
		return blnRes;
	}

	public String getDefaultContentWindowHandle() {
		return this.driver.switchTo().defaultContent().getWindowHandle();
	}

	public boolean moveFocusToElement(String locator) {
		boolean blnRes = false;
		try {
			WebElement element = this.driver.findElement(byLocator(locator));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].focus();",element);
			Thread.sleep(500);
			blnRes = true;
		} catch(Exception e){
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to move focus element");
			blnRes = false;
		}
		return blnRes;
	}

	public boolean saveDownloadFiles(String fileName) {
		boolean blnRes = false;
		try {

			//Creating Downloads directory if not present.
			File dir = new File(idbConstants.downloadsPath);
			if (!dir.exists()) {
				if (dir.mkdir()) {
					LogManager.logInfo(WebActionEngine.class.getName(), idbConstants.downloadsPath+" Directory is created!");
				} else {
					LogManager.logError(WebActionEngine.class.getName(), idbConstants.downloadsPath+" Failed to create directory!");
				}
			}

			Thread.sleep(idbConstants.wdWaitTimeout); //wait for file to download completely
			Robot robot = new Robot();  
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_SHIFT);
			robot.keyPress(KeyEvent.VK_S);
			robot.keyRelease(KeyEvent.VK_S);
			robot.keyRelease(KeyEvent.VK_SHIFT);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000);
			StringSelection dwnldPath = new StringSelection(idbConstants.downloadsPath +File.separator+fileName);
			LogManager.logInfo(WebActionEngine.class.getName(), "Downloaded file is "+fileName);
			Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
			clipboard.setContents(dwnldPath, dwnldPath);

			//copy paste the path in download window
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_TAB);  
			robot.keyPress(KeyEvent.VK_TAB); 
			robot.keyPress(KeyEvent.VK_TAB); 
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_TAB);

		}catch(Exception e){
			blnRes = false;
			LogManager.logException(e, WebActionEngine.class.getName(), "Unable to perform download");
		}
		return blnRes;
	}

	/**
	 * Purpose: To do right clicks  --Over ridden method from Web Engine to sync
	 * Input: Locator Name and test input
	 * Output: returns true if succeeded
	 */
	public boolean rightClick(String locator, String locatorName) {
		boolean flag = false;
		try {
			WebElement ele = getElement(locator);
			new Actions(this.driver).contextClick(ele).build().perform();
			Thread.sleep(idbConstants.defSleepTime/2);
			flag = true;
			LogManager.logInfo(WebActionEngine.class.getName(), "MouseOver action is performed on " + locatorName);
		} catch (Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "MouseOver action is not perform on " + locatorName);
			return false;
		}
		return flag;
	}	


	/**
	 * Purpose: To do double click -- on element with given locator
	 * Input: Locator Name and test input
	 * Output: returns true if succeeded
	 */	
	public void doubleClick(String elementLocator) {
		try {
			Actions action = new Actions(driver);
			WebElement element = getElement(elementLocator);
			action.doubleClick(element).perform();
		}catch(Exception e) {
			LogManager.logException(e, WebActionEngine.class.getName(), "Exception to double click element " + elementLocator);
		}
	}

	/**
	 * @Purpose: To Validate Error Message
	 * @param: String locator, String value
	 * @return: boolean
	 * @Author-Date: 
	 * @Reviewed-Date:
	 */
	public String getValidationMessage(String locator)
	{
		String validationMessage=null;
		try
		{
			waitForElementPresent(locator, idbConstants.wdWaitTimeout);
			scrollElementIntoView(locator);
			validationMessage=getAttributeValue(locator, "validationMessage");
			LogManager.logInfo(WebActionEngine.class.getName(), "Got valiadtion message: " + validationMessage + " for " + locator);
		}
		catch(Exception e)
		{
			LogManager.logException(e,WebActionEngine.class.getName(), "Unable to get validation error message for "+ locator);
		}
		return validationMessage;
	}	

	public Color getColor(String locator) {		
		Color elementColor = null;
		try {
			WebElement objTarget = getElement(locator);			
			elementColor = Color.fromString(objTarget.getCssValue("color"));			
		}catch(NoSuchElementException nse) {
			LogManager.logException(nse, WebActionEngine.class.getName(), "Exception to fetch color of " + locator);
		}		
		return elementColor;		
	}
}
