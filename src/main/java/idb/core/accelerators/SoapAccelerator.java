package idb.core.accelerators;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;


public class SoapAccelerator {

	private String endpoint;
	public String soapTemplateXMLFilePath;
	public HashMap<String, String> params;
	public String requestXML;
	String templateXML;
	String currRequestXML;
	private SOAPMessage responseSOAPMessage;
	private SOAPMessage requestSOAPMessage;

	public SOAPMessage getResponseSOAPMessage() {
		return responseSOAPMessage;
	}

	public void setResponseSOAPMessage(SOAPMessage responseSOAPMessage) {
		this.responseSOAPMessage = responseSOAPMessage;
	}

	public SOAPMessage getRequestSOAPMessage() {
		return requestSOAPMessage;
	}

	public void setRequestSOAPMessage(SOAPMessage requestSOAPMessage) {
		this.requestSOAPMessage = requestSOAPMessage;
	}

	/**
	 * Parameterized constructor that initializes the instance variables endpoint, request template XML nad Parameters 	
	 * @param endPoint
	 * @param xmlFilePath
	 *
	 * @throws SOAPException
	 * @throws IOException
	 */

	public SoapAccelerator(String endPoint, String xmlTemplateFilePath,HashMap<String, String> parameters) throws SOAPException, IOException, Exception {
		this.endpoint = endPoint;
		this.soapTemplateXMLFilePath = xmlTemplateFilePath;
		this.params = parameters;
		this.requestSOAPMessage = buildXML();
	}

	/*******Setter and getters for the instance variables************/
	public String getCurrRequestXML() {
		return currRequestXML;
	}


	public void setCurrRequestXML(String currRequestXML) {
		this.currRequestXML = currRequestXML;
	}


	public String getRequestXML() {
		return requestXML;
	}

	public String getResponseXML() throws Exception{
		return convertSOAPMessageToString(requestSOAPMessage);
	}

	public void setRequestXML(String requestXML) {
		this.requestXML = requestXML;
	}

	public String getTemplateXML() {
		return templateXML;
	}

	public void setTemplateXML(String templateXML) {
		this.templateXML = templateXML;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getSoapTemplateXMLFilePath() {
		return soapTemplateXMLFilePath;
	}

	public void setSoapTemplateXMLFilePath(String soapTemplateXMLFilePath) {
		this.soapTemplateXMLFilePath = soapTemplateXMLFilePath;
	}

	public HashMap<String, String> getParams() {
		return params;
	}

	public void setParams(HashMap<String, String> params) {
		this.params = params;
	}

	/*********************************************/


	//private SOAPMessage parseXML(String endpoint,String soapTemplateXMLFilePath, HashMap<String, String> params) throws SOAPException, IOException{		

	/**
	 * It builds the request XML with given input parameters
	 * @return SoapMessage 
	 * @throws SOAPException
	 * @throws IOException
	 */

	public SOAPMessage buildXML() throws SOAPException, IOException{

		String currLine;
		//this.endpoint = endpoint;		
		File requestFile = new File(soapTemplateXMLFilePath);
		BufferedReader br = new BufferedReader(new FileReader(requestFile));               
		//templateXML = new String(Files.readAllBytes(Paths.get("file")), StandardCharsets.UTF_8);		
		String fullFileContent = "";
		while ((currLine = br.readLine()) != null)
		{
			fullFileContent = fullFileContent + currLine; 
		}
		String requestXML = setParameters(fullFileContent,params);
		MessageFactory factory = MessageFactory.newInstance();
		SOAPMessage message = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(requestXML.getBytes(Charset.forName("UTF-8"))));
		currRequestXML=requestXML;
		return message;

	}


	/**
	 * It sets the given parameters in to Template XML and returns it.
	 * @param inputXML
	 * @return Request XML with parameters
	 */
	private String setParameters(String inputXML, HashMap<String, String> params) {

		String outputXML ="";

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();			;
			Document doc = dBuilder.parse(new InputSource(new StringReader(inputXML))); 
			doc.getDocumentElement().normalize();


			Iterator it = params.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry)it.next();

				NodeList nList = doc.getElementsByTagName(pair.getKey().toString());
				nList.item(0).setTextContent(pair.getValue().toString());

			}

			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			outputXML = writer.getBuffer().toString().replaceAll("\n|\r", "");
			System.out.println(outputXML);			
			currRequestXML =outputXML;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return currRequestXML;
	}



	/**
	 * It execute request XML and returs the responseSOAPMessage in the form of SoapMessage
	 * @return
	 * @throws SOAPException
	 * @throws IOException
	 */

	public SOAPMessage execute() throws SOAPException, IOException{
		return execute(this.endpoint);
	}

	/**
	 *
	 * @param currEndPoint
	 * @return
	 * @throws SOAPException
	 * @throws IOException
	 */
	public SOAPMessage execute(String currEndPoint) throws SOAPException, IOException{

		MessageFactory factory = MessageFactory.newInstance();
		SOAPMessage message = factory.createMessage(new MimeHeaders(), new ByteArrayInputStream(currRequestXML.getBytes(Charset.forName("UTF-8"))));
		System.out.println(message);
		SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
		SOAPConnection soapConnection = soapConnectionFactory.createConnection();
		System.out.println("BEFORE");
		responseSOAPMessage = soapConnection.call(message, currEndPoint);
		System.out.println("AFTER");
		return responseSOAPMessage;
	}

	/**
	 * It Prints the reposne in the console
	 *
	 * @throws Exception
	 */

	public String getSOAPResponseXML() throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = responseSOAPMessage.getSOAPPart().getContent();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		responseSOAPMessage.writeTo(stream);
		String message = new String(stream.toByteArray(), "utf-8");
		return message;

	}


	/**
	 * It validates status code in the responseSOAPMessage
	 * @param
	 * @throws Exception
	 */
	public int getStatuscode() throws Exception {
		int statusCode=-1;

		try{
			statusCode = Integer.parseInt(responseSOAPMessage.getSOAPPart().getEnvelope().getBody().getFault().getFaultCode());
		}		
		catch(Exception e){
			System.out.println("\n\n");
			System.out.println("Status Code in the responseSOAPMessage Header: HTTP/1.1 200 OK");
			statusCode=200;
		}
		return statusCode;
	}

	/**
	 * Converts soap message to XML string
	 * @param currSOAPMessage
	 * @return XML string
	 * @throws Exception
	 */
	public String convertSOAPMessageToString(SOAPMessage currSOAPMessage) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		Source sourceContent = currSOAPMessage.getSOAPPart().getContent();
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		currSOAPMessage.writeTo(stream);
		String message = new String(stream.toByteArray(), "utf-8");
		System.out.println(message);
		return message;

	}




	/**
	 * Fetches node value from responseSOAPMessage
	 *
	 * @param tagName
	 * @throws Exception
	 */

	public String getNodeValue(String tagName) throws Exception {

		SOAPBody body = responseSOAPMessage.getSOAPBody();
		NodeList list = body.getElementsByTagName(tagName);			
		System.out.println("Order Number is "+list.item(0).getTextContent());
		return list.item(0).getTextContent();

	}


	/**
	 * Fetches node value from responseSOAPMessage
	 *
	 * @param tagName
	 * @throws Exception
	 */

	public NodeList getNodeValueAsList(String tagName) throws Exception {

		SOAPBody body = responseSOAPMessage.getSOAPBody();
		NodeList list = body.getElementsByTagName(tagName);			
		System.out.println("Order Number is "+list.item(0).getTextContent());
		return list;

	}

	/**
	 * Fetches node count from responseSOAPMessage
	 *
	 * @param nodeName
	 * @return
	 * @throws Exception
	 */

	public int getNodeCount(String nodeName) throws Exception {

		SOAPBody body = responseSOAPMessage.getSOAPBody();
		NodeList list = body.getElementsByTagName(nodeName);		 
		return list.getLength();


	}


	/**
	 * Get all sub childs under given node 
	 * @param soapResponse
	 * @param nodeName
	 * @return List of Subchilds
	 * @throws Exception
	 */

	private NodeList getSubChilds(SOAPMessage soapResponse, String nodeName) throws Exception {

		SOAPBody body = soapResponse.getSOAPBody();			
		NodeList list = body.getElementsByTagName(nodeName).item(0).getChildNodes();

		for(int k=0;k<list.getLength();k++){
			String s = list.item(k).getTextContent();
			System.out.println(s);
		}
		return list;
	}



	/**
	 * Clone the given node and add it under given parent node 
	 * @param parentNode
	 * @param copyNode
	 * @param childNodesParams
	 * @return
	 */

	public String cloneNode(String parentNode, String copyNode, HashMap<String,String>[] childNodesParams) {


		String outputMultiNodeXML ="";

		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(new InputSource(new StringReader(currRequestXML))); 
			doc.getDocumentElement().normalize();

			NodeList tempNode0 = doc.getElementsByTagName(copyNode);

			//Node tempNode = tempNode0.item(0).cloneNode(true);

			//Node tempNode = doc.getElementById(copyNode).cloneNode(true);			

			//NodeList childNodes = tempNode.getChildNodes();

			System.out.println(childNodesParams.length); 

			for(int hashcount =0; hashcount<childNodesParams.length;hashcount++){
				Node tempNode = tempNode0.item(0).cloneNode(true);

				NodeList childNodes = tempNode.getChildNodes();
				for (String element : childNodesParams[hashcount].keySet()){	
					boolean nodeFound=false;

					for(int childNodeCount=0;childNodeCount<childNodes.getLength();childNodeCount++){

						if(childNodes.item(childNodeCount).getNodeName().equals(element)){

							childNodes.item(childNodeCount).setTextContent(childNodesParams[hashcount].get(element));
							nodeFound = true;
							break;
						}
					}

					if(nodeFound==false){
						System.out.println(parentNode+"/"+copyNode+"/"+element+ " tag name is not found");
					}
				}

				doc.getElementsByTagName(parentNode).item(0).appendChild(tempNode);	
				tempNode=null;
			}
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			currRequestXML = writer.getBuffer().toString().replaceAll("\n|\r", "");
			System.out.println(currRequestXML);

		}catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println(currRequestXML);
		return currRequestXML;
	}


	/**
	 * Function to convert document to file
	 *
	 * @param doc      responseSOAPMessage that we want to convert
	 * @param fileName new file name for xml responseSOAPMessage
	 */	
	public void convertDocumentToFile(Document doc, String fileName) throws TransformerException {
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		Result output = new StreamResult(new File(fileName));
		Source input = new DOMSource(doc);
		transformer.transform(input, output);

	}

	/**
	 * Function to create document from file
	 *
	 * @param fileName xml file path
	 * @return Document object
	 */
	private Document createDocumentFromFile(String fileName) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		InputSource is = new InputSource(fileName);
		is.setEncoding("UTF-8");
		Document outputDoc = dbFactory.newDocumentBuilder().parse(is);
		System.out.println("Created document from file: " + fileName);
		return outputDoc;
	}


	/**
	 * Function to convert xml to doc
	 *
	 * @param xmlStr
	 * @return Document object
	 */

	private Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builder;  
		try  
		{  
			builder = factory.newDocumentBuilder();  
			Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) ); 
			return doc;
		} catch (Exception e) {  
			e.printStackTrace();  
		} 
		return null;
	}


	/**
	 * get the Node list based on given xpath
	 * @param xpathExpression
	 * @return Node list
	 */
	public NodeList getNodeList(String xpathExpression){
		NodeList nodeList =null;
		try{

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(getSOAPResponseXML())));
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			nodeList = (NodeList) xpath.compile(xpathExpression).evaluate(doc, XPathConstants.NODESET);

		}
		catch(Exception e){
			e.printStackTrace();
		}

		return nodeList;

	}
	/**
	 * get the Node value based on given xpath
	 * @param xpathExpression
	 * @return String
	 */
	public String getNodeValueByXpath(String xpathExpression){
		Node node =null;
		try{

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(getSOAPResponseXML())));
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			node = (Node) xpath.compile(xpathExpression).evaluate(doc, XPathConstants.NODE);

		}
		catch(Exception e){
			e.printStackTrace();
		}

		return node.getTextContent();

	}

	/**
	 * •	Adds a new attribute. If an attribute with that name is already present in the element, its value is changed to be that of the value parameter.
	 * This value is a simple string; it is not parsed as it is being set. So any markup (such as syntax to be recognized as an entity reference) is treated as literal text,
	 * and needs to be appropriately escaped by the implementation when it is written out.
	 * In order to assign an attribute value that contains entity references,
	 * the user must create an Attr node plus any Text and EntityReference nodes, build the appropriate subtree, and use setAttributeNode to assign it as the value of an attribute.
	 * To set an attribute with a qualified name and namespace URI, use the setAttributeNS method.
	 * @param envelopeAttribute
	 * @param envelopeAttributeValue
	 * @throws SOAPException
	 */
	public void setEnvelopAttribute(String envelopeAttribute, String envelopeAttributeValue) throws SOAPException
	{
		SOAPEnvelope currEnv = requestSOAPMessage.getSOAPPart().getEnvelope();
		currEnv.setAttribute(envelopeAttribute, envelopeAttributeValue);
		requestSOAPMessage.saveChanges();
	}




	/**
	 * get the Node count based on xpath
	 * @param xpathExpression
	 * @return Node list
	 */
	public int getNodeCountUsingXpath(String xpathExpression){
		NodeList nodeList =null;
		try{
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(getSOAPResponseXML())));
			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();	
			nodeList = (NodeList) xpath.compile(xpathExpression).evaluate(doc, XPathConstants.NODESET);

		}
		catch(Exception e){
			e.printStackTrace();
		}
		return nodeList.getLength();
	}
	
	/**
	 * get formatted XML String
	 * 
	 * @param xml
	 * @return
	 */
	public String xmlFormatter(String xmlContent) {

		try {
			final InputSource src = new InputSource(new StringReader(xmlContent));
			final Node document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
			final Boolean keepDeclaration = Boolean.valueOf(xmlContent.startsWith("<?xml"));

			// May need this: System.setProperty(DOMImplementationRegistry.PROPERTY,"com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl");
			final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
			final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
			final LSSerializer writer = impl.createLSSerializer();

			writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE); // Set this to true if the output needs to be beautified.
			writer.getDomConfig().setParameter("xml-declaration", keepDeclaration); // Set this to true if the declaration is needed to be outputted.

			return writer.writeToString(document);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
