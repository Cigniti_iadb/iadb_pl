package idb.core.accelerators;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ISuite;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.annotations.Optional;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.model.Log;
import com.aventstack.extentreports.reporter.configuration.Theme;

import idb.config.idbConstants;
import idb.config.idbConstants.ComparisionType;
import idb.core.logs.LogManager;
import idb.core.reports.ExtentManager;
import idb.core.testmanagement.TestRailsAPIAccess;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import idb.core.testmanagement.JiraUtils;

public abstract class AbstractTestEngine {

	//To store suite execution start time stamp
	public static String suiteStartTime = "";
	public String suiteName="";
	
	//Reporting references
	public static ExtentManager extentMngr=null;
	public ExtentTest tcReport = null;	

	//Test Rail references
	public static boolean blnLog2TestRail=false;
	public static TestRailsAPIAccess trAccess = null;	
	public String trTestPlanId = null;
	
	//Storing test results data for bulk adding multiple test results
	public static List<Object> suiteResults = new ArrayList<Object>();

	//System parameters
	public String hostName="";
	public String hostIP = "";
	public String osName = System.getProperty("os.name");

	//Web App execution params
	public String browserUnderTest = "";
	public String execEnv = null; 
	public static String osUnderTest = null;

	//Web API execution params
	public String apiUT="";
	public String apiServerUrl = "";	
	
	//Mobile execution params
	public String appiumVerNo = "";
	public String appiumServerUrl = "";
	public DesiredCapabilities appiumDesiredCaps  = new DesiredCapabilities();
	//Mobile platform execution params	
	public String deviceUnderTest = null;

	//App parameters
	public String appUnderTest = null;
	public String testEnvName = null;
	
	protected SoftAssert testngAssertion = new SoftAssert();
	
	/*
	 *purpose: To Initialize extent reports and update system info
	 *author-date : Satya, Gajula / 1/8/18
	 *reviewer-date:
	 */
	public void initializeReport() {

		try {
			suiteStartTime = new SimpleDateFormat("dd_MMM_yyyy_hh_mm_ss_SSS").format(new Date());
			//String reportName = idbConstants.currSuiteName + "_" + suiteStartTime;
			String reportName = "ExecutionReport";
	        if(extentMngr ==null) {
	        	extentMngr = new ExtentManager(reportName, idbConstants.reportsPath);
	        }			
	        extentMngr.setAnalysisStrategy(AnalysisStrategy.TEST);
	        extentMngr.configReportTitle(this.appUnderTest + "-Test Results");
	        extentMngr.configReportName(this.appUnderTest);
	        extentMngr.configTheme(Theme.STANDARD);	

	    	//Capture testing host machine details
	        InetAddress myHost;
			myHost = InetAddress.getLocalHost(); 
	        hostName = myHost.getHostName();
	        hostIP = InetAddress.getLocalHost().getHostAddress();

	        //Update system information in extent reports        
	        extentMngr.setSystemInfo("Host O.S", this.osName);
	        extentMngr.setSystemInfo("Host Name - ", this.hostName);
	        extentMngr.setSystemInfo("Host I.P- ", this.hostIP);		
	        extentMngr.setSystemInfo("Test Suite Name", this.suiteName);
	        
	        extentMngr.flushReport();
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
	}

	public void initializeTestSuites(List<ISuite> suites) {
		
	}
	/*
	 *Purpose: To initialize a test rails connection
	 *author-date : Satya, Gajula / 1/8/18
	 *reviewer-date: 
	 */	
	public void initializeTestRailsAccess() {
		blnLog2TestRail = idbConstants.suiteConfig.getTestRailsLogging(this.appUnderTest);		
		String trUrl = idbConstants.suiteConfig.getTestRailsUrl(this.appUnderTest);
		String trUser = idbConstants.suiteConfig.getTestRailsUser(this.appUnderTest);
		String trPwd = idbConstants.suiteConfig.getTestRailsPassword(this.appUnderTest);
		trAccess = new TestRailsAPIAccess(trUrl, trUser, trPwd);		
	}

	

	/*
	 * Purpose: To update web application under test name & its environment details
	 *author-date : Satya, Gajula / 1/8/18
	 *reviewer-date: 
	 */
	public abstract void reportTestAppEnvironmentSummary();

	/*
	 * Purpose: To update web application under test name & its environment details
	 *author-date : Satya, Gajula / 1/8/18
	 *reviewer-date: 
	 */
	public void reportWebEnvironmentSummary() {
		extentMngr.setSystemInfo("Browser Under Test", this.browserUnderTest);
		extentMngr.setSystemInfo("App Under Test", this.appUnderTest);
        extentMngr.setSystemInfo("App Test Environment", testEnvName);
        extentMngr.setSystemInfo("App Url", idbConstants.suiteConfig.getTestAppUrl(this.appUnderTest, this.testEnvName));
        //extentMngr.setSystemInfo("App Version", gblConstants.suiteConfig.getTestAppVersion(this.appUnderTest, this.testEnvName));		
	}

	/*
	 * Purpose: To update web api environment under test
	 *author-date : Satya, Gajula / 1/8/18
	 *reviewer-date: 
	 */
	public void reportWebApiEnvironmentSummary() {
        extentMngr.setSystemInfo("App Under Test", this.appUnderTest);
        extentMngr.setSystemInfo("API Test Environment", this.testEnvName);
        
        //Check for wsdl/serviceUrl 
        if (apiUT.trim().length() > 0 && apiUT.toLowerCase().contains("http")) {
        	extentMngr.setSystemInfo("API Server Url", apiUT);
        }else {
        	extentMngr.setSystemInfo("API Server Url", idbConstants.suiteConfig.getTestAPIServerEndpoint(appUnderTest, testEnvName) + "/" + apiUT);
        }        		
	}

	

	/*
	 * Purpose: To update web application under test name & its environment details
	 * author-date : Satya, Gajula / 1/8/18
	 * reviewer-date: 
	 */
	public void reportMobileEnvironmentSummary() {
        
		extentMngr.setSystemInfo("Appium Ver No", this.appiumVerNo);
        extentMngr.setSystemInfo("Appium Sever Url", this.appiumServerUrl);
		extentMngr.setSystemInfo("App Under Test", appUnderTest);
        extentMngr.setSystemInfo("App Test Environment", testEnvName);
        extentMngr.setSystemInfo("Mobile Under Test", this.deviceUnderTest);
        extentMngr.setSystemInfo("Mobile OS Type", idbConstants.suiteConfig.getMobileDevicePlatform(this.appUnderTest, this.deviceUnderTest));
        extentMngr.setSystemInfo("Mobile OS Version", idbConstants.suiteConfig.getMobileDevicePlatformVer(this.appUnderTest, this.deviceUnderTest));

	}

	

	/*
	 * Purpose: To launch browser under test for target execution environment
	 * author-date : Satya, Gajula / 3/8/18
	 * reviewer-date:
	 */
	public boolean launchBrowser(WebActionEngine webEngine) {

		try{
            System.out.println(this.execEnv);        
            //Start webEngine for local driver
            if (this.execEnv.trim().equalsIgnoreCase("local")) {
            	webEngine.setWebDriverForLocal(this.browserUnderTest);
            //Start webEngine for cloud driver
            }else if (this.execEnv.trim().contains("saucelabs")) {
            	webEngine.setRemoteWebDriverForCloudSauceLabs();
            //Start webEngine for Selenium grid
            }else if (this.execEnv.trim().contains("hub")) {
            	webEngine.setWebdriverForGrid(this.execEnv.trim(), this.browserUnderTest);
            } 		
    	}catch(Exception e){
    		//Log Exception to start a driver instance
    		LogManager.logException(e, AbstractTestEngine.class.getName(), "Exception " + e.getMessage() + " to launch browser");
    		e.printStackTrace();
    	//	throw new SkipException("Unable to start browser " + this.browserUnderTest);
    	}		
		return false;
	}

	

	/*
	 * Purpose: To launch browser under test for target execution environment
	 * author-date : Satya, Gajula / 3/8/18
	 * reviewer-date:
	 */	
	public boolean loadApplication(WebActionEngine webEngine) {

    	String appUrl = idbConstants.suiteConfig.getTestAppUrl(this.appUnderTest, this.testEnvName);
    	String appTitle = idbConstants.suiteConfig.getTestAppPageTitle(this.appUnderTest);
		boolean blnLoadPage = webEngine.loadPage(appUrl, appTitle);

		/*if(!blnLoadPage){
			reportStatus(Status.FAIL, "Unable to load Login Page, Skipping test... ");
			throw new SkipException("Unable to load " + appUrl);
		}*/
		return blnLoadPage;
	}

	/*
	 * Purpose: To initialize desired capabilities for appium driver
	 * author: Satya, Gajula -  7th Aug 18
	 * reviewer: 
	 */
	public DesiredCapabilities initializeAppiumDesiredCapabilities() {		

		switch(idbConstants.suiteConfig.getMobileDevicePlatform(this.appUnderTest, this.deviceUnderTest).trim().toUpperCase()) {
			case "ANDROID":
				//Define desired capabilities
				appiumDesiredCaps.setCapability(MobileCapabilityType.APPIUM_VERSION, idbConstants.suiteConfig.getAppiumVer(this.appUnderTest));
				appiumDesiredCaps.setCapability(MobileCapabilityType.PLATFORM_NAME, idbConstants.suiteConfig.getMobileDevicePlatform(this.appUnderTest, this.deviceUnderTest));
				if(!this.deviceUnderTest.toLowerCase().trim().equals("anydevice")){
					appiumDesiredCaps.setCapability(MobileCapabilityType.PLATFORM_VERSION, idbConstants.suiteConfig.getMobileDevicePlatformVer(this.appUnderTest, this.deviceUnderTest));
					appiumDesiredCaps.setCapability(MobileCapabilityType.UDID, idbConstants.suiteConfig.getMobileUDID(this.appUnderTest, this.deviceUnderTest));	
				}
				appiumDesiredCaps.setCapability(MobileCapabilityType.DEVICE_NAME, this.deviceUnderTest);
				appiumDesiredCaps.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, idbConstants.suiteConfig.getAndroidAppPackageName(this.appUnderTest));
				appiumDesiredCaps.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, idbConstants.suiteConfig.getAndroidAppPackageActivity(this.appUnderTest));
				appiumDesiredCaps.setCapability("automationName", idbConstants.suiteConfig.getAndroidAutomationName(this.appUnderTest));
				appiumDesiredCaps.setCapability("skipUnlock", "true");
				appiumDesiredCaps.setCapability("noReset", "true");			
				break;
			default:
				appiumDesiredCaps = null;
		}
		return appiumDesiredCaps;
	}
	
	/*
	 *Purpose: To get input execution parameters from command line or testng xml
	 *Author-date: Satya, Gajula - 14th Feb 19
	 *reviewer-date:  
	 */	
	protected String getExecutionParameter(ITestContext testContext, String paramName) {		
		String paramValue = null;
		try {
			//Check if execution parameter is passed through command line
			if (System.getProperty(paramName) !=null && System.getProperty(paramName).trim().length() > 0 ) {				
				paramValue = System.getProperty(paramName).trim();
			//Read execution parameter value from testng xml
			}else if (testContext.getCurrentXmlTest().getParameter(paramName) != null) {
				paramValue = testContext.getCurrentXmlTest().getParameter(paramName).trim();
			}							
		}catch(Exception e) {
			LogManager.logException(e, AbstractTestEngine.class.getName(), "Exception to read value of xml parameter " + paramName);
			e.printStackTrace();
		}
		return paramValue;
	}

	/*
	 *Purpose: To start new test case log in extent report
	 *Author-date: Satya, Gajula - 2/8/18
	 *reviewer-date:  
	 */
	public ExtentTest startTestCaseReport(ITestContext testContext, String testRailsId) {		

		ExtentTest tcReportLog = null;
		try {
			Long lngTestRailsId = Long.valueOf(testRailsId.replace("C","").replace("c","").trim());		
			String tcTitle = "TestCase " + testRailsId; //(String) trAccess.getTestCaseProperty(lngTestRailsId, "title");
			if (tcTitle != null) {
				//tcReportLog = this.extentMngr.addTest(testRailsId + " : " + testContext.getName(), tcTitle);				
				tcReportLog = this.extentMngr.addTest(testRailsId + " : " + tcTitle, tcTitle);				
				this.tcReport = tcReportLog;
				testContext.setAttribute("tcReport", this.tcReport);
			}							
		}catch(Exception e) {
			e.printStackTrace();
			LogManager.logException(e, testContext.getCurrentXmlTest().getName(), "Exception to start test case " + testRailsId);
		}
		return tcReport;
	}

	/*
	 * Purpose: To start new test case log in extent report 
	 * Author-date: Satya,Gajula - 2/8/18 
	 * reviewer-date:
	 */
	public ExtentTest startAdditionalTestCaseReport(ITestContext testContext, String testRailsId) {

		ExtentTest mergereport = null;
		try {
			Long lngTestRailsId = Long.valueOf(testRailsId.replace("C", "").replace("c", "").trim());
			String tcTitle = (String) trAccess.getTestCaseProperty(lngTestRailsId, "title");
			if (tcTitle != null) {
				mergereport = extentMngr.addTest(testRailsId + " : " + tcTitle, tcTitle);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LogManager.logException(e, testContext.getCurrentXmlTest().getName(),
					"Exception to start test case " + testRailsId);
		}
		return mergereport;
	}
	
	/*
	 *Purpose: To update extent reports and test rails with given test case report log
	 *Author-date: Satya, Gajula - 2/8/18
	 *reviewer-date:  
	 */
	public void endTestCaseReport(ExtentTest tcReportLog, String testRailsId) {

		//Update result in test rails		
		try {
			
			//Capture test case execution log
			String tcLog = getTestRailsLog(tcReportLog);
						
			//Capture project and test plan/test runs info
			Long lngTestRailsId = (long)-1;
	    	if (testRailsId == null || testRailsId.trim().length() == 0 ) {
	    		String tcTitle = tcReport.getModel().getName();
	    		testRailsId = tcTitle.substring(0, tcTitle.indexOf(":"));	    		
	    	}		
	    	lngTestRailsId = Long.valueOf(testRailsId.replace("C","").replace("c","").trim());
        	Long testRunId = 0L; //(Long) trAccess.getTestRunIdUsingTestPlanId(this.trTestPlanId.replace("r", "").replace("R",""), lngTestRailsId.toString());        	
        
	    	//String trProjName = gblConstants.suiteConfig.getTestRailsProjectName(this.appUnderTest);
	        //Long planID = (Long) trAccess.getTestPlanId(trProjName, this.trTestPlanId);	        	    	

	        //Skip Test Rail result logging as configured in SuiteConfig
	        if(blnLog2TestRail) {
	        	
		        if(testRunId !=null) {	 	    		
		    		//Report status to test rails
		    		switch (tcReportLog.getStatus()) {
		    			case PASS:
		    			case INFO:
		    			case WARNING:	    				
		    				trAccess.AddStatus(testRunId, lngTestRailsId, TestRailsAPIAccess.result_PASS, tcLog);
		    				break;
		    			
		    			case FAIL:
		    				trAccess.AddStatus(testRunId, lngTestRailsId, TestRailsAPIAccess.result_FAIL, tcLog);
		    				break;

		    			default:
		    				tcReportLog.log(Status.FAIL, "Failed to execute this test case through automation");
		    				trAccess.AddStatus(testRunId, lngTestRailsId, TestRailsAPIAccess.result_RETEST, "Failed to execute this test case through automation");
		    		}
		    	}else {
		    		//Test run not found - Add warning and log error 
		    		LogManager.logError(AbstractTestEngine.class.getName(), "Failed to report " + testRailsId  + " test case results to test Rails");	    		
		    		tcReportLog.log(Status.WARNING, this.trTestPlanId + " is not an existing Test Plan ID to report test case result");	    		
		    	}	        	
	        }else {
	        	LogManager.logInfo(AbstractTestEngine.class.getName(), "Test Rails Reporting is disabled");
	        }
	        
	        //Update TestNG Log
	        updateTestNGLog(tcReportLog);
		}catch(Exception e) {
			e.printStackTrace();
			LogManager.logException(e, AbstractTestEngine.class.getName(), "Exception to post " + testRailsId + "test case result to test rails");
			tcReportLog.log(Status.FATAL, "Exception to post" + testRailsId  + " test case results to test Rails");
		}finally {
			this.extentMngr.flushReport();
			this.tcReport = null;
		}
	}
	/**
	 * Purpose : To update test case execution details and status to TestNG results  
	 * @param extentTCLog
	 */
	public void updateTestNGLog(ExtentTest extentTCLog) {
		
		try {
			String detailedLog = getTestRailsLog(extentTCLog);
			AssertionError resError = null;
			switch (extentTCLog.getStatus()) {
				case PASS:
				case INFO:
				case WARNING:	    									
					Reporter.log(detailedLog);
					Reporter.getCurrentTestResult().setStatus(ITestResult.SUCCESS);
					break;
				
				case FAIL:
					resError = new AssertionError(detailedLog);
					Reporter.getCurrentTestResult().setThrowable(resError);
					Reporter.getCurrentTestResult().setStatus(ITestResult.FAILURE);
					break;
		
				default:
					String resErrorMsg = "Failed to execute this test case through automation";
					resError = new AssertionError(resErrorMsg);
					Reporter.getCurrentTestResult().setThrowable(resError);
					Reporter.getCurrentTestResult().setStatus(ITestResult.SKIP);
			}
		}catch(Exception e) {
			LogManager.logException(e, AbstractTestEngine.class.getName(), "Exception to update TestNG log");
		}
	}
	
	/*
	 *Purpose: Adding single testcase report data into the suiteResults object. Storing data for bulk TCs update in TestRail 
	 *Author-date: Michal Bais - 7/25/19
	 */
	public void addTestCaseReport(ExtentTest tcReportLog, String testRailsId) {

		try {
			String tcLog = getTestRailsLog(tcReportLog);
						
			Long lngTestRailsId = (long)-1;
	    	if (testRailsId == null || testRailsId.trim().length() == 0 ) {
	    		String tcTitle = tcReport.getModel().getName();
	    		testRailsId = tcTitle.substring(0, tcTitle.indexOf(":"));	    		
	    	}		
	    	lngTestRailsId = Long.valueOf(testRailsId.replace("C","").replace("c","").trim()); 	    	

	        //Skip Test Rail result logging as configured in SuiteConfig
	        if(blnLog2TestRail) {
				Map data = new HashMap();  
				data.put("case_id", lngTestRailsId);
				
		    		switch (tcReportLog.getStatus()) {
		    			case PASS:
		    			case INFO:
		    			case WARNING:	    				
							data.put("status_id", TestRailsAPIAccess.result_PASS);
							break;
		    			
		    			case FAIL:
							data.put("status_id", TestRailsAPIAccess.result_FAIL);
							break;

		    			default:
							data.put("status_id", TestRailsAPIAccess.result_RETEST);
		    		}
		        	
					data.put("comment", tcLog);
					
					suiteResults.add(data);
		        	  				    		        	
	        }else {
	        	LogManager.logInfo(AbstractTestEngine.class.getName(), "Test Rails Reporting is disabled");
	        }
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 *Purpose: Post all suite TCs results to TestRail. 
	 *Only supports Test Suites which have all TCs from the same test run.
	 *Author-date: Michal Bais - 7/25/19
	 */
	public void postTestCasesReports() {

		//Update result in test rails		
		try {

			Long lngTestRailsId = (long)-1;		
	    	
			//Getting TestCaseId of the first test case. Needed as a parameter to get test run ID
	    	HashMap firstTestCase = (HashMap) suiteResults.get(0);
	        String firstTestCaseId = (String) firstTestCase.get("case_id").toString();

	    	lngTestRailsId = Long.valueOf(firstTestCaseId.replace("C","").replace("c","").trim());
	    	//Getting Test Run Id via test rails API. It is different value from Test Plan ID. 
        	Long testRunId = (Long) trAccess.getTestRunIdUsingTestPlanId(this.trTestPlanId.replace("r", "").replace("R",""), lngTestRailsId.toString());        	
  
	        System.out.print("Reporting TCs results to Test Rail: ");        	
        	for (int i =0; i< suiteResults.size(); i++){
    	    	HashMap currentCase = (HashMap) suiteResults.get(i);
    	        System.out.print(currentCase.get("case_id").toString()+", ");        		        		
        	}

	        if(blnLog2TestRail) {
		        if(testRunId !=null) {	
		        	Map<String, Object> testrailResultsObject = new HashMap<String, Object>();   				
		        	testrailResultsObject.put("results", suiteResults);
		    				trAccess.AddResults(testRunId, testrailResultsObject);
				        	suiteResults.clear();
		    	}else {
		    		//Test run not found - Add warning and log error 
		    		LogManager.logError(AbstractTestEngine.class.getName(), "Failed to report " + testRunId  + " test run results to test Rails");	    		
		    	}	        	
	        }else {
	        	LogManager.logInfo(AbstractTestEngine.class.getName(), "Test Rails Reporting is disabled");
	        }
		}catch(Exception e) {
			e.printStackTrace();
			LogManager.logException(e, AbstractTestEngine.class.getName(), "Exception to post test run result to test rails");
		}
	}	
	
	
	/*
	 *Purpose: To get test steps results from ExtentTest test case log
	 *Author-date: Satya, Gajula - 1/8/18
	 *reviewer-date:  
	 */
	public String getTestRailsLog(ExtentTest tcReportLog) {		

		//Get all log messages in test case
		List<Log> allLogs = tcReportLog.getModel().getLogContext().getAll();
		
		//Initialize testRails Log &  
		String testRailsLog="";				
		if(allLogs.size() > 0) {
			testRailsLog = "Automation Result: \n";
			
			//Cumulatively get log messages from test case report
			for (int logCntr=0;logCntr<allLogs.size();logCntr++) {			
				String stepStatus = allLogs.get(logCntr).getStatus().name();
				String stepLog = allLogs.get(logCntr).getDetails();
				//Update Log messages excluding screenshot info
				if(!(stepLog.trim().toUpperCase().contains("<IMG DATA-")) ) {
					testRailsLog += stepStatus + ":"+ stepLog + ", \n";
				}			
			}			
		}else {
			//Add status as failure, if test case is not execute i.e. does't have any log 
			testRailsLog = "No logs are reported for this test case " + tcReportLog.getModel().getName();
			tcReportLog.log(Status.ERROR, testRailsLog);			
		}
		return testRailsLog;

	}	
	

	/*
	 * Purpose: To report status to current tcReport and debugLog
	 */
    public void reportStatus(Status result, String strLogMsg){

		//Update HTML Report
		this.tcReport.log(result, strLogMsg);

		//Update Logger
		switch (result) {
			case WARNING:
			case PASS:		
				testngAssertion.assertTrue(true, strLogMsg);
				LogManager.logInfo(result.name(), strLogMsg);
				break;
			case FATAL:				
			case FAIL:
				testngAssertion.assertTrue(false,strLogMsg);
				LogManager.logError(result.name(), strLogMsg);
				break;
			default:
		}
    } 

    

	/*
	 * Purpose: To report status to current tcReport and debugLog
	 */
    public void reportStatus(boolean blnRes, String successMsg, String failureMsg){

		if (blnRes) {
			reportStatus(Status.PASS, successMsg);
		}else {
			reportStatus(Status.FAIL, failureMsg);
			attachScreenshot(Status.FAIL, failureMsg); // Added to attach screen shot on failure
		}
    }         
    

	/*
	 * Purpose: To report status status with a screenshot
	 * author-date : Satya, Gajula - 3/8/18
	 */   
    public void reportStatus(Status result, String strLogMsg, String strScreenShotName ){    		

		//Update HTML Report
    	reportStatus(result, strLogMsg);
    	attachScreenshot(result, strScreenShotName);
    }    

	/*
	 * Purpose: To report status status based on comparison type
	 * author-date : Shyamal
	 */       
    public Boolean reportStatus(String objName, String actual, ComparisionType Type, String expected, String strScreenShotName)  {
    	Boolean status = false;
    	switch (Type) {
    		case EQUAL :
    			if (actual.equals(expected)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] equal to Expected ->[" + expected + "]", strScreenShotName);
    				status = true;
    			} else {
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] not equal to Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    			
    		case EQUAL_IGNORECASE:	
    			if (actual.equalsIgnoreCase(expected)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] equal to ignorecase to Expected ->[" + expected + "]", strScreenShotName);
    				status = true;
    			} else{
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] not equal to ignorecase to Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    			
    		case CONTAINS:	
    			if (actual.contains(expected)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] contains Expected ->[" + expected + "]", strScreenShotName);
    				status = true;
    			} else{
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] not contains Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    			
    		case SUBSTRING_OF:	
    			if (expected.contains(actual)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] is substring of Expected ->[" + expected + "]", strScreenShotName);
    				status = true;
    			} else{
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] is not substring of Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    			
    		case NOT_EQUAL:	
    			if (!actual.equals(expected)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] not equal to Expected ->[" + expected + "]", strScreenShotName);
    				status = true;
    			} else{
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] equal to Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    	}
    	return status;
    }
    
	/*
	 * Purpose: To report status status based on comparison type
	 * author-date : Shyamal
	 */           
    public Boolean reportStatus(String objName, String actual, ComparisionType Type, String expected)  {
    	Boolean status = false;
    	String strScreenShotName;
    	if (objName == null || objName.isEmpty() )  {
    		objName = "";
    		Date date= new Date();
    		strScreenShotName = Long.toString(date.getTime());
    	} else {
    		strScreenShotName = objName;
    	}
    	switch (Type) {
    		case EQUAL :
    			if (actual.equals(expected)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] equal to Expected ->[" + expected + "]");
    				status = true;
    			} else {
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] not equal to Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    			
    		case EQUAL_IGNORECASE:	
    			if (actual.equalsIgnoreCase(expected)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] equal to ignorecase to Expected ->[" + expected + "]");
    				status = true;
    			} else{
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] not equal to ignorecase to Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    			
    		case CONTAINS:	
    			if (actual.contains(expected)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] contains Expected ->[" + expected + "]");
    				status = true;
    			} else{
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] not contains Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    			
    		case SUBSTRING_OF:	
    			if (expected.contains(actual)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] is substring of Expected ->[" + expected + "]");
    				status = true;
    			} else{
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] is not substring of Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    			
    		case NOT_EQUAL:	
    			if (!actual.equals(expected)) {
    				reportStatus(Status.PASS, "Verification of value [" + objName + "] : Actual ->[" + actual + "] not equal to Expected ->[" + expected + "]");
    				status = true;
    			} else{
    				reportStatus(Status.FAIL, "Verification of value [" + objName + "] : Actual ->[" + actual + "] equal to Expected ->[" + expected + "]", strScreenShotName);	
    				status = false;
    			} 
    			break;
    	}
    	return status;
    }    
    
	/*
	 * 
	 * @purpose: To ReportMultiple Test Case Reports
	 * 
	 * @ Input params tcreportsList: List of Extent Test Objects,blnresult ,Strings
	 * success and Failure messages
	 * 
	 * @
	 */
	public void reportStatus(ExtentTest[] tcreportsList, boolean blnRes, String successMsg, String failureMsg) {

		Status currTCStatus = Status.INFO;
		String logMessage = "";
		if (blnRes) {
			currTCStatus = Status.PASS;
			logMessage = successMsg;
		} else {
			currTCStatus = Status.FAIL;
			logMessage = failureMsg;
		}

		for (ExtentTest currReport : tcreportsList) {
			if (currReport != null) {
				currReport.log(currTCStatus, logMessage);
				
			}
			
			/*if (currTCStatus == Status.FAIL) {
				extentMngr.attachScreenshot(currReport, Status.FAIL,
						this.cnActionEngine.captureScreenShot(gblConstants.reportsPath, failureMsg));
			}*/

		}
	}
	
	/*
	 * 
	 * @purpose: To ReportMultiple Test Case Reports
	 * 
	 * @ Input params tcreportsList: List of Extent Test Objects,blnresult ,Strings
	 * success and Failure messages
	 * 
	 * @
	 */
	public void reportStatus(ExtentTest[] tcreportsList, Status status, String statusMsg) {

		//String logMessage = "";

		for (ExtentTest currReport : tcreportsList) {
			if (currReport != null) {
				currReport.log(status, statusMsg);
				
				/* if(status == Status.FAIL) {
					extentMngr.attachScreenshot(currReport, Status.FAIL,
							this.cnActionEngine.captureScreenShot(gblConstants.reportsPath, statusMsg));				
				}*/			
			}
		}
	}    
    
	/*
	 * Purpose: To check for instances of given process and kill them
	 * author-date : Ramesh, Jadala - 9/7/18
	 */  
    public boolean killProcess(String processName) {
		boolean flag=false;
		
		try {
			if(osName.toUpperCase().contains("WINDOWS")) {
				//Get running proceses list
				Process pro = Runtime.getRuntime().exec("tasklist");
				BufferedReader reader = new BufferedReader(new InputStreamReader(pro.getInputStream()));
				String line;
				
				//Iterate through list and kill given process
				while ((line = reader.readLine()) != null) {
					if (line.startsWith(processName)) {
						Runtime.getRuntime().exec("Taskkill /IM "+processName+".exe /F");
						Thread.sleep(idbConstants.defSleepTime);
						flag=true;
					}
				}
				
				//Verify and report process
				if(flag) {
					LogManager.logInfo(AbstractTestEngine.class.getName(), processName+ " process is killed.");
				}else {
					LogManager.logInfo(AbstractTestEngine.class.getName(),  processName+ " process is NOT running");
				}				
			}
	
		} catch (Exception e) {
			flag=false;
			e.printStackTrace();
			LogManager.logException(e, AbstractTestEngine.class.getName(), "Exception to kill process....");
		}
		
		return flag;
	} 
    
	/*
	 * Purpose: To login application under test using default credentials from SuiteConfig.json
	 * author-date : Satya, Gajula - 3/8/18
	 */   
    public abstract boolean attachScreenshot(Status result, String screenshotName);    

	/*
	 * Purpose: To initialize app execution env, browser, app under test, app test environment, device under, test rails plan
	 * author-date : Satya, Gajula - 3/8/18
	 */   
    public abstract void beforeSuite(ITestContext testContext, String execEnvName, String testBrowser, String appUT, 
			String appTestEnv, String devUT, String trPlanId);

	/*
	 * Purpose: To report status to current tcReport and debugLog
	 * author-date : Satya, Gajula - 3/8/18
	 */   
    public abstract void afterSuite();


	/*
	 * Purpose: To initialize extent reports test case, corresponding to its test case id 
	 * author-date : Satya, Gajula - 3/8/18
	 */   
    public abstract void beforeTest(ITestContext testContext, String testRailsTCId);
    
    //public abstract void beforeTest(ITestContext testContext, String testRailsTCId, String execEnvName, String appTestEnv, String trPlanId);

	/*
	 * Purpose: To report test case results back to the report 
	 * author-date : Satya, Gajula - 3/8/18
	 */   
    public abstract void afterTest(String testRailsTCId);

	/*
	 * Purpose: To login application under test using default credentials from SuiteConfig.json
	 * author-date : Satya, Gajula - 3/8/18
	 */   
   // public abstract boolean loginUsingDefaultUser();
    
    
    /*
	 * Purpose: To verify status of url and return status code
	 * author-date : Jagadeesh, Boddeda - 3/15/19
	 */
    public boolean checkUrlStatus(ITestContext testContext, String url) {    	
    	boolean status = false;
    	HttpURLConnection connection = null;
    	try {
    		//create connection
    		connection = (HttpURLConnection) new URL(url).openConnection();
    		connection.setConnectTimeout(10000);
    		connection.setReadTimeout(10000);
    		connection.setRequestMethod("TRACE");
    		connection.connect();

    		status = (connection.getResponseCode() == 200)? true:false;
    	} catch (Exception e) {
    		status=false;
    		e.printStackTrace();
    		LogManager.logException(e, AbstractTestEngine.class.getName(), "Exception to check url status");
    	}
    	if (!status) {
    		ExtentTest execLog = extentMngr.addTest(testContext.getSuite().getName(), "Check status of Environment URL");  			
    		execLog.log(Status.FATAL, "Failed to execute suite: " + testContext.getSuite().getName() + " as server url " + url + " is not accessible");    		
    	}
    	connection.disconnect();
    	return status;
    }
}